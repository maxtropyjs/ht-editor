// const express = require('express');
// const app = express();
let mockJSON = {};
let mockImage = {};
let mockStorage = {};
mockStorage.displays = {
  "table": {
    "benchmark.json": true,
    "benchmark.png": true
  }
}
let obj = {};
// function test() {
//   fetch('http://127.0.0.1:7001/').then((data) => {
//     data.text().then((res) => {
//       console.log(res);
//       obj.displays = {
//         res
//       }
//     })
//   });
// }
// test();
MockService = function (handler) {
    this.handler = handler;
    this.handler({
        type: 'connected',
        message: 'Mock service connected'
    });
};

// mockStorage[data.substr(1)] = {
//   "table": {
//     "benchmark.json": true,
//     "benchmark.png": true
//   }
// }

ht.Default.def('MockService', Object, {
    request: function(cmd, data, callback) {
        console.log(cmd, data);
        this[cmd](data, callback);
        var message = cmd;
        if (data) {
            if (typeof data === 'string') {
                message = cmd + ': ' + data;
            }
            else if (data.path) {
                message = cmd + ': ' + data.path;
            }
        }
        this.handler({ type: 'request', message: message, cmd: cmd, data: data });
    },
    explore: function(data, callback) {
      callback(obj[data.substr(1)]);
    },
    mkdir: function(data, callback) {
        this.setPathContent(data, {}, callback);
    },
    upload: function(data, callback) {
        var path = data.path;
        if (/\.(png|jpg|gif|jpeg|bmp)$/i.test(path)) {
            mockImage[path] = data.content;
        }
        else {
            mockJSON[path] = data.content;
        }
        const test = () => {
          console.log('test');
        }
        test();
        this.setPathContent(path, true, callback);
    },
    rename: function(data, callback) {
        var ss = data.old.split('/');
        var value = mockStorage;
        for (var i = 0; i < ss.length - 1; i++) {
            value = value[ss[i]];
        }
        var oldContent = value[ss[ss.length - 1]];
        delete value[ss[ss.length - 1]];
        value[data.new.split('/').pop()] = oldContent;
        callback(true);
        this.handler({ type: 'fileChanged', path: data.old });
        this.handler({ type: 'fileChanged', path: data.new });
    },
    remove: function(data, callback) {
        this.setPathContent(data, undefined, callback);
    },
    locate: function(data, callback) {
        // Not supported
    },
    source: function(path, callback) {
        if (/\.(png|jpg|gif|jpeg|bmp)$/i.test(path)) {
            ht.Default.xhrLoad(path, function(data) {
                var file = new FileReader();
                file.onload = function (e) {
                    callback(e.target.result);
                }
                file.readAsDataURL(data);
            }, { responseType: 'blob' });
        }
        else {
            ht.Default.xhrLoad(path, function(data) {
                callback(data);
            });
        }
    },
    setPathContent: function(path, content, callback) {
        var ss = path.split('/');
        var value = mockStorage;
        for (var i = 0; i < ss.length - 1; i++) {
            if (value[ss[i]] == null) {
                if (content === undefined) {
                    break;
                }
                value[ss[i]] = {};
            }
            value = value[ss[i]];
        }
        if (value) value[ss[ss.length - 1]] = content;
        callback(true);
        this.handler({ type: 'fileChanged', path: path });
    }
});
