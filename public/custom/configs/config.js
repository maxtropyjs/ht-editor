window.isPracticing = window.location.host.indexOf('hightopo') >= 0;

window.hteditor_config = {
    //color_select: '#FF7733',
    locale: 'zh',
    locateFileEnabled: !isPracticing,
    componentsVisible: !isPracticing,
    displaysModifiable: !isPracticing,
    symbolsModifiable: !isPracticing,
    componentsModifiable: !isPracticing,
    assetsModifiable: !isPracticing,
    expandedTitles: {
        TitleExtension: false
    },
    subConfigs: [
        'custom/configs/config-handleEvent.js',
        'custom/configs/config-valueTypes.js',
        'custom/configs/config-dataBindings.js',
        'custom/configs/config-inspectorFilter.js',
        'custom/configs/config-customProperties.js',
        'custom/configs/config-onTitleCreating.js',
        'custom/configs/config-onTitleCreated.js',
        'custom/configs/config-onMainToolbarCreated.js',
        'custom/configs/config-onMainMenuCreated.js',
        'custom/configs/config-onRightToolbarCreated.js'
    ],
    libs: [
        'custom/libs/echarts.js',
        'custom/libs/NewHTTPService.js'
    ],
    serviceClass: 'HttpService',
    appendDisplayConnectActionTypes: ['HostParent', 'CreateEdge', 'CopySize'],
    appendSymbolConnectActionTypes: ['CopySize'],
    appendConnectActions: {
        CopySize: {
            action: function(gv, source, target) {
                if (source instanceof ht.Node && target instanceof ht.Node) {
                    source.setWidth(target.getWidth());
                    source.setHeight(target.getHeight());
                }
            },
            extraInfo: {
                visible: function (gv) {
                    return gv.sm().ld() instanceof ht.Node;
                }
            }
        },
        HostParent: {
            action: function(gv, source, target) {
                if (source instanceof ht.Node && target instanceof ht.Node) {
                    gv.dm().beginTransaction();
                    if (source instanceof ht.Node) source.setHost(target);
                    source.setParent(target);
                    gv.dm().endTransaction();
                }
            },
            extraInfo: {
                delete: {
                    visible: function(gv) {
                        var data = gv.sm().ld();
                        return data instanceof ht.Node && (data.getHost() || data.getParent());
                    },
                    action: function(gv, source) {
                        if (source instanceof ht.Node) {
                            gv.dm().beginTransaction();
                            source.setHost(undefined);
                            source.setParent(undefined);
                            gv.dm().endTransaction();
                        }
                    }
                },
                visible: function (gv) {
                    return gv.sm().ld() instanceof ht.Node;
                }
            }
        },
        CreateEdge: {
            action: function(gv, source, target) {
                if (source instanceof ht.Node && target instanceof ht.Node) {
                    var edge = new ht.Edge(source, target);
                    gv.editView.addData(edge, false, false, true);
                }
            },
            extraInfo: {
                visible: function (gv) {
                    return gv.sm().ld() instanceof ht.Node;
                }
            }
        }
    }
};
