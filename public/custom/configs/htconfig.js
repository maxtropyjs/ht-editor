const userId = localStorage.getItem('sid');

window.htconfig = {
    Default: {
        toolTipDelay: 100,
        toolTipContinual: true,
        convertURL: function (url) {
            var storagePrefix = '';
            if (storagePrefix && url && !/^data:image/.test(url) && !/^http/.test(url) && !/^https/.test(url)) {
                url = storagePrefix + '/' + url
            }
            // append timestamp
            // url += (url.indexOf('?') >= 0 ? '&' : '?') + 'ts=' + Date.now();
            // append sid
            var match = window.location.href.match('sid=([0-9a-z\-]*)');
            if (match) {
                window.sid = match[1]
            }
            if (window.sid) {
                url += '&sid=' + window.sid;
            }
            if (url.indexOf('custom/images/') !== -1) {
                return url;
            }
            const pathList = url.split('/');
            const last = pathList.pop();
            if ((last.indexOf('userId=') > -1)) {
              return 'api/download?fileName=' + last.substr(7) + '/'  + pathList.join('/') + '&publish=true';
            }
            return 'api/download?fileName=' + userId + '/'  + url;
        }
    }
};
