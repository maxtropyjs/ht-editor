var dataJson ={
    "v": "7.0.1",
    "p": {
        "autoAdjustIndex": true,
        "hierarchicalRendering": true
    },
    "a": {
        "gridAngle": 1.0472,
        "gridRotation": -0.5236,
        "connectActionType": null,
        "width": 0,
        "previewURL": "display.html/test"
    },
    "d": [
        {
            "c": "ht.Data",
            "i": 132,
            "p": {
                "displayName": "海尔一期"
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Data",
            "i": 133,
            "p": {
                "displayName": "L-G-C",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Block",
            "i": 134,
            "p": {
                "displayName": "G01",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": -745.3886,
                    "y": -825.20269
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 249.16031,
                "height": 165.87001
            }
        },
        {
            "c": "ht.Node",
            "i": 135,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 134
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -650.25579,
                    "y": -877.44855
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 136,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 134
                },
                "position": {
                    "x": -768.77295,
                    "y": -800.8187
                },
                "width": 220.28631,
                "height": 126.48033,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -878.9161,
                            "y": -737.57854
                        },
                        {
                            "x": -878.9161,
                            "y": -737.57854
                        },
                        {
                            "x": -800.83193,
                            "y": -782.41161
                        },
                        {
                            "x": -745.76036,
                            "y": -814.03169
                        },
                        {
                            "x": -690.68878,
                            "y": -845.65177
                        },
                        {
                            "x": -658.62979,
                            "y": -864.05887
                        },
                        {
                            "x": -658.62979,
                            "y": -864.05887
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 137,
            "p": {
                "displayName": "G02",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": -579.8701,
                    "y": -729.09369
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 250.83729,
                "height": 166.98639
            }
        },
        {
            "c": "ht.Node",
            "i": 138,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 137
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -484.097,
                    "y": -781.69118
                },
                "width": 41.27595,
                "height": 52.34999
            }
        },
        {
            "c": "ht.Shape",
            "i": 139,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 137
                },
                "position": {
                    "x": -603.41184,
                    "y": -704.54559
                },
                "width": 221.76895,
                "height": 127.3316,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -714.29631,
                            "y": -640.87978
                        },
                        {
                            "x": -714.29631,
                            "y": -640.87978
                        },
                        {
                            "x": -635.68661,
                            "y": -686.0146
                        },
                        {
                            "x": -580.24437,
                            "y": -717.8475
                        },
                        {
                            "x": -524.80213,
                            "y": -749.6804
                        },
                        {
                            "x": -492.52736,
                            "y": -768.21138
                        },
                        {
                            "x": -492.52736,
                            "y": -768.21138
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 140,
            "p": {
                "displayName": "G03",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": -403.07642,
                    "y": -624.78272
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 249.16032,
                "height": 165.87002
            }
        },
        {
            "c": "ht.Node",
            "i": 141,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 140
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -307.94361,
                    "y": -677.02858
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 142,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 140
                },
                "position": {
                    "x": -426.46077,
                    "y": -600.39873
                },
                "width": 220.28631,
                "height": 126.48033,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -536.60392,
                            "y": -537.15857
                        },
                        {
                            "x": -536.60392,
                            "y": -537.15857
                        },
                        {
                            "x": -458.51975,
                            "y": -581.99164
                        },
                        {
                            "x": -403.44818,
                            "y": -613.61172
                        },
                        {
                            "x": -348.3766,
                            "y": -645.2318
                        },
                        {
                            "x": -316.31761,
                            "y": -663.6389
                        },
                        {
                            "x": -316.31761,
                            "y": -663.6389
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 143,
            "p": {
                "displayName": "G06",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": -227.87163,
                    "y": -525.80748
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 249.16031,
                "height": 165.87002
            }
        },
        {
            "c": "ht.Node",
            "i": 144,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 143
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -132.73882,
                    "y": -578.05334
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 145,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 143
                },
                "position": {
                    "x": -251.25598,
                    "y": -501.42349
                },
                "width": 220.28631,
                "height": 126.48033,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -361.39914,
                            "y": -438.18333
                        },
                        {
                            "x": -361.39914,
                            "y": -438.18333
                        },
                        {
                            "x": -283.31497,
                            "y": -483.0164
                        },
                        {
                            "x": -228.24339,
                            "y": -514.63648
                        },
                        {
                            "x": -173.17182,
                            "y": -546.25656
                        },
                        {
                            "x": -141.11283,
                            "y": -564.66365
                        },
                        {
                            "x": -141.11283,
                            "y": -564.66365
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 146,
            "p": {
                "displayName": "G07",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": -54.20464,
                    "y": -426.87614
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 249.16031,
                "height": 165.87
            }
        },
        {
            "c": "ht.Node",
            "i": 147,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 146
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 40.92817,
                    "y": -479.122
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 148,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 146
                },
                "position": {
                    "x": -77.58899,
                    "y": -402.49216
                },
                "width": 220.28631,
                "height": 126.48033,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -187.73215,
                            "y": -339.252
                        },
                        {
                            "x": -187.73215,
                            "y": -339.252
                        },
                        {
                            "x": -109.64798,
                            "y": -384.08506
                        },
                        {
                            "x": -54.5764,
                            "y": -415.70515
                        },
                        {
                            "x": 0.49517,
                            "y": -447.32523
                        },
                        {
                            "x": 32.55416,
                            "y": -465.73232
                        },
                        {
                            "x": 32.55416,
                            "y": -465.73232
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 149,
            "p": {
                "displayName": "G08",
                "parent": {
                    "__i": 133
                },
                "position": {
                    "x": 166.28382,
                    "y": -339.7197
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 343.56413,
                "height": 197.44251
            }
        },
        {
            "c": "ht.Shape",
            "i": 150,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 149
                },
                "position": {
                    "x": 263.93296,
                    "y": -397.37851
                },
                "width": 123.59232,
                "height": 70.96218,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 202.1368,
                            "y": -361.89741
                        },
                        {
                            "x": 202.1368,
                            "y": -361.89741
                        },
                        {
                            "x": 245.94616,
                            "y": -387.05115
                        },
                        {
                            "x": 276.84425,
                            "y": -404.79169
                        },
                        {
                            "x": 307.74233,
                            "y": -422.53224
                        },
                        {
                            "x": 325.72912,
                            "y": -432.8596
                        },
                        {
                            "x": 325.72912,
                            "y": -432.8596
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 151,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 149
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 210.82531,
                    "y": -375.2871
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 152,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 149
                },
                "position": {
                    "x": 92.30815,
                    "y": -298.65725
                },
                "width": 220.28631,
                "height": 126.48033,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -17.83501,
                            "y": -235.41709
                        },
                        {
                            "x": -17.83501,
                            "y": -235.41709
                        },
                        {
                            "x": 60.24916,
                            "y": -280.25016
                        },
                        {
                            "x": 115.32074,
                            "y": -311.87024
                        },
                        {
                            "x": 170.39231,
                            "y": -343.49032
                        },
                        {
                            "x": 202.4513,
                            "y": -361.89741
                        },
                        {
                            "x": 202.4513,
                            "y": -361.89741
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Data",
            "i": 153,
            "p": {
                "displayName": "L-G-K",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Block",
            "i": 154,
            "p": {
                "displayName": "L01",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -859.31817,
                    "y": -782.98507
                },
                "width": 265,
                "height": 228.77317
            }
        },
        {
            "c": "ht.Text",
            "i": 155,
            "p": {
                "parent": {
                    "__i": 154
                },
                "position": {
                    "x": -839.50764,
                    "y": -700.83919
                },
                "anchor": {
                    "x": 0.5,
                    "y": 0.46172
                },
                "width": 116.82452,
                "height": 59.89579
            },
            "s": {
                "text": "冷干机01",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 156,
            "p": {
                "displayName": "冷干机01",
                "parent": {
                    "__i": 154
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -908.97485,
                    "y": -866.1771
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 182.15668,
                "height": 160.52071
            }
        },
        {
            "c": "ht.Block",
            "i": 157,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 154
                },
                "position": {
                    "x": -769.03648,
                    "y": -808.55849
                },
                "width": 76.69755,
                "height": 77.57951
            }
        },
        {
            "c": "ht.Node",
            "i": 158,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 157
                },
                "tag": "95-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -765.00226,
                    "y": -805.16506
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 43.14237,
                "height": 43.12497
            }
        },
        {
            "c": "ht.Node",
            "i": 159,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 157
                },
                "tag": "95-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -769.03648,
                    "y": -808.10204
                },
                "width": 76.69755,
                "height": 76.66661
            }
        },
        {
            "c": "ht.Node",
            "i": 160,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 157
                },
                "tag": "95-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -771.43328,
                    "y": -809.01493
                },
                "width": 62.31676,
                "height": 76.66661
            }
        },
        {
            "c": "ht.Node",
            "i": 161,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 154
                },
                "tag": "95-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -925.90621,
                    "y": -793.30841
                },
                "width": 131.82391,
                "height": 95.83326
            }
        },
        {
            "c": "ht.Block",
            "i": 162,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 154
                },
                "position": {
                    "x": -797.11044,
                    "y": -880.00187
                },
                "width": 41.94397,
                "height": 34.73956
            }
        },
        {
            "c": "ht.Node",
            "i": 163,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 162
                },
                "tag": "95-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -789.59744,
                    "y": -872.45679
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.94397,
                "height": 34.73956
            }
        },
        {
            "c": "ht.Node",
            "i": 164,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 162
                },
                "tag": "95-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -797.11044,
                    "y": -880.02851
                },
                "width": 39.14759,
                "height": 31.60646
            }
        },
        {
            "c": "ht.Block",
            "i": 165,
            "p": {
                "displayName": "G01",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -1009.1901,
                    "y": -662.45646
                },
                "width": 327.44331,
                "height": 188.18654
            }
        },
        {
            "c": "ht.Shape",
            "i": 166,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 165
                },
                "position": {
                    "x": -917.9002,
                    "y": -714.96207
                },
                "width": 144.86351,
                "height": 83.17532,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -990.33194,
                            "y": -673.37441
                        },
                        {
                            "x": -990.33194,
                            "y": -673.37441
                        },
                        {
                            "x": -938.98266,
                            "y": -702.8573
                        },
                        {
                            "x": -902.76678,
                            "y": -723.65112
                        },
                        {
                            "x": -866.5509,
                            "y": -744.44494
                        },
                        {
                            "x": -845.46844,
                            "y": -756.54973
                        },
                        {
                            "x": -845.46844,
                            "y": -756.54973
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 167,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 165
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -1001.58694,
                    "y": -676.63808
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 168,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 165
                },
                "position": {
                    "x": -1091.10628,
                    "y": -615.3329
                },
                "width": 163.61096,
                "height": 93.93942,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -1172.91176,
                            "y": -568.36319
                        },
                        {
                            "x": -1172.91176,
                            "y": -568.36319
                        },
                        {
                            "x": -1114.91712,
                            "y": -601.66159
                        },
                        {
                            "x": -1074.01438,
                            "y": -625.14644
                        },
                        {
                            "x": -1033.11164,
                            "y": -648.63129
                        },
                        {
                            "x": -1009.3008,
                            "y": -662.3026
                        },
                        {
                            "x": -1009.3008,
                            "y": -662.3026
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 169,
            "p": {
                "displayName": "K01",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -1195.93077,
                    "y": -536.75383
                },
                "width": 494.24167,
                "height": 356.41649
            }
        },
        {
            "c": "ht.Text",
            "i": 170,
            "p": {
                "parent": {
                    "__i": 169
                },
                "position": {
                    "x": -1227.96705,
                    "y": -395.64239
                },
                "width": 144.77969,
                "height": 74.19361
            },
            "s": {
                "text": "空压机01",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 171,
            "p": {
                "displayName": "空压机01",
                "parent": {
                    "__i": 169
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -1222.09022,
                    "y": -541.6438
                },
                "width": 267.32997,
                "height": 267.09698
            }
        },
        {
            "c": "ht.Block",
            "i": 172,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 169
                },
                "position": {
                    "x": -1144.16775,
                    "y": -603.38403
                },
                "width": 95.05065,
                "height": 96.09863
            }
        },
        {
            "c": "ht.Node",
            "i": 173,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 172
                },
                "tag": "86-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -1139.16817,
                    "y": -599.18055
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.46599,
                "height": 53.4194
            }
        },
        {
            "c": "ht.Node",
            "i": 174,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 172
                },
                "tag": "86-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -1144.16775,
                    "y": -602.81862
                },
                "width": 95.05065,
                "height": 94.96782
            }
        },
        {
            "c": "ht.Node",
            "i": 175,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 172
                },
                "tag": "86-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -1144.16775,
                    "y": -603.94944
                },
                "width": 77.22866,
                "height": 94.96782
            }
        },
        {
            "c": "ht.Block",
            "i": 176,
            "p": {
                "displayName": "D01",
                "parent": {
                    "__i": 169
                },
                "position": {
                    "x": -1137.50788,
                    "y": -503.78405
                },
                "width": 96.66285,
                "height": 148.17073
            }
        },
        {
            "c": "ht.Text",
            "i": 177,
            "p": {
                "parent": {
                    "__i": 176
                },
                "position": {
                    "x": -1129.8517,
                    "y": -451.51357
                },
                "width": 81.3505,
                "height": 43.62976
            },
            "s": {
                "text": "电表01",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 178,
            "p": {
                "displayName": "电表01",
                "parent": {
                    "__i": 176
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -1141.24086,
                    "y": -520.03679
                },
                "width": 89.19687,
                "height": 115.66525
            }
        },
        {
            "c": "ht.Node",
            "i": 179,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 169
                },
                "tag": "86-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -1361.36745,
                    "y": -538.83442
                },
                "width": 163.36831,
                "height": 118.70977
            }
        },
        {
            "c": "ht.Node",
            "i": 180,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 169
                },
                "tag": "169-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -1029.80993,
                    "y": -568.22224
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 181,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 169
                },
                "position": {
                    "x": -1186.44429,
                    "y": -693.44592
                },
                "width": 51.98083,
                "height": 43.03229
            }
        },
        {
            "c": "ht.Node",
            "i": 182,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 181
                },
                "tag": "86-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -1177.13348,
                    "y": -684.09974
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 51.98083,
                "height": 43.03229
            }
        },
        {
            "c": "ht.Node",
            "i": 183,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 181
                },
                "tag": "86-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -1186.44429,
                    "y": -693.47893
                },
                "width": 48.51529,
                "height": 39.15128
            }
        },
        {
            "c": "ht.Block",
            "i": 184,
            "p": {
                "displayName": "L02",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -686.08,
                    "y": -682.91506
                },
                "width": 265,
                "height": 231.01596
            }
        },
        {
            "c": "ht.Text",
            "i": 185,
            "p": {
                "parent": {
                    "__i": 184
                },
                "position": {
                    "x": -665.54836,
                    "y": -597.2741
                },
                "width": 116.85961,
                "height": 59.73404
            },
            "s": {
                "text": "冷干机02",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 186,
            "p": {
                "displayName": "冷干机02",
                "parent": {
                    "__i": 184
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -735.7914,
                    "y": -763.7773
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 182.2114,
                "height": 160.08723
            }
        },
        {
            "c": "ht.Block",
            "i": 187,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 184
                },
                "position": {
                    "x": -602.06739,
                    "y": -701.80887
                },
                "width": 76.72059,
                "height": 77.36999
            }
        },
        {
            "c": "ht.Node",
            "i": 188,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 187
                },
                "tag": "96-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -598.03197,
                    "y": -698.42462
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 43.15533,
                "height": 43.00851
            }
        },
        {
            "c": "ht.Node",
            "i": 189,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 187
                },
                "tag": "96-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -602.06739,
                    "y": -701.35366
                },
                "width": 76.72059,
                "height": 76.45957
            }
        },
        {
            "c": "ht.Node",
            "i": 190,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 187
                },
                "tag": "96-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -603.26615,
                    "y": -702.26408
                },
                "width": 62.33548,
                "height": 76.45957
            }
        },
        {
            "c": "ht.Node",
            "i": 191,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 184
                },
                "tag": "96-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -752.64825,
                    "y": -691.24368
                },
                "width": 131.86351,
                "height": 95.57446
            }
        },
        {
            "c": "ht.Block",
            "i": 192,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 184
                },
                "position": {
                    "x": -623.04568,
                    "y": -781.10017
                },
                "width": 41.95657,
                "height": 34.64574
            }
        },
        {
            "c": "ht.Node",
            "i": 193,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 192
                },
                "tag": "96-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -615.53042,
                    "y": -773.57546
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.95657,
                "height": 34.64574
            }
        },
        {
            "c": "ht.Node",
            "i": 194,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 192
                },
                "tag": "96-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -623.04568,
                    "y": -781.12674
                },
                "width": 39.15935,
                "height": 31.5211
            }
        },
        {
            "c": "ht.Block",
            "i": 195,
            "p": {
                "displayName": "G02",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -1035.54261,
                    "y": -448.80642
                },
                "width": 727.30932,
                "height": 417.77494
            }
        },
        {
            "c": "ht.Shape",
            "i": 196,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 195
                },
                "position": {
                    "x": -744.98519,
                    "y": -615.72414
                },
                "width": 146.19448,
                "height": 83.93951,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -818.08243,
                            "y": -573.75439
                        },
                        {
                            "x": -818.08243,
                            "y": -573.75439
                        },
                        {
                            "x": -766.26135,
                            "y": -603.50815
                        },
                        {
                            "x": -729.71273,
                            "y": -624.49303
                        },
                        {
                            "x": -693.16412,
                            "y": -645.47791
                        },
                        {
                            "x": -671.88795,
                            "y": -657.69389
                        },
                        {
                            "x": -671.88795,
                            "y": -657.69389
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 197,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 195
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -828.55908,
                    "y": -577.25434
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 198,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 195
                },
                "position": {
                    "x": -1116.2016,
                    "y": -402.40471
                },
                "width": 565.99135,
                "height": 324.97151,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -1399.19728,
                            "y": -239.91896
                        },
                        {
                            "x": -1399.19728,
                            "y": -239.91896
                        },
                        {
                            "x": -1198.57218,
                            "y": -355.11055
                        },
                        {
                            "x": -1057.07433,
                            "y": -436.35342
                        },
                        {
                            "x": -915.5765,
                            "y": -517.5963
                        },
                        {
                            "x": -833.20593,
                            "y": -564.89047
                        },
                        {
                            "x": -833.20593,
                            "y": -564.89047
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 199,
            "p": {
                "displayName": "K02",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -1413.34496,
                    "y": -228.89974
                },
                "width": 493.63431,
                "height": 360.0412
            }
        },
        {
            "c": "ht.Text",
            "i": 200,
            "p": {
                "parent": {
                    "__i": 199
                },
                "position": {
                    "x": -1448.55878,
                    "y": -86.16359
                },
                "width": 145.96386,
                "height": 74.56889
            },
            "s": {
                "text": "空压机02",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 201,
            "p": {
                "displayName": "空压机02",
                "parent": {
                    "__i": 199
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -1443.0516,
                    "y": -232.45366
                },
                "width": 269.51649,
                "height": 268.44799
            }
        },
        {
            "c": "ht.Block",
            "i": 202,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 199
                },
                "position": {
                    "x": -1370.30753,
                    "y": -284.26511
                },
                "width": 95.82808,
                "height": 96.58472
            }
        },
        {
            "c": "ht.Node",
            "i": 203,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 202
                },
                "tag": "88-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -1365.26706,
                    "y": -280.04038
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.9033,
                "height": 53.6896
            }
        },
        {
            "c": "ht.Node",
            "i": 204,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 202
                },
                "tag": "88-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -1370.30753,
                    "y": -283.69683
                },
                "width": 95.82808,
                "height": 95.44817
            }
        },
        {
            "c": "ht.Node",
            "i": 205,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 202
                },
                "tag": "88-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -1371.80485,
                    "y": -284.83338
                },
                "width": 77.86032,
                "height": 95.44817
            }
        },
        {
            "c": "ht.Block",
            "i": 206,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 199
                },
                "position": {
                    "x": -1410.73501,
                    "y": -387.29537
                },
                "width": 52.40598,
                "height": 43.24995
            }
        },
        {
            "c": "ht.Node",
            "i": 207,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 206
                },
                "tag": "88-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -1401.34805,
                    "y": -377.90191
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 52.40598,
                "height": 43.24995
            }
        },
        {
            "c": "ht.Node",
            "i": 208,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 206
                },
                "tag": "88-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -1410.73501,
                    "y": -387.32854
                },
                "width": 48.91211,
                "height": 39.34931
            }
        },
        {
            "c": "ht.Block",
            "i": 209,
            "p": {
                "displayName": "D02",
                "parent": {
                    "__i": 199
                },
                "position": {
                    "x": -1351.11345,
                    "y": -195.4941
                },
                "width": 110.59304,
                "height": 146.71691
            }
        },
        {
            "c": "ht.Text",
            "i": 210,
            "p": {
                "parent": {
                    "__i": 209
                },
                "position": {
                    "x": -1336.82488,
                    "y": -144.06087
                },
                "width": 82.01588,
                "height": 43.85044
            },
            "s": {
                "text": "电表02",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 211,
            "p": {
                "displayName": "电表02",
                "parent": {
                    "__i": 209
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -1361.44675,
                    "y": -210.72741
                },
                "width": 89.92643,
                "height": 116.2503
            }
        },
        {
            "c": "ht.Node",
            "i": 212,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 199
                },
                "tag": "88-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -1577.80985,
                    "y": -232.45366
                },
                "width": 164.70452,
                "height": 119.31022
            }
        },
        {
            "c": "ht.Node",
            "i": 213,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 199
                },
                "tag": "170-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -1247.5278,
                    "y": -257.98858
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 214,
            "p": {
                "displayName": "L03",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -513.27739,
                    "y": -583.82377
                },
                "width": 264.99999,
                "height": 231.63157
            }
        },
        {
            "c": "ht.Text",
            "i": 215,
            "p": {
                "parent": {
                    "__i": 214
                },
                "position": {
                    "x": -493.49127,
                    "y": -498.62165
                },
                "width": 116.43578,
                "height": 61.22732
            },
            "s": {
                "text": "冷干机03",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 216,
            "p": {
                "displayName": "冷干机03",
                "parent": {
                    "__i": 214
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -562.32794,
                    "y": -668.22246
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 181.55054,
                "height": 164.08922
            }
        },
        {
            "c": "ht.Block",
            "i": 217,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 214
                },
                "position": {
                    "x": -430.77061,
                    "y": -603.78774
                },
                "width": 76.44233,
                "height": 79.30416
            }
        },
        {
            "c": "ht.Node",
            "i": 218,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 217
                },
                "tag": "97-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -426.74981,
                    "y": -600.31888
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 42.99881,
                "height": 44.08367
            }
        },
        {
            "c": "ht.Node",
            "i": 219,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 217
                },
                "tag": "97-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -430.77061,
                    "y": -603.32115
                },
                "width": 76.44233,
                "height": 78.37097
            }
        },
        {
            "c": "ht.Node",
            "i": 220,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 217
                },
                "tag": "97-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -431.96502,
                    "y": -604.25434
                },
                "width": 62.1094,
                "height": 78.37097
            }
        },
        {
            "c": "ht.Node",
            "i": 221,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 214
                },
                "tag": "97-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -580.08476,
                    "y": -592.41576
                },
                "width": 131.38526,
                "height": 97.96371
            }
        },
        {
            "c": "ht.Block",
            "i": 222,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 214
                },
                "position": {
                    "x": -455.21976,
                    "y": -681.88363
                },
                "width": 41.8044,
                "height": 35.51185
            }
        },
        {
            "c": "ht.Node",
            "i": 223,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 222
                },
                "tag": "97-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -447.73176,
                    "y": -674.17081
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.8044,
                "height": 35.51185
            }
        },
        {
            "c": "ht.Node",
            "i": 224,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 222
                },
                "tag": "97-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -455.21976,
                    "y": -681.91086
                },
                "width": 39.01733,
                "height": 32.30909
            }
        },
        {
            "c": "ht.Block",
            "i": 225,
            "p": {
                "displayName": "G03",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -667.5326,
                    "y": -458.9735
                },
                "width": 329.9779,
                "height": 189.6418
            }
        },
        {
            "c": "ht.Shape",
            "i": 226,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 225
                },
                "position": {
                    "x": -571.94019,
                    "y": -513.94945
                },
                "width": 138.79308,
                "height": 79.6899,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -641.33673,
                            "y": -474.10449
                        },
                        {
                            "x": -641.33673,
                            "y": -474.10449
                        },
                        {
                            "x": -592.13921,
                            "y": -502.3519
                        },
                        {
                            "x": -557.44094,
                            "y": -522.27437
                        },
                        {
                            "x": -522.74266,
                            "y": -542.19685
                        },
                        {
                            "x": -502.54367,
                            "y": -553.79439
                        },
                        {
                            "x": -502.54367,
                            "y": -553.79439
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 227,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 225
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -644.77269,
                    "y": -477.78405
                },
                "anchor": {
                    "x": 0.66135,
                    "y": 0.5
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 228,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 225
                },
                "position": {
                    "x": -744.60336,
                    "y": -414.63201
                },
                "width": 175.83639,
                "height": 100.95882,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -832.52155,
                            "y": -364.1526
                        },
                        {
                            "x": -832.52155,
                            "y": -364.1526
                        },
                        {
                            "x": -770.1934,
                            "y": -399.93914
                        },
                        {
                            "x": -726.2343,
                            "y": -425.17884
                        },
                        {
                            "x": -682.2752,
                            "y": -450.41854
                        },
                        {
                            "x": -656.68516,
                            "y": -465.11141
                        },
                        {
                            "x": -656.68516,
                            "y": -465.11141
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 229,
            "p": {
                "displayName": "K03",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -841.26103,
                    "y": -336.85227
                },
                "width": 494.1588,
                "height": 355.81536
            }
        },
        {
            "c": "ht.Text",
            "i": 230,
            "p": {
                "parent": {
                    "__i": 229
                },
                "position": {
                    "x": -885.63039,
                    "y": -195.9147
                },
                "width": 144.94128,
                "height": 73.94023
            },
            "s": {
                "text": "空压机03",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 231,
            "p": {
                "displayName": "空压机03",
                "parent": {
                    "__i": 229
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -867.77277,
                    "y": -339.84771
                },
                "width": 267.62834,
                "height": 266.18484
            }
        },
        {
            "c": "ht.Block",
            "i": 232,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 229
                },
                "position": {
                    "x": -786.08112,
                    "y": -393.92837
                },
                "width": 95.15674,
                "height": 95.77045
            }
        },
        {
            "c": "ht.Node",
            "i": 233,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 232
                },
                "tag": "89-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -781.07597,
                    "y": -389.73925
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.52567,
                "height": 53.23697
            }
        },
        {
            "c": "ht.Node",
            "i": 234,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 232
                },
                "tag": "89-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -786.08112,
                    "y": -393.3649
                },
                "width": 95.15674,
                "height": 94.6435
            }
        },
        {
            "c": "ht.Node",
            "i": 235,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 232
                },
                "tag": "89-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -787.56795,
                    "y": -394.49185
                },
                "width": 77.31485,
                "height": 94.6435
            }
        },
        {
            "c": "ht.Block",
            "i": 236,
            "p": {
                "displayName": "D03",
                "parent": {
                    "__i": 229
                },
                "position": {
                    "x": -782.34397,
                    "y": -302.27568
                },
                "width": 96.77074,
                "height": 146.18592
            }
        },
        {
            "c": "ht.Text",
            "i": 237,
            "p": {
                "parent": {
                    "__i": 236
                },
                "position": {
                    "x": -774.67925,
                    "y": -250.9231
                },
                "width": 81.4413,
                "height": 43.48076
            },
            "s": {
                "text": "电表03",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 238,
            "p": {
                "displayName": "电表03",
                "parent": {
                    "__i": 236
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -786.08112,
                    "y": -317.73352
                },
                "width": 89.29643,
                "height": 115.27025
            }
        },
        {
            "c": "ht.Node",
            "i": 239,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 229
                },
                "tag": "89-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -1006.5651,
                    "y": -335.23612
                },
                "width": 163.55065,
                "height": 118.30438
            }
        },
        {
            "c": "ht.Node",
            "i": 240,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 229
                },
                "tag": "171-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -675.18163,
                    "y": -375.56223
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 241,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 229
                },
                "position": {
                    "x": -830.72934,
                    "y": -484.8084
                },
                "anchor": {
                    "x": 0.5,
                    "y": 0.69841
                },
                "width": 52.03884,
                "height": 42.88534
            }
        },
        {
            "c": "ht.Node",
            "i": 242,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 241
                },
                "tag": "89-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -821.40814,
                    "y": -484.00301
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 52.03884,
                "height": 42.88534
            }
        },
        {
            "c": "ht.Node",
            "i": 243,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 241
                },
                "tag": "89-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -830.72934,
                    "y": -493.35017
                },
                "width": 48.56944,
                "height": 39.01758
            }
        },
        {
            "c": "ht.Block",
            "i": 244,
            "p": {
                "displayName": "L06",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -338.5664,
                    "y": -483.12521
                },
                "width": 265,
                "height": 228.77168
            }
        },
        {
            "c": "ht.Text",
            "i": 245,
            "p": {
                "parent": {
                    "__i": 244
                },
                "position": {
                    "x": -315.06557,
                    "y": -398.92872
                },
                "width": 117.02084,
                "height": 60.3787
            },
            "s": {
                "text": "冷干机06",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 246,
            "p": {
                "displayName": "冷干机06",
                "parent": {
                    "__i": 244
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -388.52918,
                    "y": -566.08372
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 182.46278,
                "height": 161.81492
            }
        },
        {
            "c": "ht.Block",
            "i": 247,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 244
                },
                "position": {
                    "x": -254.80405,
                    "y": -504.68471
                },
                "width": 76.82643,
                "height": 78.20499
            }
        },
        {
            "c": "ht.Node",
            "i": 248,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 247
                },
                "tag": "100-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -250.76305,
                    "y": -501.26392
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 43.21487,
                "height": 43.47266
            }
        },
        {
            "c": "ht.Node",
            "i": 249,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 247
                },
                "tag": "100-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -254.80405,
                    "y": -504.22458
                },
                "width": 76.82643,
                "height": 77.28474
            }
        },
        {
            "c": "ht.Node",
            "i": 250,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 247
                },
                "tag": "100-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -256.00447,
                    "y": -505.14484
                },
                "width": 62.42148,
                "height": 77.28474
            }
        },
        {
            "c": "ht.Node",
            "i": 251,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 244
                },
                "tag": "100-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -405.04368,
                    "y": -491.37538
                },
                "width": 132.04543,
                "height": 96.60592
            }
        },
        {
            "c": "ht.Block",
            "i": 252,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 244
                },
                "position": {
                    "x": -277.77128,
                    "y": -580.00122
                },
                "width": 42.01446,
                "height": 35.01965
            }
        },
        {
            "c": "ht.Node",
            "i": 253,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 252
                },
                "tag": "100-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -270.24565,
                    "y": -572.3953
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 42.01446,
                "height": 35.01965
            }
        },
        {
            "c": "ht.Node",
            "i": 254,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 252
                },
                "tag": "100-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -277.77128,
                    "y": -580.02808
                },
                "width": 39.21338,
                "height": 31.86128
            }
        },
        {
            "c": "ht.Block",
            "i": 255,
            "p": {
                "displayName": "G06",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -691.62744,
                    "y": -245.52522
                },
                "width": 733.63085,
                "height": 421.40452
            }
        },
        {
            "c": "ht.Shape",
            "i": 256,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 255
                },
                "position": {
                    "x": -400.46699,
                    "y": -412.78917
                },
                "width": 151.30995,
                "height": 86.87662,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -476.12197,
                            "y": -369.35085
                        },
                        {
                            "x": -476.12197,
                            "y": -369.35085
                        },
                        {
                            "x": -422.48762,
                            "y": -400.14572
                        },
                        {
                            "x": -384.66013,
                            "y": -421.86487
                        },
                        {
                            "x": -346.83265,
                            "y": -443.58404
                        },
                        {
                            "x": -324.81202,
                            "y": -456.22746
                        },
                        {
                            "x": -324.81202,
                            "y": -456.22746
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 257,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 255
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -482.04365,
                    "y": -374.2871
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 258,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 255
                },
                "position": {
                    "x": -772.83044,
                    "y": -198.81114
                },
                "width": 571.22484,
                "height": 327.97636,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -1058.44287,
                            "y": -34.82296
                        },
                        {
                            "x": -1058.44287,
                            "y": -34.82296
                        },
                        {
                            "x": -855.96266,
                            "y": -151.07967
                        },
                        {
                            "x": -713.15645,
                            "y": -233.07377
                        },
                        {
                            "x": -570.35025,
                            "y": -315.06785
                        },
                        {
                            "x": -487.21802,
                            "y": -362.79933
                        },
                        {
                            "x": -487.21802,
                            "y": -362.79933
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 259,
            "p": {
                "displayName": "K06",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -1058.53231,
                    "y": -26.04675
                },
                "width": 494.89433,
                "height": 352.34165
            }
        },
        {
            "c": "ht.Text",
            "i": 260,
            "p": {
                "parent": {
                    "__i": 259
                },
                "position": {
                    "x": -1105.69353,
                    "y": 113.15727
                },
                "width": 144.26915,
                "height": 73.9336
            },
            "s": {
                "text": "空压机06",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 261,
            "p": {
                "displayName": "空压机06",
                "parent": {
                    "__i": 259
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -1086.49654,
                    "y": -28.55141
                },
                "width": 266.38728,
                "height": 266.16097
            }
        },
        {
            "c": "ht.Block",
            "i": 262,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 259
                },
                "position": {
                    "x": -1006.60592,
                    "y": -84.87118
                },
                "width": 95.17023,
                "height": 93.7556
            }
        },
        {
            "c": "ht.Node",
            "i": 263,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 262
                },
                "tag": "92-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -1003.53594,
                    "y": -81.57345
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 32.83067,
                "height": 52.43043
            }
        },
        {
            "c": "ht.Node",
            "i": 264,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 262
                },
                "tag": "92-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -1006.60592,
                    "y": -85.14415
                },
                "width": 58.36563,
                "height": 93.20965
            }
        },
        {
            "c": "ht.Node",
            "i": 265,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 262
                },
                "tag": "92-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -1006.60592,
                    "y": -84.5982
                },
                "width": 95.17023,
                "height": 93.20965
            }
        },
        {
            "c": "ht.Block",
            "i": 266,
            "p": {
                "displayName": "D06",
                "parent": {
                    "__i": 259
                },
                "position": {
                    "x": -1001.47682,
                    "y": 5.66264
                },
                "width": 93.50343,
                "height": 141.05565
            }
        },
        {
            "c": "ht.Text",
            "i": 267,
            "p": {
                "parent": {
                    "__i": 266
                },
                "position": {
                    "x": -995.25692,
                    "y": 54.45204
                },
                "width": 81.06363,
                "height": 43.47686
            },
            "s": {
                "text": "电表06",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 268,
            "p": {
                "displayName": "电表06",
                "parent": {
                    "__i": 266
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -1003.57399,
                    "y": -8.44261
                },
                "width": 89.30908,
                "height": 112.84514
            }
        },
        {
            "c": "ht.Node",
            "i": 269,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 259
                },
                "tag": "92-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -1224.19256,
                    "y": -23.38859
                },
                "width": 163.57383,
                "height": 115.81542
            }
        },
        {
            "c": "ht.Node",
            "i": 270,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 259
                },
                "tag": "167-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -892.08514,
                    "y": -64.61699
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 271,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 259
                },
                "position": {
                    "x": -1041.83865,
                    "y": -175.89321
                },
                "anchor": {
                    "x": 0.65322,
                    "y": 0.62702
                },
                "width": 49.03725,
                "height": 41.98309
            }
        },
        {
            "c": "ht.Node",
            "i": 272,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 271
                },
                "tag": "92-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -1040.42066,
                    "y": -172.10772
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 48.57633,
                "height": 41.98309
            }
        },
        {
            "c": "ht.Node",
            "i": 273,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 271
                },
                "tag": "92-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -1049.69863,
                    "y": -181.45404
                },
                "width": 48.34422,
                "height": 39.01408
            }
        },
        {
            "c": "ht.Block",
            "i": 274,
            "p": {
                "displayName": "L07",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -166.2785,
                    "y": -381.51883
                },
                "width": 265,
                "height": 232.05571
            }
        },
        {
            "c": "ht.Text",
            "i": 275,
            "p": {
                "parent": {
                    "__i": 274
                },
                "position": {
                    "x": -143.98154,
                    "y": -294.38811
                },
                "width": 116.05661,
                "height": 57.79426
            },
            "s": {
                "text": "冷干机07",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 276,
            "p": {
                "displayName": "冷干机07",
                "parent": {
                    "__i": 274
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -214.73782,
                    "y": -463.64033
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 180.95932,
                "height": 154.8886
            }
        },
        {
            "c": "ht.Block",
            "i": 277,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 274
                },
                "position": {
                    "x": -79.86718,
                    "y": -406.89016
                },
                "width": 76.1934,
                "height": 74.85751
            }
        },
        {
            "c": "ht.Node",
            "i": 278,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 277
                },
                "tag": "101-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -75.85948,
                    "y": -403.6158
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 42.85879,
                "height": 41.61186
            }
        },
        {
            "c": "ht.Node",
            "i": 279,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 277
                },
                "tag": "101-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -79.86718,
                    "y": -406.44973
                },
                "width": 76.1934,
                "height": 73.97665
            }
        },
        {
            "c": "ht.Node",
            "i": 280,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 277
                },
                "tag": "101-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -81.05771,
                    "y": -407.33059
                },
                "width": 61.90714,
                "height": 73.97665
            }
        },
        {
            "c": "ht.Node",
            "i": 281,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 274
                },
                "tag": "101-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -233.2998,
                    "y": -391.06554
                },
                "width": 130.95741,
                "height": 92.47081
            }
        },
        {
            "c": "ht.Block",
            "i": 282,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 274
                },
                "position": {
                    "x": -105.03522,
                    "y": -480.78636
                },
                "width": 41.66827,
                "height": 33.52067
            }
        },
        {
            "c": "ht.Node",
            "i": 283,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 282
                },
                "tag": "101-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -97.5716,
                    "y": -473.50601
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.66827,
                "height": 33.52067
            }
        },
        {
            "c": "ht.Node",
            "i": 284,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 282
                },
                "tag": "101-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -105.03522,
                    "y": -480.81206
                },
                "width": 38.89027,
                "height": 30.4975
            }
        },
        {
            "c": "ht.Block",
            "i": 285,
            "p": {
                "displayName": "G07",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -324.91937,
                    "y": -257.78163
                },
                "width": 345.20138,
                "height": 198.38256
            }
        },
        {
            "c": "ht.Shape",
            "i": 286,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 285
                },
                "position": {
                    "x": -228.13953,
                    "y": -313.43935
                },
                "width": 151.6417,
                "height": 87.06711,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -303.96039,
                            "y": -269.9058
                        },
                        {
                            "x": -303.96039,
                            "y": -269.9058
                        },
                        {
                            "x": -250.20844,
                            "y": -300.76819
                        },
                        {
                            "x": -212.29802,
                            "y": -322.53497
                        },
                        {
                            "x": -174.3876,
                            "y": -344.30175
                        },
                        {
                            "x": -152.31868,
                            "y": -356.9729
                        },
                        {
                            "x": -152.31868,
                            "y": -356.9729
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 287,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 285
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -304.3036,
                    "y": -277.17838
                },
                "anchor": {
                    "x": 0.58321,
                    "y": 0.5
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 288,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 285
                },
                "position": {
                    "x": -406.51161,
                    "y": -210.84407
                },
                "width": 182.01689,
                "height": 104.50743,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -497.52005,
                            "y": -158.59035
                        },
                        {
                            "x": -497.52005,
                            "y": -158.59035
                        },
                        {
                            "x": -433.00112,
                            "y": -195.63476
                        },
                        {
                            "x": -387.49689,
                            "y": -221.76162
                        },
                        {
                            "x": -341.99267,
                            "y": -247.88847
                        },
                        {
                            "x": -315.50316,
                            "y": -263.09778
                        },
                        {
                            "x": -315.50316,
                            "y": -263.09778
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 289,
            "p": {
                "displayName": "K07",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -497.39706,
                    "y": -139.02324
                },
                "width": 493.98133,
                "height": 353.36586
            }
        },
        {
            "c": "ht.Text",
            "i": 290,
            "p": {
                "parent": {
                    "__i": 289
                },
                "position": {
                    "x": -538.11349,
                    "y": 1.73241
                },
                "width": 145.28728,
                "height": 71.85457
            },
            "s": {
                "text": "空压机07",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 291,
            "p": {
                "displayName": "空压机07",
                "parent": {
                    "__i": 289
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -523.51628,
                    "y": -149.16219
                },
                "width": 268.26722,
                "height": 258.67646
            }
        },
        {
            "c": "ht.Block",
            "i": 292,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 289
                },
                "position": {
                    "x": -441.94249,
                    "y": -203.46143
                },
                "width": 95.3839,
                "height": 93.85071
            }
        },
        {
            "c": "ht.Node",
            "i": 293,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 292
                },
                "tag": "93-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -436.92539,
                    "y": -198.99963
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.65344,
                "height": 51.73529
            }
        },
        {
            "c": "ht.Node",
            "i": 294,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 292
                },
                "tag": "93-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -441.94249,
                    "y": -202.523
                },
                "width": 95.3839,
                "height": 91.97385
            }
        },
        {
            "c": "ht.Node",
            "i": 295,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 292
                },
                "tag": "93-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -442.97302,
                    "y": -204.39986
                },
                "width": 77.49942,
                "height": 91.97385
            }
        },
        {
            "c": "ht.Block",
            "i": 296,
            "p": {
                "displayName": "D07",
                "parent": {
                    "__i": 289
                },
                "position": {
                    "x": -441.52078,
                    "y": -108.54825
                },
                "width": 97.00174,
                "height": 143.49949
            }
        },
        {
            "c": "ht.Text",
            "i": 297,
            "p": {
                "parent": {
                    "__i": 296
                },
                "position": {
                    "x": -433.83776,
                    "y": -57.92565
                },
                "width": 81.63571,
                "height": 42.25428
            },
            "s": {
                "text": "电表07",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 298,
            "p": {
                "displayName": "电表07",
                "parent": {
                    "__i": 296
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -445.26686,
                    "y": -124.28861
                },
                "width": 89.50959,
                "height": 112.01878
            }
        },
        {
            "c": "ht.Node",
            "i": 299,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 289
                },
                "tag": "93-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -662.41719,
                    "y": -142.38215
                },
                "width": 163.94108,
                "height": 114.96732
            }
        },
        {
            "c": "ht.Node",
            "i": 300,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 289
                },
                "tag": "179-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -331.4064,
                    "y": -169.59984
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 301,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 289
                },
                "position": {
                    "x": -484.97388,
                    "y": -294.86834
                },
                "width": 52.16307,
                "height": 41.67565
            }
        },
        {
            "c": "ht.Node",
            "i": 302,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 301
                },
                "tag": "93-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -475.63043,
                    "y": -285.8168
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 52.16307,
                "height": 41.67565
            }
        },
        {
            "c": "ht.Node",
            "i": 303,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 301
                },
                "tag": "93-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -484.97388,
                    "y": -294.9003
                },
                "width": 48.68539,
                "height": 37.917
            }
        },
        {
            "c": "ht.Block",
            "i": 304,
            "p": {
                "displayName": "L08",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": 6.63065,
                    "y": -282.95373
                },
                "width": 265,
                "height": 234.09424
            }
        },
        {
            "c": "ht.Text",
            "i": 305,
            "p": {
                "parent": {
                    "__i": 304
                },
                "position": {
                    "x": 33.05873,
                    "y": -195.87024
                },
                "width": 115.74896,
                "height": 59.92727
            },
            "s": {
                "text": "冷干机08",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 306,
            "p": {
                "displayName": "冷干机08",
                "parent": {
                    "__i": 304
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": -41.34898,
                    "y": -365.66584
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 180.47963,
                "height": 160.60509
            }
        },
        {
            "c": "ht.Block",
            "i": 307,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 304
                },
                "position": {
                    "x": 91.39606,
                    "y": -303.17838
                },
                "width": 75.99142,
                "height": 77.62028
            }
        },
        {
            "c": "ht.Node",
            "i": 308,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 307
                },
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": 95.39312,
                    "y": -299.78317
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 42.74518,
                "height": 43.14764
            }
        },
        {
            "c": "ht.Node",
            "i": 309,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 307
                },
                "tag": "102-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": 91.39606,
                    "y": -302.72169
                },
                "width": 75.99142,
                "height": 76.70691
            }
        },
        {
            "c": "ht.Node",
            "i": 310,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 307
                },
                "tag": "102-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": 89.02132,
                    "y": -303.63507
                },
                "width": 61.74303,
                "height": 76.70691
            }
        },
        {
            "c": "ht.Node",
            "i": 311,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 304
                },
                "tag": "102-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -60.56422,
                    "y": -293.13333
                },
                "width": 130.61026,
                "height": 95.88364
            }
        },
        {
            "c": "ht.Block",
            "i": 312,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 304
                },
                "position": {
                    "x": 68.24242,
                    "y": -382.62194
                },
                "width": 41.55781,
                "height": 34.75782
            }
        },
        {
            "c": "ht.Node",
            "i": 313,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 312
                },
                "tag": "102-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 75.68626,
                    "y": -375.07289
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.55781,
                "height": 34.75782
            }
        },
        {
            "c": "ht.Node",
            "i": 314,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 312
                },
                "tag": "102-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 68.24242,
                    "y": -382.6486
                },
                "width": 38.78717,
                "height": 31.62307
            }
        },
        {
            "c": "ht.Block",
            "i": 315,
            "p": {
                "displayName": "G08",
                "parent": {
                    "__i": 153
                },
                "position": {
                    "x": -346.3822,
                    "y": -44.85562
                },
                "width": 735.1312,
                "height": 422.26597
            }
        },
        {
            "c": "ht.Shape",
            "i": 316,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 315
                },
                "position": {
                    "x": -53.58682,
                    "y": -213.05828
                },
                "width": 149.54043,
                "height": 85.86063,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -128.35704,
                            "y": -170.12797
                        },
                        {
                            "x": -128.35704,
                            "y": -170.12797
                        },
                        {
                            "x": -75.34993,
                            "y": -200.56269
                        },
                        {
                            "x": -37.96483,
                            "y": -222.02785
                        },
                        {
                            "x": -0.57973,
                            "y": -243.49301
                        },
                        {
                            "x": 21.18339,
                            "y": -255.98859
                        },
                        {
                            "x": 21.18339,
                            "y": -255.98859
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 317,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 315
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": -135.42507,
                    "y": -173.9237
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 318,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 315
                },
                "position": {
                    "x": -428.23716,
                    "y": 2.23279
                },
                "width": 571.42128,
                "height": 328.08916,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -713.9478,
                            "y": 166.27737
                        },
                        {
                            "x": -713.9478,
                            "y": 166.27737
                        },
                        {
                            "x": -511.39796,
                            "y": 49.98069
                        },
                        {
                            "x": -368.54264,
                            "y": -32.04162
                        },
                        {
                            "x": -225.68733,
                            "y": -114.06391
                        },
                        {
                            "x": -142.52652,
                            "y": -161.81179
                        },
                        {
                            "x": -142.52652,
                            "y": -161.81179
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 319,
            "p": {
                "displayName": "K08",
                "parent": {
                    "__i": 153
                },
                "dataBindings": {
                    "p": {}
                },
                "position": {
                    "x": -715.68535,
                    "y": 166.64367
                },
                "width": 494.14828,
                "height": 361.95534
            }
        },
        {
            "c": "ht.Text",
            "i": 320,
            "p": {
                "parent": {
                    "__i": 319
                },
                "position": {
                    "x": -757.75025,
                    "y": 311.42902
                },
                "width": 144.96182,
                "height": 72.38464
            },
            "s": {
                "text": "空压机08",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 321,
            "p": {
                "displayName": "空压机08",
                "parent": {
                    "__i": 319
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -743.18574,
                    "y": 159.42127
                },
                "width": 267.66627,
                "height": 260.58471
            }
        },
        {
            "c": "ht.Block",
            "i": 322,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 319
                },
                "position": {
                    "x": -668.97543,
                    "y": 109.58129
                },
                "width": 95.17023,
                "height": 93.7556
            }
        },
        {
            "c": "ht.Node",
            "i": 323,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 322
                },
                "tag": "94-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -663.96956,
                    "y": 113.68227
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.53325,
                "height": 52.11694
            }
        },
        {
            "c": "ht.Node",
            "i": 324,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 322
                },
                "tag": "94-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -668.97543,
                    "y": 110.13292
                },
                "width": 95.17023,
                "height": 92.65234
            }
        },
        {
            "c": "ht.Node",
            "i": 325,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 322
                },
                "tag": "94-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -668.97543,
                    "y": 109.02967
                },
                "width": 77.32581,
                "height": 92.65234
            }
        },
        {
            "c": "ht.Block",
            "i": 326,
            "p": {
                "displayName": "D08",
                "parent": {
                    "__i": 319
                },
                "position": {
                    "x": -656.84765,
                    "y": 199.86875
                },
                "width": 101.58137,
                "height": 150.73588
            }
        },
        {
            "c": "ht.Text",
            "i": 327,
            "p": {
                "parent": {
                    "__i": 326
                },
                "position": {
                    "x": -646.78338,
                    "y": 253.9537
                },
                "width": 81.45284,
                "height": 42.56599
            },
            "s": {
                "text": "电表08",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 328,
            "p": {
                "displayName": "电表08",
                "parent": {
                    "__i": 326
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -662.98379,
                    "y": 180.92339
                },
                "width": 89.30908,
                "height": 112.84514
            }
        },
        {
            "c": "ht.Block",
            "i": 329,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 319
                },
                "position": {
                    "x": -707.63833,
                    "y": 6.65754
                },
                "width": 52.04622,
                "height": 41.98309
            }
        },
        {
            "c": "ht.Node",
            "i": 330,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 329
                },
                "tag": "94-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -698.31581,
                    "y": 15.77584
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 52.04622,
                "height": 41.98309
            }
        },
        {
            "c": "ht.Node",
            "i": 331,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 329
                },
                "tag": "94-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -707.63833,
                    "y": 6.62534
                },
                "width": 48.57633,
                "height": 38.19671
            }
        },
        {
            "c": "ht.Node",
            "i": 332,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 319
                },
                "tag": "94-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -880.97257,
                    "y": 163.19624
                },
                "width": 163.57383,
                "height": 115.81542
            }
        },
        {
            "c": "ht.Node",
            "i": 333,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 319
                },
                "tag": "168-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": -549.61121,
                    "y": 150.77306
                },
                "anchor": {
                    "x": 0.5,
                    "y": 0.57025
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Node",
            "i": 334,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 132
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 377.45126,
                    "y": -919.98018
                },
                "anchor": {
                    "x": 0.52077,
                    "y": 0.65659
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Data",
            "i": 335,
            "p": {
                "displayName": "电表",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Data",
            "i": 336,
            "p": {
                "displayName": "储蓄罐出气标段",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Shape",
            "i": 337,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": -236.74539,
                    "y": -1044.88063
                },
                "width": 172.95581,
                "height": 99.30489,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -323.2233,
                            "y": -995.22819
                        },
                        {
                            "x": -323.2233,
                            "y": -995.22819
                        },
                        {
                            "x": -261.91622,
                            "y": -1030.42847
                        },
                        {
                            "x": -218.67727,
                            "y": -1055.25469
                        },
                        {
                            "x": -175.43831,
                            "y": -1080.08091
                        },
                        {
                            "x": -150.26749,
                            "y": -1094.53308
                        },
                        {
                            "x": -150.26749,
                            "y": -1094.53308
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null,
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 338,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": -69.44716,
                    "y": -944.88042
                },
                "width": 172.95581,
                "height": 99.30489,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -155.92506,
                            "y": -895.22797
                        },
                        {
                            "x": -155.92506,
                            "y": -895.22797
                        },
                        {
                            "x": -94.61798,
                            "y": -930.42825
                        },
                        {
                            "x": -51.37903,
                            "y": -955.25448
                        },
                        {
                            "x": -8.14008,
                            "y": -980.0807
                        },
                        {
                            "x": 17.03075,
                            "y": -994.53286
                        },
                        {
                            "x": 17.03075,
                            "y": -994.53286
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 339,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": 103.50865,
                    "y": -846.75391
                },
                "width": 172.95581,
                "height": 99.30489,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 17.03075,
                            "y": -797.10147
                        },
                        {
                            "x": 17.03075,
                            "y": -797.10147
                        },
                        {
                            "x": 78.33783,
                            "y": -832.30175
                        },
                        {
                            "x": 121.57678,
                            "y": -857.12797
                        },
                        {
                            "x": 164.81573,
                            "y": -881.95419
                        },
                        {
                            "x": 189.98656,
                            "y": -896.40636
                        },
                        {
                            "x": 189.98656,
                            "y": -896.40636
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 340,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": 267.22242,
                    "y": -750.56613
                },
                "width": 168.92961,
                "height": 96.9932,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 182.75762,
                            "y": -702.06953
                        },
                        {
                            "x": 182.75762,
                            "y": -702.06953
                        },
                        {
                            "x": 242.63755,
                            "y": -736.45039
                        },
                        {
                            "x": 284.86995,
                            "y": -760.69869
                        },
                        {
                            "x": 327.10235,
                            "y": -784.94699
                        },
                        {
                            "x": 351.68723,
                            "y": -799.06273
                        },
                        {
                            "x": 351.68723,
                            "y": -799.06273
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 341,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": 437.70821,
                    "y": -649.2364
                },
                "width": 163.98958,
                "height": 94.15681,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 355.71343,
                            "y": -602.158
                        },
                        {
                            "x": 355.71343,
                            "y": -602.158
                        },
                        {
                            "x": 413.84228,
                            "y": -635.53346
                        },
                        {
                            "x": 454.83967,
                            "y": -659.07266
                        },
                        {
                            "x": 495.83706,
                            "y": -682.61186
                        },
                        {
                            "x": 519.70301,
                            "y": -696.31481
                        },
                        {
                            "x": 519.70301,
                            "y": -696.31481
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 342,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": 471.87026,
                    "y": -667.22727
                },
                "width": 431.23183,
                "height": 375.5568,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 485.37841,
                            "y": -479.44888
                        },
                        {
                            "x": 687.48618,
                            "y": -595.49175
                        },
                        {
                            "x": 256.25434,
                            "y": -855.00568
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 343,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 336
                },
                "position": {
                    "x": 29.46659,
                    "y": -971.69732
                },
                "scale": {
                    "x": 1,
                    "y": -1
                },
                "width": 678.02897,
                "height": 240.49752,
                "segments": {
                    "__a": [
                        1,
                        2,
                        4,
                        4,
                        4,
                        4,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -309.54789,
                            "y": -943.39251
                        },
                        {
                            "x": -154.58526,
                            "y": -851.44856
                        },
                        {
                            "x": -154.58526,
                            "y": -851.44856
                        },
                        {
                            "x": -35.9021,
                            "y": -920.4172
                        },
                        {
                            "x": 60.04209,
                            "y": -975.85824
                        },
                        {
                            "x": 86.18946,
                            "y": -990.96742
                        },
                        {
                            "x": 110.64799,
                            "y": -1005.0719
                        },
                        {
                            "x": 130.55518,
                            "y": -1016.50188
                        },
                        {
                            "x": 159.24865,
                            "y": -1032.97662
                        },
                        {
                            "x": 229.21498,
                            "y": -1073.14872
                        },
                        {
                            "x": 233.31541,
                            "y": -1075.50303
                        },
                        {
                            "x": 250.52325,
                            "y": -1085.38314
                        },
                        {
                            "x": 261.95368,
                            "y": -1091.94607
                        },
                        {
                            "x": 261.95368,
                            "y": -1091.94607
                        },
                        {
                            "x": 368.48108,
                            "y": -1027.77498
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Data",
            "i": 344,
            "p": {
                "displayName": "储气罐",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Block",
            "i": 345,
            "p": {
                "displayName": "储气罐01",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": -342.84,
                    "y": -1017.70105
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 346,
            "p": {
                "displayName": "储气罐01",
                "parent": {
                    "__i": 345
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": -342.84,
                    "y": -1028.78271
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 347,
            "p": {
                "parent": {
                    "__i": 345
                },
                "position": {
                    "x": -334.85708,
                    "y": -919.82072
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐01",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 348,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 345
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": -344.00786,
                    "y": -1036.72665
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Block",
            "i": 349,
            "p": {
                "displayName": "储气罐02",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": -169.63504,
                    "y": -916.83459
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 350,
            "p": {
                "displayName": "储气罐02",
                "parent": {
                    "__i": 349
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": -169.63504,
                    "y": -927.91624
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 351,
            "p": {
                "parent": {
                    "__i": 349
                },
                "position": {
                    "x": -161.65212,
                    "y": -818.95425
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐02",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 352,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 349
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": -167.74835,
                    "y": -937.07253
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Block",
            "i": 353,
            "p": {
                "displayName": "储气罐03",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": 1.56974,
                    "y": -813.17132
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 354,
            "p": {
                "displayName": "储气罐03",
                "parent": {
                    "__i": 353
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 1.56974,
                    "y": -824.25297
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 355,
            "p": {
                "parent": {
                    "__i": 353
                },
                "position": {
                    "x": 9.55266,
                    "y": -715.29098
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐03",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 356,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 353
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 2.04654,
                    "y": -832.08319
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Block",
            "i": 357,
            "p": {
                "displayName": "储气罐04",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": 174.7747,
                    "y": -717.70042
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 358,
            "p": {
                "displayName": "储气罐04",
                "parent": {
                    "__i": 357
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 174.7747,
                    "y": -728.78207
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 359,
            "p": {
                "parent": {
                    "__i": 357
                },
                "position": {
                    "x": 182.75762,
                    "y": -619.82008
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐04",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 360,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 357
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 175.07864,
                    "y": -738.8414
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Block",
            "i": 361,
            "p": {
                "displayName": "储气罐05",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": 347.97966,
                    "y": -616.83395
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 362,
            "p": {
                "displayName": "储气罐05",
                "parent": {
                    "__i": 361
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 347.97966,
                    "y": -627.9156
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 363,
            "p": {
                "parent": {
                    "__i": 361
                },
                "position": {
                    "x": 355.96258,
                    "y": -518.95361
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐05",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 364,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 361
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 348.83,
                    "y": -635.6389
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Block",
            "i": 365,
            "p": {
                "displayName": "储气罐06",
                "parent": {
                    "__i": 344
                },
                "position": {
                    "x": 522.20469,
                    "y": -515.96747
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 366,
            "p": {
                "displayName": "储气罐06",
                "parent": {
                    "__i": 365
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 522.20469,
                    "y": -527.04913
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Text",
            "i": 367,
            "p": {
                "parent": {
                    "__i": 365
                },
                "position": {
                    "x": 530.18761,
                    "y": -418.08714
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐06",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 368,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 365
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 523.4947,
                    "y": -534.9979
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Data",
            "i": 369,
            "p": {
                "displayName": "工厂段",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Block",
            "i": 370,
            "p": {
                "displayName": "工厂01",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 360.37098,
                    "y": -1427.09147
                },
                "width": 236.54107,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 371,
            "p": {
                "parent": {
                    "__i": 370
                },
                "position": {
                    "x": 429.89961,
                    "y": -1435.49356
                },
                "width": 97.48381
            },
            "s": {
                "text": "冰箱第一工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 372,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 370
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 344.10044,
                    "y": -1427.09147
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory.gif"
            }
        },
        {
            "c": "ht.Block",
            "i": 373,
            "p": {
                "displayName": "LL01",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 227.59856,
                    "y": -1319.68018
                },
                "width": 219.35979,
                "height": 171.08854
            }
        },
        {
            "c": "ht.Text",
            "i": 374,
            "p": {
                "parent": {
                    "__i": 373
                },
                "position": {
                    "x": 244.9473,
                    "y": -1259.13591
                },
                "width": 97.48381
            },
            "s": {
                "text": "流量计01",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 375,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 373
                },
                "position": {
                    "x": 294.04468,
                    "y": -1330.65327
                },
                "width": 86.46755,
                "height": 49.6465,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 250.81091,
                            "y": -1305.83002
                        },
                        {
                            "x": 250.81091,
                            "y": -1305.83002
                        },
                        {
                            "x": 281.46077,
                            "y": -1323.42806
                        },
                        {
                            "x": 303.07766,
                            "y": -1335.83968
                        },
                        {
                            "x": 324.69455,
                            "y": -1348.2513
                        },
                        {
                            "x": 337.27845,
                            "y": -1355.47652
                        },
                        {
                            "x": 337.27845,
                            "y": -1355.47652
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 376,
            "p": {
                "displayName": "流量计01",
                "parent": {
                    "__i": 373
                },
                "image": "assets/素材包/海尔/流量计.png",
                "position": {
                    "x": 236.92106,
                    "y": -1327.8584
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Node",
            "i": 377,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 373
                },
                "tag": "111-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 172.91867,
                    "y": -1347.33913
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 378,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 373
                },
                "position": {
                    "x": 239.92106,
                    "y": -1390.72445
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 379,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 378
                },
                "tag": "111-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 246.19026,
                    "y": -1384.42594
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 380,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 378
                },
                "tag": "111-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 239.92106,
                    "y": -1390.74669
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 381,
            "p": {
                "displayName": "LL05",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 1161.13752,
                    "y": -735.4208
                },
                "width": 241.1336,
                "height": 170.18547
            }
        },
        {
            "c": "ht.Text",
            "i": 382,
            "p": {
                "parent": {
                    "__i": 381
                },
                "position": {
                    "x": 1166.59959,
                    "y": -675.32807
                },
                "width": 97.48381
            },
            "s": {
                "text": "流量计05",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 383,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 381
                },
                "position": {
                    "x": 1227.08376,
                    "y": -753.38331
                },
                "width": 109.24113,
                "height": 62.72225,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 1172.4632,
                            "y": -722.02219
                        },
                        {
                            "x": 1172.4632,
                            "y": -722.02219
                        },
                        {
                            "x": 1211.18554,
                            "y": -744.25514
                        },
                        {
                            "x": 1238.49582,
                            "y": -759.9357
                        },
                        {
                            "x": 1265.80611,
                            "y": -775.61627
                        },
                        {
                            "x": 1281.70433,
                            "y": -784.74444
                        },
                        {
                            "x": 1281.70433,
                            "y": -784.74444
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 384,
            "p": {
                "displayName": "流量计05",
                "parent": {
                    "__i": 381
                },
                "image": "assets/素材包/海尔/流量计.png",
                "position": {
                    "x": 1158.57335,
                    "y": -746.05056
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Node",
            "i": 385,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 381
                },
                "tag": "116-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 1095.57072,
                    "y": -761.13334
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 386,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 381
                },
                "position": {
                    "x": 1169.07097,
                    "y": -806.01354
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 387,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 386
                },
                "tag": "116-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 1175.34017,
                    "y": -799.71503
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 388,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 386
                },
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 1169.07097,
                    "y": -806.03578
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 389,
            "p": {
                "displayName": "工厂02",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 665.61676,
                    "y": -1327.8584
                },
                "width": 245.11742,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 390,
            "p": {
                "parent": {
                    "__i": 389
                },
                "position": {
                    "x": 739.43356,
                    "y": -1306.74122
                },
                "width": 97.48381
            },
            "s": {
                "text": "冰箱第二工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 391,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 389
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 645.05805,
                    "y": -1327.8584
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory01.gif"
            }
        },
        {
            "c": "ht.Block",
            "i": 392,
            "p": {
                "displayName": "LL02",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 488.82344,
                    "y": -1178.82426
                },
                "width": 269.76236,
                "height": 170.02974
            }
        },
        {
            "c": "ht.Text",
            "i": 393,
            "p": {
                "parent": {
                    "__i": 392
                },
                "position": {
                    "x": 478.97087,
                    "y": -1118.80939
                },
                "width": 97.48381
            },
            "s": {
                "text": "流量计02",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 394,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 392
                },
                "position": {
                    "x": 554.26955,
                    "y": -1205.37057
                },
                "width": 138.87013,
                "height": 79.73414,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 484.83448,
                            "y": -1165.5035
                        },
                        {
                            "x": 484.83448,
                            "y": -1165.5035
                        },
                        {
                            "x": 534.05932,
                            "y": -1193.7666
                        },
                        {
                            "x": 568.77685,
                            "y": -1213.70014
                        },
                        {
                            "x": 603.49439,
                            "y": -1233.63367
                        },
                        {
                            "x": 623.70461,
                            "y": -1245.23765
                        },
                        {
                            "x": 623.70461,
                            "y": -1245.23765
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 395,
            "p": {
                "displayName": "流量计02",
                "parent": {
                    "__i": 392
                },
                "image": "assets/素材包/海尔/流量计.png",
                "position": {
                    "x": 470.94463,
                    "y": -1189.53188
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Shape",
            "i": 396,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 392
                },
                "position": {
                    "x": 418.68464,
                    "y": -1128.95448
                },
                "width": 87.37574,
                "height": 50.16795,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 374.99677,
                            "y": -1103.8705
                        },
                        {
                            "x": 374.99677,
                            "y": -1103.8705
                        },
                        {
                            "x": 405.96856,
                            "y": -1121.65337
                        },
                        {
                            "x": 427.81249,
                            "y": -1134.19536
                        },
                        {
                            "x": 449.65643,
                            "y": -1146.73735
                        },
                        {
                            "x": 462.37251,
                            "y": -1154.03845
                        },
                        {
                            "x": 462.37251,
                            "y": -1154.03845
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 397,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 392
                },
                "tag": "112-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 422.36006,
                    "y": -1209.33913
                },
                "anchor": {
                    "x": 0.62198,
                    "y": 0.5
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 398,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 392
                },
                "position": {
                    "x": 477.80416,
                    "y": -1249.33913
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 399,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 398
                },
                "tag": "112-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 484.07336,
                    "y": -1243.04062
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 400,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 398
                },
                "tag": "112-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 477.80416,
                    "y": -1249.36137
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 401,
            "p": {
                "displayName": "工厂03",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 1037.22681,
                    "y": -1255.24772
                },
                "width": 258.74556,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 402,
            "p": {
                "parent": {
                    "__i": 401
                },
                "position": {
                    "x": 1117.85768,
                    "y": -1244.67187
                },
                "width": 97.48381
            },
            "s": {
                "text": "冰箱第三工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 403,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 401
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 1009.85403,
                    "y": -1255.24772
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory.gif"
            }
        },
        {
            "c": "ht.Block",
            "i": 404,
            "p": {
                "displayName": "LL03",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 808.45394,
                    "y": -1073.28866
                },
                "width": 384.64852,
                "height": 221.017
            }
        },
        {
            "c": "ht.Text",
            "i": 405,
            "p": {
                "parent": {
                    "__i": 404
                },
                "position": {
                    "x": 778.77595,
                    "y": -1013.00416
                },
                "width": 97.48381
            },
            "s": {
                "text": "流量计03",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 406,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 404
                },
                "position": {
                    "x": 892.70888,
                    "y": -1121.74772
                },
                "width": 216.13864,
                "height": 124.09889,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 784.63956,
                            "y": -1059.69827
                        },
                        {
                            "x": 784.63956,
                            "y": -1059.69827
                        },
                        {
                            "x": 861.25352,
                            "y": -1103.6872
                        },
                        {
                            "x": 915.28818,
                            "y": -1134.71192
                        },
                        {
                            "x": 969.32284,
                            "y": -1165.73664
                        },
                        {
                            "x": 1000.7782,
                            "y": -1183.79716
                        },
                        {
                            "x": 1000.7782,
                            "y": -1183.79716
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 407,
            "p": {
                "displayName": "流量计03",
                "parent": {
                    "__i": 404
                },
                "image": "assets/素材包/海尔/流量计.png",
                "position": {
                    "x": 770.74972,
                    "y": -1083.72665
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Shape",
            "i": 408,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 404
                },
                "position": {
                    "x": 689.93851,
                    "y": -1005.15849
                },
                "width": 147.61765,
                "height": 84.75665,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 616.12968,
                            "y": -962.78016
                        },
                        {
                            "x": 616.12968,
                            "y": -962.78016
                        },
                        {
                            "x": 668.45523,
                            "y": -992.82358
                        },
                        {
                            "x": 705.35964,
                            "y": -1014.01273
                        },
                        {
                            "x": 742.26405,
                            "y": -1035.20189
                        },
                        {
                            "x": 763.74733,
                            "y": -1047.53681
                        },
                        {
                            "x": 763.74733,
                            "y": -1047.53681
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 409,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 404
                },
                "tag": "113-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 708.63439,
                    "y": -1105.50315
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 410,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 404
                },
                "position": {
                    "x": 778.77595,
                    "y": -1143.80939
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 411,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 410
                },
                "tag": "113-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 785.04515,
                    "y": -1137.51088
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 412,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 410
                },
                "tag": "113-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 778.77595,
                    "y": -1143.83163
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 413,
            "p": {
                "displayName": "工厂04",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 1197.24574,
                    "y": -1066.77787
                },
                "width": 266.20779,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 414,
            "p": {
                "parent": {
                    "__i": 413
                },
                "position": {
                    "x": 1281.60773,
                    "y": -1044.01348
                },
                "width": 97.48381
            },
            "s": {
                "text": "洗衣机第一工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 415,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 413
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 1166.14184,
                    "y": -1066.77787
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory.gif"
            }
        },
        {
            "c": "ht.Block",
            "i": 416,
            "p": {
                "displayName": "LL04",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 1002.27906,
                    "y": -909.89986
                },
                "width": 292.59673,
                "height": 173.94621
            }
        },
        {
            "c": "ht.Text",
            "i": 417,
            "p": {
                "displayName": "流量计08",
                "parent": {
                    "__i": 416
                },
                "position": {
                    "x": 981.00932,
                    "y": -848.26884
                },
                "width": 97.48381
            },
            "s": {
                "text": "流量计08",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Shape",
            "i": 418,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 416
                },
                "position": {
                    "x": 1067.72518,
                    "y": -941.38536
                },
                "width": 161.7045,
                "height": 92.8448,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 986.87293,
                            "y": -894.96296
                        },
                        {
                            "x": 986.87293,
                            "y": -894.96296
                        },
                        {
                            "x": 1044.19179,
                            "y": -927.87335
                        },
                        {
                            "x": 1084.61792,
                            "y": -951.08455
                        },
                        {
                            "x": 1125.04404,
                            "y": -974.29575
                        },
                        {
                            "x": 1148.57742,
                            "y": -987.80776
                        },
                        {
                            "x": 1148.57742,
                            "y": -987.80776
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 419,
            "p": {
                "displayName": "流量计08",
                "parent": {
                    "__i": 416
                },
                "image": "assets/素材包/海尔/流量计.png",
                "position": {
                    "x": 972.98308,
                    "y": -918.99134
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Shape",
            "i": 420,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 416
                },
                "position": {
                    "x": 913.83985,
                    "y": -852.86413
                },
                "width": 104.2817,
                "height": 59.87474,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 861.69899,
                            "y": -822.92675
                        },
                        {
                            "x": 861.69899,
                            "y": -822.92675
                        },
                        {
                            "x": 898.66339,
                            "y": -844.15036
                        },
                        {
                            "x": 924.73382,
                            "y": -859.11904
                        },
                        {
                            "x": 950.80424,
                            "y": -874.08772
                        },
                        {
                            "x": 965.9807,
                            "y": -882.80149
                        },
                        {
                            "x": 965.9807,
                            "y": -882.80149
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 421,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 416
                },
                "tag": "119-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 910.9807,
                    "y": -939.16083
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 422,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 416
                },
                "position": {
                    "x": 983.2782,
                    "y": -982.37297
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 423,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 422
                },
                "tag": "119-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 989.5474,
                    "y": -976.07446
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 424,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 422
                },
                "tag": "119-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 983.2782,
                    "y": -982.39521
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 425,
            "p": {
                "displayName": "工厂05",
                "parent": {
                    "__i": 369
                },
                "position": {
                    "x": 1329.07855,
                    "y": -864.05887
                },
                "width": 263.66562,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 426,
            "p": {
                "parent": {
                    "__i": 425
                },
                "position": {
                    "x": 1412.16945,
                    "y": -844.70189
                },
                "width": 97.48381
            },
            "s": {
                "text": "洗衣机第二工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 427,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 425
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 1299.24574,
                    "y": -864.05887
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory01.gif"
            }
        },
        {
            "c": "ht.Data",
            "i": 428,
            "p": {
                "displayName": "储蓄罐进气标段",
                "parent": {
                    "__i": 132
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Shape",
            "i": 429,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": -329.13639,
                    "y": -830.18272
                },
                "width": 283.64937,
                "height": 99.6383,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -470.96107,
                            "y": -780.36357
                        },
                        {
                            "x": -372.43964,
                            "y": -837.34315
                        },
                        {
                            "x": -319.80358,
                            "y": -807.73774
                        },
                        {
                            "x": -187.3117,
                            "y": -880.00187
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 430,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": -426.34376,
                    "y": -942.52571
                },
                "width": 132.80673,
                "height": 76.25276,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -492.74712,
                            "y": -904.39933
                        },
                        {
                            "x": -492.74712,
                            "y": -904.39933
                        },
                        {
                            "x": -445.67155,
                            "y": -931.42839
                        },
                        {
                            "x": -412.46987,
                            "y": -950.49158
                        },
                        {
                            "x": -379.26819,
                            "y": -969.55477
                        },
                        {
                            "x": -359.94039,
                            "y": -980.65208
                        },
                        {
                            "x": -359.94039,
                            "y": -980.65208
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 431,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": -150.7011,
                    "y": -730.13473
                },
                "width": 285.92914,
                "height": 106.37724,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -293.66567,
                            "y": -676.9461
                        },
                        {
                            "x": -194.6005,
                            "y": -733.97321
                        },
                        {
                            "x": -142.39602,
                            "y": -704.09229
                        },
                        {
                            "x": -7.73653,
                            "y": -783.32335
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 432,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": 18.51372,
                    "y": -634.97302
                },
                "width": 274.00313,
                "height": 109.94359,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -118.48784,
                            "y": -580.00123
                        },
                        {
                            "x": -118.48784,
                            "y": -580.00123
                        },
                        {
                            "x": -77.7991,
                            "y": -601.19666
                        },
                        {
                            "x": -32.92307,
                            "y": -626.96282
                        },
                        {
                            "x": -28.90264,
                            "y": -629.27121
                        },
                        {
                            "x": -21.31862,
                            "y": -633.62567
                        },
                        {
                            "x": -21.31862,
                            "y": -633.62567
                        },
                        {
                            "x": 18.10331,
                            "y": -610.76654
                        },
                        {
                            "x": 155.51529,
                            "y": -689.94482
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 433,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": 192.26508,
                    "y": -534.52957
                },
                "width": 274.00313,
                "height": 109.94359,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 55.26351,
                            "y": -479.55777
                        },
                        {
                            "x": 55.26351,
                            "y": -479.55777
                        },
                        {
                            "x": 95.95225,
                            "y": -500.7532
                        },
                        {
                            "x": 140.82828,
                            "y": -526.51937
                        },
                        {
                            "x": 144.84871,
                            "y": -528.82775
                        },
                        {
                            "x": 152.43273,
                            "y": -533.18222
                        },
                        {
                            "x": 152.43273,
                            "y": -533.18222
                        },
                        {
                            "x": 191.85467,
                            "y": -510.32308
                        },
                        {
                            "x": 329.26664,
                            "y": -589.50136
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 434,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": 803.07301,
                    "y": -845.96814
                },
                "width": 695.61282,
                "height": 328.16901,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 455.2666,
                            "y": -966.67798
                        },
                        {
                            "x": 533.03682,
                            "y": -1010.05264
                        },
                        {
                            "x": 1098.83101,
                            "y": -681.88364
                        },
                        {
                            "x": 1150.87942,
                            "y": -711.21267
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 435,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "position": {
                    "x": -68.65943,
                    "y": -671.09911
                },
                "width": 1133.38151,
                "height": 524.35753,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -635.35018,
                            "y": -877.41335
                        },
                        {
                            "x": -537.69651,
                            "y": -933.27788
                        },
                        {
                            "x": 366.34511,
                            "y": -408.92035
                        },
                        {
                            "x": 498.03133,
                            "y": -489.12023
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 436,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 428
                },
                "rotation": 3.14159,
                "position": {
                    "x": 335.59309,
                    "y": -1111.63242
                },
                "scale": {
                    "x": 1,
                    "y": -1
                },
                "width": 392.92391,
                "height": 363.91705,
                "segments": {
                    "__a": [
                        1,
                        2,
                        4,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 280.39418,
                            "y": -929.6739
                        },
                        {
                            "x": 139.13114,
                            "y": -1010.82971
                        },
                        {
                            "x": 139.13114,
                            "y": -1010.82971
                        },
                        {
                            "x": 297.74184,
                            "y": -1104.40886
                        },
                        {
                            "x": 377.82247,
                            "y": -1150.71136
                        },
                        {
                            "x": 438.55743,
                            "y": -1185.82823
                        },
                        {
                            "x": 532.05505,
                            "y": -1239.48719
                        },
                        {
                            "x": 532.05505,
                            "y": -1239.48719
                        },
                        {
                            "x": 532.05505,
                            "y": -1239.48719
                        },
                        {
                            "x": 452.88688,
                            "y": -1286.25396
                        },
                        {
                            "x": 441.19246,
                            "y": -1293.59095
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Data",
            "i": 437,
            "p": {
                "displayName": "海尔二期"
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Shape",
            "i": 438,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 1081.08268,
                    "y": -296.09128
                },
                "width": 193.42208,
                "height": 113.12176,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 984.37164,
                            "y": -239.5304
                        },
                        {
                            "x": 1077.4346,
                            "y": -292.96374
                        },
                        {
                            "x": 1135.37829,
                            "y": -326.23289
                        },
                        {
                            "x": 1177.79372,
                            "y": -352.65216
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 439,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 1052.65503,
                    "y": -363.54246
                },
                "width": 496.98837,
                "height": 167.08374,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 804.16085,
                            "y": -333.81562
                        },
                        {
                            "x": 925.73893,
                            "y": -403.6213
                        },
                        {
                            "x": 1001.43696,
                            "y": -447.08434
                        },
                        {
                            "x": 1301.14922,
                            "y": -280.00059
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 440,
            "p": {
                "displayName": "储气罐07",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 804.16085,
                    "y": -350.16853
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 441,
            "p": {
                "displayName": "储气罐07",
                "parent": {
                    "__i": 440
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 804.16085,
                    "y": -361.25019
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Node",
            "i": 442,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 440
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 805.19628,
                    "y": -369.98765
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Text",
            "i": 443,
            "p": {
                "parent": {
                    "__i": 440
                },
                "position": {
                    "x": 812.14377,
                    "y": -252.2882
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐07",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Block",
            "i": 444,
            "p": {
                "displayName": "G09",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 569.10916,
                    "y": -189.3707
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 546.96143,
                "height": 288.10293
            }
        },
        {
            "c": "ht.Shape",
            "i": 445,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 444
                },
                "position": {
                    "x": 734.31221,
                    "y": -199.92003
                },
                "scale": {
                    "x": -1,
                    "y": 1
                },
                "width": 177.27256,
                "height": 101.7834,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 645.67594,
                            "y": -149.02834
                        },
                        {
                            "x": 645.67594,
                            "y": -149.02834
                        },
                        {
                            "x": 708.51316,
                            "y": -185.10717
                        },
                        {
                            "x": 752.83129,
                            "y": -210.55301
                        },
                        {
                            "x": 797.14943,
                            "y": -235.99886
                        },
                        {
                            "x": 822.94849,
                            "y": -250.81174
                        },
                        {
                            "x": 822.94849,
                            "y": -250.81174
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 446,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 444
                },
                "position": {
                    "x": 658.81685,
                    "y": -254.36068
                },
                "width": 247.02662,
                "height": 141.83364,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 535.30354,
                            "y": -183.44386
                        },
                        {
                            "x": 535.30354,
                            "y": -183.44386
                        },
                        {
                            "x": 622.86626,
                            "y": -233.71916
                        },
                        {
                            "x": 684.62292,
                            "y": -269.17757
                        },
                        {
                            "x": 746.37957,
                            "y": -304.63598
                        },
                        {
                            "x": 782.33017,
                            "y": -325.27749
                        },
                        {
                            "x": 782.33017,
                            "y": -325.27749
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 447,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 444
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 522.55805,
                    "y": -187.69538
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 448,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 444
                },
                "position": {
                    "x": 394.49828,
                    "y": -105.21937
                },
                "width": 237.02244,
                "height": 136.0896,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 275.98706,
                            "y": -37.17457
                        },
                        {
                            "x": 275.98706,
                            "y": -37.17457
                        },
                        {
                            "x": 360.00363,
                            "y": -85.41381
                        },
                        {
                            "x": 419.25924,
                            "y": -119.4362
                        },
                        {
                            "x": 478.51485,
                            "y": -153.4586
                        },
                        {
                            "x": 513.0095,
                            "y": -173.26417
                        },
                        {
                            "x": 513.0095,
                            "y": -173.26417
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 449,
            "p": {
                "displayName": "储气罐08",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 978.38588,
                    "y": -249.30206
                },
                "width": 95,
                "height": 225.16331
            }
        },
        {
            "c": "ht.Node",
            "i": 450,
            "p": {
                "displayName": "储气罐08",
                "parent": {
                    "__i": 449
                },
                "image": "assets/素材包/海尔/储气罐空.png",
                "position": {
                    "x": 978.38588,
                    "y": -260.38372
                },
                "width": 95,
                "height": 203
            }
        },
        {
            "c": "ht.Node",
            "i": 451,
            "p": {
                "displayName": "循环",
                "parent": {
                    "__i": 449
                },
                "image": "assets/素材包/海尔/循环.png",
                "position": {
                    "x": 978.38588,
                    "y": -270.68941
                },
                "width": 39.1267,
                "height": 40.4759
            }
        },
        {
            "c": "ht.Text",
            "i": 452,
            "p": {
                "parent": {
                    "__i": 449
                },
                "position": {
                    "x": 986.3688,
                    "y": -151.42173
                },
                "width": 54.77534,
                "height": 29.40264
            },
            "s": {
                "text": "储气罐08",
                "text.font": "20px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Block",
            "i": 453,
            "p": {
                "displayName": "G10",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 728.20576,
                    "y": -96.82454
                },
                "anchor": {
                    "x": 0.53591,
                    "y": 0.47173
                },
                "width": 480.17154,
                "height": 273.07617
            }
        },
        {
            "c": "ht.Shape",
            "i": 454,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 453
                },
                "position": {
                    "x": 824.75963,
                    "y": -153.13225
                },
                "width": 252.57962,
                "height": 145.02196,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 698.46982,
                            "y": -80.62127
                        },
                        {
                            "x": 698.46982,
                            "y": -80.62127
                        },
                        {
                            "x": 788.00089,
                            "y": -132.02673
                        },
                        {
                            "x": 851.14579,
                            "y": -168.28222
                        },
                        {
                            "x": 914.2907,
                            "y": -204.53771
                        },
                        {
                            "x": 951.04944,
                            "y": -225.64324
                        },
                        {
                            "x": 951.04944,
                            "y": -225.64324
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 455,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 453
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 685.72433,
                    "y": -84.8728
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 456,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 453
                },
                "position": {
                    "x": 573.52684,
                    "y": -11.50432
                },
                "width": 205.29788,
                "height": 117.87452,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 470.8779,
                            "y": 47.43294
                        },
                        {
                            "x": 470.8779,
                            "y": 47.43294
                        },
                        {
                            "x": 543.64916,
                            "y": 5.65034
                        },
                        {
                            "x": 594.97363,
                            "y": -23.81829
                        },
                        {
                            "x": 646.2981,
                            "y": -53.28692
                        },
                        {
                            "x": 676.17578,
                            "y": -70.44158
                        },
                        {
                            "x": 676.17578,
                            "y": -70.44158
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Data",
            "i": 457,
            "p": {
                "displayName": "离心机标段",
                "parent": {
                    "__i": 437
                }
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Block",
            "i": 458,
            "p": {
                "displayName": "LLX",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": 229.14166,
                    "y": -40.1833
                },
                "width": 265,
                "height": 226.51921
            }
        },
        {
            "c": "ht.Text",
            "i": 459,
            "p": {
                "parent": {
                    "__i": 458
                },
                "position": {
                    "x": 251.06759,
                    "y": 45.24683
                },
                "width": 115.79922,
                "height": 55.65894
            },
            "s": {
                "text": "冷干机（离心）",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 460,
            "p": {
                "displayName": "冷干机（离心）",
                "parent": {
                    "__i": 458
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": 181.08366,
                    "y": -124.61959
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 180.558,
                "height": 149.16595
            }
        },
        {
            "c": "ht.Block",
            "i": 461,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 458
                },
                "position": {
                    "x": 316.00548,
                    "y": -73.66441
                },
                "width": 76.02442,
                "height": 72.09177
            }
        },
        {
            "c": "ht.Node",
            "i": 462,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 461
                },
                "tag": "1139-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": 321.19218,
                    "y": -70.51103
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 42.76374,
                "height": 40.07443
            }
        },
        {
            "c": "ht.Node",
            "i": 463,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 461
                },
                "tag": "1139-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": 316.00548,
                    "y": -73.24025
                },
                "width": 76.02442,
                "height": 71.24344
            }
        },
        {
            "c": "ht.Node",
            "i": 464,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 461
                },
                "tag": "1139-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": 317.19336,
                    "y": -74.08858
                },
                "width": 61.76984,
                "height": 71.24344
            }
        },
        {
            "c": "ht.Node",
            "i": 465,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 458
                },
                "tag": "1139-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 161.97515,
                    "y": -56.13472
                },
                "width": 130.66697,
                "height": 89.0543
            }
        },
        {
            "c": "ht.Block",
            "i": 466,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 458
                },
                "position": {
                    "x": 286.30844,
                    "y": -137.30182
                },
                "width": 41.57586,
                "height": 32.28218
            }
        },
        {
            "c": "ht.Node",
            "i": 467,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 466
                },
                "tag": "1139-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 293.75551,
                    "y": -130.29045
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.57586,
                "height": 32.28218
            }
        },
        {
            "c": "ht.Node",
            "i": 468,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 466
                },
                "tag": "1139-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 286.30844,
                    "y": -137.32658
                },
                "width": 38.80402,
                "height": 29.37071
            }
        },
        {
            "c": "ht.Block",
            "i": 469,
            "p": {
                "displayName": "G07",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": 21.28119,
                    "y": 111.97997
                },
                "width": 439.67866,
                "height": 252.62795
            }
        },
        {
            "c": "ht.Shape",
            "i": 470,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 469
                },
                "position": {
                    "x": 184.23341,
                    "y": 18.32849
                },
                "width": 113.77421,
                "height": 65.32499,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 127.3463,
                            "y": 50.99098
                        },
                        {
                            "x": 127.3463,
                            "y": 50.99098
                        },
                        {
                            "x": 167.67548,
                            "y": 27.83544
                        },
                        {
                            "x": 196.11903,
                            "y": 11.5042
                        },
                        {
                            "x": 224.56258,
                            "y": -4.82705
                        },
                        {
                            "x": 241.12052,
                            "y": -14.33401
                        },
                        {
                            "x": 241.12052,
                            "y": -14.33401
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 471,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 469
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 117.59856,
                    "y": 48.73981
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 472,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 469
                },
                "position": {
                    "x": -44.51241,
                    "y": 149.84653
                },
                "width": 308.09146,
                "height": 176.89483,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": -198.55814,
                            "y": 238.29395
                        },
                        {
                            "x": -198.55814,
                            "y": 238.29395
                        },
                        {
                            "x": -89.34998,
                            "y": 175.59062
                        },
                        {
                            "x": -12.32711,
                            "y": 131.36691
                        },
                        {
                            "x": 64.69576,
                            "y": 87.14319
                        },
                        {
                            "x": 109.53333,
                            "y": 61.39912
                        },
                        {
                            "x": 109.53333,
                            "y": 61.39912
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 473,
            "p": {
                "displayName": "LX01",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": -118.76526,
                    "y": 198.06154
                },
                "width": 494.67665,
                "height": 356.123
            }
        },
        {
            "c": "ht.Text",
            "i": 474,
            "p": {
                "parent": {
                    "__i": 473
                },
                "position": {
                    "x": -164.85986,
                    "y": 339.94491
                },
                "width": 143.93164,
                "height": 72.35626
            },
            "s": {
                "text": "离心机01",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 475,
            "p": {
                "displayName": "离心机01",
                "parent": {
                    "__i": 473
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": -150.39885,
                    "y": 187.99676
                },
                "width": 265.76407,
                "height": 260.48255
            }
        },
        {
            "c": "ht.Block",
            "i": 476,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 473
                },
                "position": {
                    "x": -74.78397,
                    "y": 138.32848
                },
                "width": 94.49389,
                "height": 93.71883
            }
        },
        {
            "c": "ht.Node",
            "i": 477,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 476
                },
                "tag": "1137-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": -69.81368,
                    "y": 142.42786
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.15281,
                "height": 52.09651
            }
        },
        {
            "c": "ht.Node",
            "i": 478,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 476
                },
                "tag": "1137-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": -74.78397,
                    "y": 138.87989
                },
                "width": 94.49389,
                "height": 92.61602
            }
        },
        {
            "c": "ht.Node",
            "i": 479,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 476
                },
                "tag": "1137-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": -74.78397,
                    "y": 137.77708
                },
                "width": 76.77629,
                "height": 92.61602
            }
        },
        {
            "c": "ht.Block",
            "i": 480,
            "p": {
                "displayName": "D09",
                "parent": {
                    "__i": 473
                },
                "position": {
                    "x": -68.13776,
                    "y": 232.07122
                },
                "width": 107.78632,
                "height": 148.08983
            }
        },
        {
            "c": "ht.Text",
            "i": 481,
            "p": {
                "parent": {
                    "__i": 480
                },
                "position": {
                    "x": -54.68159,
                    "y": 284.84148
                },
                "width": 80.87399,
                "height": 42.5493
            },
            "s": {
                "text": "电表09",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 482,
            "p": {
                "displayName": "电表09",
                "parent": {
                    "__i": 480
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": -77.69372,
                    "y": 217.4715
                },
                "anchor": {
                    "x": 0.5,
                    "y": 0.52699
                },
                "width": 88.6744,
                "height": 112.8009
            }
        },
        {
            "c": "ht.Node",
            "i": 483,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 473
                },
                "tag": "1137-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": -284.89789,
                    "y": 193.44572
                },
                "width": 162.41138,
                "height": 115.77002
            }
        },
        {
            "c": "ht.Node",
            "i": 484,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 473
                },
                "tag": "1241-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": 47.57307,
                    "y": 162.03388
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 485,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 473
                },
                "position": {
                    "x": -111.57084,
                    "y": 40.98336
                },
                "width": 51.67635,
                "height": 41.96663
            }
        },
        {
            "c": "ht.Node",
            "i": 486,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 485
                },
                "tag": "1137-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": -102.31457,
                    "y": 50.09809
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 51.67635,
                "height": 41.96663
            }
        },
        {
            "c": "ht.Node",
            "i": 487,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 485
                },
                "tag": "1137-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": -111.57084,
                    "y": 40.95118
                },
                "width": 48.23111,
                "height": 38.18173
            }
        },
        {
            "c": "ht.Block",
            "i": 488,
            "p": {
                "displayName": "L14",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": 402.64983,
                    "y": 56.57415
                },
                "width": 265.00001,
                "height": 230.11864
            }
        },
        {
            "c": "ht.Text",
            "i": 489,
            "p": {
                "parent": {
                    "__i": 488
                },
                "position": {
                    "x": 437.71915,
                    "y": 143.14129
                },
                "width": 116.11485,
                "height": 56.98436
            },
            "s": {
                "text": "冷干机14",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 490,
            "p": {
                "displayName": "冷干机14",
                "parent": {
                    "__i": 488
                },
                "image": "assets/素材包/海尔/冷干机.png",
                "position": {
                    "x": 354.09969,
                    "y": -25.43424
                },
                "anchor": {
                    "x": 0,
                    "y": 0
                },
                "width": 181.05014,
                "height": 152.71809
            }
        },
        {
            "c": "ht.Block",
            "i": 491,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 488
                },
                "position": {
                    "x": 486.97145,
                    "y": 33.47212
                },
                "width": 76.23164,
                "height": 73.8085
            }
        },
        {
            "c": "ht.Node",
            "i": 492,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 491
                },
                "tag": "193-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": 490.98117,
                    "y": 36.7006
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 42.8803,
                "height": 41.02874
            }
        },
        {
            "c": "ht.Node",
            "i": 493,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 491
                },
                "tag": "193-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": 486.97145,
                    "y": 33.90638
                },
                "width": 76.23164,
                "height": 72.93998
            }
        },
        {
            "c": "ht.Node",
            "i": 494,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 491
                },
                "tag": "193-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": 485.78034,
                    "y": 33.03786
                },
                "width": 61.9382,
                "height": 72.93998
            }
        },
        {
            "c": "ht.Node",
            "i": 495,
            "p": {
                "displayName": "drier",
                "parent": {
                    "__i": 488
                },
                "tag": "193-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 335.66139,
                    "y": 47.58258
                },
                "width": 131.02313,
                "height": 91.17498
            }
        },
        {
            "c": "ht.Block",
            "i": 496,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 488
                },
                "position": {
                    "x": 461.47088,
                    "y": -41.9597
                },
                "width": 41.68918,
                "height": 33.05093
            }
        },
        {
            "c": "ht.Node",
            "i": 497,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 496
                },
                "tag": "193-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 468.93825,
                    "y": -34.78136
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 41.68918,
                "height": 33.05093
            }
        },
        {
            "c": "ht.Node",
            "i": 498,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 496
                },
                "tag": "193-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 461.47088,
                    "y": -41.98504
                },
                "width": 38.90978,
                "height": 30.07012
            }
        },
        {
            "c": "ht.Block",
            "i": 499,
            "p": {
                "displayName": "G08",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": 283.93785,
                    "y": 202.47145
                },
                "width": 267.31929,
                "height": 235.05846
            }
        },
        {
            "c": "ht.Shape",
            "i": 500,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 499
                },
                "position": {
                    "x": 354.71639,
                    "y": 121.04624
                },
                "width": 125.7622,
                "height": 72.20805,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 291.83528,
                            "y": 157.15027
                        },
                        {
                            "x": 291.83528,
                            "y": 157.15027
                        },
                        {
                            "x": 336.4138,
                            "y": 131.55492
                        },
                        {
                            "x": 367.85435,
                            "y": 113.50291
                        },
                        {
                            "x": 399.2949,
                            "y": 95.4509
                        },
                        {
                            "x": 417.59748,
                            "y": 84.94222
                        },
                        {
                            "x": 417.59748,
                            "y": 84.94222
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 501,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 499
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 285.56278,
                    "y": 152.50082
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 502,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 499
                },
                "position": {
                    "x": 213.70307,
                    "y": 242.18398
                },
                "width": 126.84973,
                "height": 155.6334,
                "segments": {
                    "__a": [
                        1,
                        4,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 277.12793,
                            "y": 320.00068
                        },
                        {
                            "x": 262.736,
                            "y": 312.10822
                        },
                        {
                            "x": 150.2782,
                            "y": 240.68368
                        },
                        {
                            "x": 150.2782,
                            "y": 240.68368
                        },
                        {
                            "x": 277.12793,
                            "y": 164.36728
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 503,
            "p": {
                "displayName": "K14",
                "parent": {
                    "__i": 457
                },
                "position": {
                    "x": 334.58943,
                    "y": 337.39372
                },
                "width": 505.58761,
                "height": 356.27855
            }
        },
        {
            "c": "ht.Text",
            "i": 504,
            "p": {
                "parent": {
                    "__i": 503
                },
                "position": {
                    "x": 285.56278,
                    "y": 479.76414
                },
                "width": 143.61614,
                "height": 71.53771
            },
            "s": {
                "text": "空压机14",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 505,
            "p": {
                "displayName": "空压机14",
                "parent": {
                    "__i": 503
                },
                "image": "assets/素材包/海尔/空压机.png",
                "position": {
                    "x": 299.9921,
                    "y": 329.53495
                },
                "width": 265.76407,
                "height": 260.48255
            }
        },
        {
            "c": "ht.Block",
            "i": 506,
            "p": {
                "displayName": "状态灯",
                "parent": {
                    "__i": 503
                },
                "position": {
                    "x": 377.54306,
                    "y": 272.45557
                },
                "width": 94.28677,
                "height": 93.1825
            }
        },
        {
            "c": "ht.Node",
            "i": 507,
            "p": {
                "displayName": "灰色按钮",
                "parent": {
                    "__i": 506
                },
                "tag": "190-status-2",
                "image": "assets/素材包/海尔/开关灰.png",
                "position": {
                    "x": 382.50245,
                    "y": 276.77051
                },
                "anchor": {
                    "x": 0.59573,
                    "y": 0.5789
                },
                "width": 53.03631,
                "height": 51.50715
            }
        },
        {
            "c": "ht.Node",
            "i": 508,
            "p": {
                "displayName": "红色按钮",
                "parent": {
                    "__i": 506
                },
                "tag": "190-status-1",
                "image": "assets/素材包/海尔/开关红.png",
                "position": {
                    "x": 377.54306,
                    "y": 273.26268
                },
                "width": 94.28677,
                "height": 91.56827
            }
        },
        {
            "c": "ht.Node",
            "i": 509,
            "p": {
                "displayName": "绿色按钮",
                "parent": {
                    "__i": 506
                },
                "tag": "190-status-0",
                "image": "assets/素材包/海尔/开关绿.png",
                "position": {
                    "x": 377.54306,
                    "y": 272.17233
                },
                "width": 76.77629,
                "height": 92.61602
            }
        },
        {
            "c": "ht.Block",
            "i": 510,
            "p": {
                "displayName": "D10",
                "parent": {
                    "__i": 503
                },
                "position": {
                    "x": 387.52066,
                    "y": 371.82991
                },
                "width": 103.8453,
                "height": 144.33077
            }
        },
        {
            "c": "ht.Text",
            "i": 511,
            "p": {
                "parent": {
                    "__i": 510
                },
                "position": {
                    "x": 392.95269,
                    "y": 423.27252
                },
                "width": 92.98124,
                "height": 41.44555
            },
            "s": {
                "text": "电表10",
                "text.font": "14px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 512,
            "p": {
                "displayName": "电表10",
                "parent": {
                    "__i": 510
                },
                "image": "assets/素材包/海尔/电表.png",
                "position": {
                    "x": 379.93522,
                    "y": 356.06497
                },
                "width": 88.6744,
                "height": 112.8009
            }
        },
        {
            "c": "ht.Node",
            "i": 513,
            "p": {
                "displayName": "compressor",
                "parent": {
                    "__i": 503
                },
                "tag": "190-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 174.83136,
                    "y": 348.97172
                },
                "anchor": {
                    "x": 0.57284,
                    "y": 0.63997
                },
                "width": 162.41138,
                "height": 115.77002
            }
        },
        {
            "c": "ht.Node",
            "i": 514,
            "p": {
                "displayName": "eMeter",
                "parent": {
                    "__i": 503
                },
                "tag": "369-board",
                "image": "symbols/空压机/电表数据.json",
                "position": {
                    "x": 506.38324,
                    "y": 314.21314
                },
                "width": 162,
                "height": 115
            }
        },
        {
            "c": "ht.Block",
            "i": 515,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 503
                },
                "position": {
                    "x": 334.58944,
                    "y": 180.00038
                },
                "width": 51.56308,
                "height": 41.49187
            }
        },
        {
            "c": "ht.Node",
            "i": 516,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 515
                },
                "tag": "190-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 343.82541,
                    "y": 189.012
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 51.56308,
                "height": 41.49187
            }
        },
        {
            "c": "ht.Node",
            "i": 517,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 515
                },
                "tag": "190-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 334.58944,
                    "y": 179.96856
                },
                "width": 48.12539,
                "height": 37.74979
            }
        },
        {
            "c": "ht.Node",
            "i": 518,
            "p": {
                "displayName": "阀门",
                "parent": {
                    "__i": 437
                },
                "image": "assets/素材包/海尔/阀门.png",
                "position": {
                    "x": 1301.14922,
                    "y": -283.54931
                },
                "anchor": {
                    "x": 0.5,
                    "y": 0.58166
                },
                "scale": {
                    "x": -1,
                    "y": 1
                },
                "width": 41,
                "height": 52
            }
        },
        {
            "c": "ht.Shape",
            "i": 519,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 1483.14771,
                    "y": -131.21334
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 185.87659,
                "height": 192.7401,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 1576.086,
                            "y": -34.84329
                        },
                        {
                            "x": 1390.20941,
                            "y": -137.06572
                        },
                        {
                            "x": 1536.25778,
                            "y": -227.58339
                        },
                        {
                            "x": 1536.25778,
                            "y": -227.58339
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Shape",
            "i": 520,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 1320.09989,
                    "y": -204.8736
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 146.04837,
                "height": 138.31383,
                "segments": {
                    "__a": [
                        1,
                        2,
                        2,
                        2
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 1329.09974,
                            "y": -135.71669
                        },
                        {
                            "x": 1247.0757,
                            "y": -183.51284
                        },
                        {
                            "x": 1393.12407,
                            "y": -274.03052
                        },
                        {
                            "x": 1393.12407,
                            "y": -274.03052
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Block",
            "i": 521,
            "p": {
                "displayName": "LL06",
                "parent": {
                    "__i": 437
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1111.85403,
                    "y": -96.63754
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 376.09181,
                "height": 288.53759
            }
        },
        {
            "c": "ht.Text",
            "i": 522,
            "p": {
                "parent": {
                    "__i": 521
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1249.0613,
                    "y": -92.14181
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 101.6773,
                "height": 52.15086
            },
            "s": {
                "text": "流量计06",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 523,
            "p": {
                "displayName": "流量计06",
                "parent": {
                    "__i": 521
                },
                "image": "assets/素材包/海尔/流量计.png",
                "rotation": 3.14159,
                "position": {
                    "x": 1240.68979,
                    "y": -165.9066
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Shape",
            "i": 524,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 521
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1078.59723,
                    "y": -41.2428
                },
                "width": 309.57791,
                "height": 177.74829,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 923.80828,
                            "y": 47.63135
                        },
                        {
                            "x": 923.80828,
                            "y": 47.63135
                        },
                        {
                            "x": 1033.54334,
                            "y": -15.37451
                        },
                        {
                            "x": 1110.93782,
                            "y": -59.81158
                        },
                        {
                            "x": 1188.3323,
                            "y": -104.24865
                        },
                        {
                            "x": 1233.38619,
                            "y": -130.11694
                        },
                        {
                            "x": 1233.38619,
                            "y": -130.11694
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 525,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 521
                },
                "tag": "117-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 1177.79372,
                    "y": -185.64323
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 526,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 521
                },
                "position": {
                    "x": 1251.80856,
                    "y": -226.40666
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 527,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 526
                },
                "tag": "117-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 1258.07776,
                    "y": -220.10815
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 528,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 526
                },
                "tag": "117-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 1251.80856,
                    "y": -226.4289
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 529,
            "p": {
                "displayName": "工厂06",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 920.54649,
                    "y": 21.40016
                },
                "width": 248.54108,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 530,
            "p": {
                "parent": {
                    "__i": 529
                },
                "position": {
                    "x": 996.07512,
                    "y": 12.99806
                },
                "width": 97.48381
            },
            "s": {
                "text": "热水器第一工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 531,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 529
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 898.27595,
                    "y": 21.40016
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory.gif"
            }
        },
        {
            "c": "ht.Block",
            "i": 532,
            "p": {
                "displayName": "LL07",
                "parent": {
                    "__i": 437
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1385.62697,
                    "y": -44.03752
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 204.43881,
                "height": 189.09936
            }
        },
        {
            "c": "ht.Text",
            "i": 533,
            "p": {
                "parent": {
                    "__i": 532
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1437.00786,
                    "y": 7.29574
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 101.6773,
                "height": 52.15086
            },
            "s": {
                "text": "流量计07",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 534,
            "p": {
                "displayName": "流量计07",
                "parent": {
                    "__i": 532
                },
                "image": "assets/素材包/海尔/流量计.png",
                "rotation": 3.14159,
                "position": {
                    "x": 1428.63636,
                    "y": -66.46905
                },
                "scale": {
                    "x": -1,
                    "y": -1
                },
                "width": 45,
                "height": 94
            }
        },
        {
            "c": "ht.Shape",
            "i": 535,
            "p": {
                "displayName": "管道",
                "parent": {
                    "__i": 532
                },
                "rotation": 3.14159,
                "position": {
                    "x": 1352.68283,
                    "y": 9.55943
                },
                "width": 138.55003,
                "height": 81.90527,
                "segments": {
                    "__a": [
                        1,
                        4,
                        4
                    ]
                },
                "points": {
                    "__a": [
                        {
                            "x": 1283.40781,
                            "y": 50.51206
                        },
                        {
                            "x": 1283.40781,
                            "y": 50.51206
                        },
                        {
                            "x": 1332.9227,
                            "y": 19.72751
                        },
                        {
                            "x": 1367.40393,
                            "y": -0.07035
                        },
                        {
                            "x": 1401.88517,
                            "y": -19.86821
                        },
                        {
                            "x": 1421.95784,
                            "y": -31.39321
                        },
                        {
                            "x": 1421.95784,
                            "y": -31.39321
                        }
                    ]
                }
            },
            "s": {
                "shape.background": null,
                "shape.border.width": 10,
                "shape.dash": true,
                "shape.dash.pattern": [
                    20,
                    40
                ],
                "shape.dash.3d": true,
                "shape.dash.3d.color": "rgb(247,247,247)",
                "note.border.color": "rgb(61,61,61)",
                "border.color": "rgb(143,143,143)",
                "shape.border.color": "rgb(0,101,201)",
                "shape.dash.color": "#929292",
                "shape.border.3d": true,
                "shape.border.3d.color": null
            }
        },
        {
            "c": "ht.Node",
            "i": 536,
            "p": {
                "displayName": "flowMeter",
                "parent": {
                    "__i": 532
                },
                "tag": "118-board",
                "image": "symbols/空压机/面板.json",
                "position": {
                    "x": 1366.33286,
                    "y": -82.54174
                },
                "width": 110,
                "height": 80
            }
        },
        {
            "c": "ht.Block",
            "i": 537,
            "p": {
                "displayName": "wifi信号对",
                "parent": {
                    "__i": 532
                },
                "position": {
                    "x": 1443.41135,
                    "y": -124.08731
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 538,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 537
                },
                "tag": "118-online",
                "image": "assets/素材包/海尔/wifi.png",
                "position": {
                    "x": 1449.68055,
                    "y": -117.7888
                },
                "anchor": {
                    "x": 0.67912,
                    "y": 0.71719
                },
                "width": 35,
                "height": 29
            }
        },
        {
            "c": "ht.Node",
            "i": 539,
            "p": {
                "displayName": "wifioff",
                "parent": {
                    "__i": 537
                },
                "tag": "118-offline",
                "image": "assets/素材包/海尔/wifioff02.png",
                "position": {
                    "x": 1443.41135,
                    "y": -124.10955
                },
                "width": 32.66657,
                "height": 26.38454
            }
        },
        {
            "c": "ht.Block",
            "i": 540,
            "p": {
                "displayName": "工厂07",
                "parent": {
                    "__i": 437
                },
                "position": {
                    "x": 1272.54383,
                    "y": 16.34653
                },
                "width": 241.94572,
                "height": 267
            }
        },
        {
            "c": "ht.Text",
            "i": 541,
            "p": {
                "parent": {
                    "__i": 540
                },
                "position": {
                    "x": 1344.77478,
                    "y": 34.1209
                },
                "width": 97.48381
            },
            "s": {
                "text": "热水器第二工厂",
                "text.font": "18px arial, sans-serif",
                "text.align": "center",
                "text.color": "#fff",
                "2d.visible": false
            }
        },
        {
            "c": "ht.Node",
            "i": 542,
            "p": {
                "displayName": "厂房",
                "parent": {
                    "__i": 540
                },
                "image": "symbols/html/image.json",
                "position": {
                    "x": 1253.57097,
                    "y": 16.34653
                },
                "width": 204,
                "height": 267
            },
            "a": {
                "imageURL": "instance/storage/assets/factory.gif"
            }
        },
        {
            "c": "ht.Data",
            "i": 543,
            "p": {
                "displayName": "面板"
            },
            "s": {
                "editor.folder": true
            }
        },
        {
            "c": "ht.Data",
            "i": 544,
            "p": {
                "displayName": "wifi信号",
                "parent": {
                    "__i": 543
                }
            },
            "s": {
                "editor.folder": true
            }
        }
    ],
    "contentRect": {
        "x": -1660.16211,
        "y": -1560.59147,
        "width": 3244.24812,
        "height": 2076.12446
    }
}
