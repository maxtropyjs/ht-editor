import Cache from './cache';

const initial = {};
const LocalCache = new Cache('localStorage', initial);

export default LocalCache;
