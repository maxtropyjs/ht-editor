
class Cache {
  constructor(type, initialValue) {
    this.storage = window[type];
    this.cache = initialValue;
  }

  get(key, valueType) {
    const value = this.storage.getItem(key);
    if (valueType === 'object') {
      const result = JSON.parse(value || '{}');
      this.cache[key] = result;
      return result;
    } else {
      const result = value || '';
      Reflect.set(this.cache, key, result);
      return result;
    }
  }

  set(key, value, valueType) {
    this.cache[key] = value;
    if (valueType === 'object') {
      this.storage.setItem(key, JSON.stringify(value || {}));
    } else {
      this.storage.setItem(key, '' + value);
    }
  }

  remove(key) {
    this.storage.removeItem(key);
  }
}

export default Cache;
