// use localStorage to store the authority info, which might be sent from server in actual project.
export function getAuthority() {
  // return localStorage.getItem('antd-pro-authority') || ['admin', 'user'];
  return localStorage.getItem('ht-authority') || ['guest'];
}

export function setAuthority(authority) {
  return localStorage.setItem('ht-authority', authority);
}

export function removeAuthority() {
  return localStorage.removeItem('ht-authority');
}
