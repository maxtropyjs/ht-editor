module.exports = {
  userTitle: [
    {
      title: '企业名称',
      key: 'uCompany',
      dataIndex: 'uCompany',
      width:"20%"
    },
    {
      title: '账号（用户名）',
      key: 'uName',
      dataIndex: 'uName',
      width:"20%"
    },
    {
      title: '界面数量',
      key: 'uPageNum',
      dataIndex: 'uPageNum',
      width:"15%"
    },
    {
      title: '是否停用',
      key: 'isEnabled',
      dataIndex: 'isEnabled',
      width:"15%"
    },
    {
      title: '用户识别码',
      key: 'uCode',
      dataIndex: 'uCode',
      width:"15%"
    }
  ]
};
