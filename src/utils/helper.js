

// 无特殊字符
export function noSpecialCharacter() {
  return /^[\u4e00-\u9fa5a-zA-Z0-9]+$/;
}

export function nameRegex() {
  return /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/;
}

export function hostRegex() {
  return /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3})$|^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]))$/;
}

export function characterAndNumberRegex() {
  return /^[A-Za-z0-9]+$/;
}

export function phoneRegex() {
  return /^(\d{3,4})[-](\d{7,8})$/;
}

export function faxRegex() {
  return /^(\d{3,4})[-](\d{7,8})$/;
}

export function mobileRegex() {
  return /^1(3|4|5|7|8)\d{9}$/;
}

export function emailRegex() {
  return /\S+@\S+\.\S+/;
}

export function weChatRegex() {
  return /^[A-Za-z0-9-_]+$/;
}

export function distributionIdRegex() {
  return /^[A-F0-9]{1,18}$/;
}

export function addressFieldRegex() {
  return /^[A-F0-9]+$/;
}
