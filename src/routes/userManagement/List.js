import React from 'react';
import {
  Button,
  Divider,
  Input,
  Popconfirm,
  Pagination,
  Breadcrumb,
  Switch,
  Tooltip,
  message
} from 'antd';
import { Link } from 'react-router-dom';
import { stringify } from "qs";
import DetailTable from '../../components/usermanagement/TableComponent';
import { userTitle } from '../../utils/titles';
import { getUsers, deleteUsers } from '../../services/user';
import './list.less';

class List extends React.Component {
  state = {
    queryParams: {
      pageSize: 10,
      pageNum: 1,
      queryValue: ''
    },
    data: [],
    total: ''
  };

  componentDidMount() {
    this.getUser();
  }

  getUser = () => {
    const { queryParams } = this.state;
    getUsers(queryParams)
      .then(data => {
        console.log(data);
        this.setState({
          data: data.data.list.map(item => ({
            ...item,
            isEnabled: item.isEnabled == 1 ? <Switch disabled checked /> : <Switch disabled checked={false}/>,
            uCompany: item.uCompany ? <Tooltip title={item.uCompany }>
                                        <span>{item.uCompany}</span>
                                      </Tooltip>: '--',
            uCode: item.uCode ? <Tooltip title={item.uCode }>
                                  <span>{item.uCode}</span>
                                </Tooltip>: '--',
            uName: item.uName ? <Tooltip title={item.uName }>
                                  <span>{item.uName}</span>
                                </Tooltip>: '--',
            uPageNum: item.uPageNum ? <Tooltip title={item.uPageNum }>
                                        <span>{item.uPageNum}</span>
                                      </Tooltip>: '--',
          })),
          total: data.data.total
        });
      })
      .catch(e => {
        // console.log(e)
      });
  };

  changeValue = event => {
    let that = this;
    let data = this.state.queryParams;
    this.setState(
      {
        queryParams: {
          ...data,
          queryValue: event.target.value
        }
      },
      () => {
        //这里打印的是最新的state值
        console.log(that.state.queryParams);
      }
    );
  };

  // 搜索
  searchList = () => {
    let data = this.state.queryParams;
    this.setState(
      {
        queryParams: {
          ...data,
          pageSize: 10,
          pageNum: 1
        }
      },
      () => {
        this.getUser();
      }
    );
  };
  onPressEnter = () => {
    this.searchList()
  }

  // 删除
  onDelete = id => {
    let params = {
      id: id
    };
    const list = this.state.data;
    deleteUsers(params)
      .then(data => {
        console.log(JSON.parse(data).success)
        if(JSON.parse(data).success){
          this.getUser()
        }else{
          message.error(JSON.parse(data).message);
        }

        // console.log(data)
        // this.setState({data: list.filter(item => item.id !== id)});
        // console.log(this.state.data)
      })
      .catch(e => {
        // console.log(e)
      });
  };

  // 进入账号
  enterHome = record => {};

  // 操作
  action = record => {
    console.log('record')
    console.log(record)
    console.log('record')
    if (record.uName.props.title == 'superadmin') {
      return (
        <Link to={'/'+record.id+'/dashboard'}>进入账号</Link>
      );
    } else {
      return (
        <span>
          <Link
            to={{
              pathname: '/editoruser',
              query: record,
              state: { status: true }
            }}
          >
            <span>编辑</span>
          </Link>
          <Divider type="vertical" />
          <Popconfirm title="删除?" onConfirm={() => this.onDelete(record.id)}>
            <a href="javascript:;">删除</a>
          </Popconfirm>
          <Divider type="vertical" />




          <Link to={{
            pathname: '/'+record.id+'/dashboard',
            search: stringify({
              from: 'dashboard',
            }),
          }}>
            进入账号
          </Link>
          {/*<a href="javascript:;" onClick={() => this.enterHome()}>*/}

          {/*</a>*/}
        </span>
      );
    }
  };

  //分页
  onChange = (page, pageSize) => {
    let that = this;
    let data = this.state.queryParams;
    this.setState(
      {
        queryParams: {
          ...data,
          pageSize: pageSize,
          pageNum: page
        }
      },
      () => {
        //这里打印的是最新的state值
        console.log(that.state.queryParams);
        this.getUser();
      }
    );
  };
  onShowSizeChange = (current, size) => {
    let that = this;
    let data = this.state.queryParams;
    this.setState(
      {
        queryParams: {
          ...data,
          pageSize: size,
          pageNum: current
        }
      },
      () => {
        //这里打印的是最新的state值
        this.getUser();
      }
    );
  };

  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <div className="usermanagement">
        <Breadcrumb className="breadcrumb">
          <Breadcrumb.Item>首页</Breadcrumb.Item>
          <Breadcrumb.Item>管理中心</Breadcrumb.Item>
        </Breadcrumb>
        <div className="box">
          <div className="top">
            <div>
              <span style={{ color: '#999', fontSize: 13 }}>用户名称：</span>
              <Input
                onChange={this.changeValue}
                className="input"
                placeholder="输入关键字搜索角色"
                onPressEnter={this.onPressEnter}
              />
              <Button type="primary" onClick={this.searchList} className="btn">
                查询
              </Button>
            </div>
            <Link to="/adduser" className="newBtn">
              <span className="iconAdd">+</span>
              <span className="new">新增用户</span>
            </Link>
          </div>
          {/* 表格 */}
          <DetailTable
            action={this.action}
            data={data}
            titleData={userTitle}
            classname="table"
          />
          <Pagination
            total={this.state.total}
            // showSizeChanger
            showQuickJumper
            className="pagination"
            onChange={this.onChange}
            // onShowSizeChange={this.onShowSizeChange}
          />
        </div>
      </div>
    );
  }
}

export default List;
