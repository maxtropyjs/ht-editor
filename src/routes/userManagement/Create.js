import React from 'react';
import { Form, Input, Button, Select, Switch, Breadcrumb,message } from 'antd';
import { Link } from 'react-router-dom';
import md5 from 'md5';
import { getCode, postUsers, putUsers } from '../../services/user';
import './create.less';

const { TextArea } = Input;
const { Option } = Select;

class Create extends React.Component {
  state = {
    code:''
  }

  // 获取用户识别码
  componentDidMount() {
    getCode()
      .then(data => {
        this.setState({
          code:data.data
        })
        // this.code = data.data;
        // console.log(data.data);
      })
      .catch(e => {
        // console.log(e)
      });
  }

  // 检查两次密码是否一致
  checkConfirm = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('uPassword')) {
      callback('两次输入的密码不匹配!');
    } else {
      callback();
    }
  };

  // 提交
  handleSubmit = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields({ force: true }, (err, values) => {
      console.log("err")
      console.log(err)
      console.log("err")
      if (!err) {
        const params = {
          uName: values.uName,
          uPhone: values.uPhone,
          uMail: values.uMail,
          uCompany: values.uCompany,
          isEnabled: values.isEnabled,
          uPageNum: values.uPageNum,
          uMark: values.uMark
        };
        if (this.props.history.location.pathname == '/editoruser') {
          params.id = this.props.location.query.id;
          params.uCode = this.props.location.query.uCode.props.title;
          putUsers(params)
            .then(data => {
              console.log(data);
              if (data.success) {
                this.props.history.push('/user');
              } else {
                message.error(data.message);
              }
            })
            .catch(e => {
              // console.log(e)
            });
        } else if (this.props.history.location.pathname == '/adduser') {
          params.uCode = this.state.code;
          params.uPassword = md5(values.uPassword);
          postUsers(params)
            .then(data => {
              console.log(data);
              if (data.success) {
                this.props.history.push('/user');
              } else {
                message.error(data.message);
              }
            })
            .catch(e => {
              // console.log(e)
            });
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    let initlist = {};
    let status = false;
    let title = {
      bread: '新增用户',
      hint: '创建新用户'
    };
    if (this.props.location.query) {
      initlist = this.props.location.query;
      console.log(initlist)
      status = this.props.location.state.status;
      title = {
        bread: '编辑用户',
        hint: '编辑用户'
      };
    }
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 14,
          offset: 10
        }
      }
    };
    return (
      <div className="adduser">
        <Breadcrumb className="breadcrumb">
          <Breadcrumb.Item>首页</Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/user">管理中心</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>{title.bread}</Breadcrumb.Item>
        </Breadcrumb>
        <div className="box">
          <div style={{ marginBottom: 58 }}>
            <span className="icon" />
            <p className="tit">{title.hint}</p>
          </div>
          <Form onSubmit={this.handleSubmit} className="form">
            {/* 用户名 */}
            <Form.Item
              {...formItemLayout}
              label={<span>用户名</span>}
              hasFeedback
            >
              {getFieldDecorator('uName', {
                initialValue: initlist.uName?initlist.uName.props.title:'',
                rules: [
                  {
                    required: true,
                    message: '请输入用户名!',
                    whitespace: true
                  },
                  {
                    pattern: /^[\u4E00-\u9FA5A-Za-z_-]{6,50}$/,
                    message: '长度6-50字符，含中文、英文、短横线、下划线！'
                  }
                ]
              })(<Input disabled={status} placeholder="请输入用户名" autocomplete="off"/>)}
            </Form.Item>
            {/* 设置密码 */}
            <Form.Item
              {...formItemLayout}
              label={<span>设置密码</span>}
              hasFeedback
            >
              {getFieldDecorator('uPassword', {
                initialValue: status ? '123456' : '',
                rules: [
                  { required: true, message: '请输入密码!', whitespace: true },
                  {
                    pattern: /^[A-Za-z0-9_-]{6,30}$/,
                    message: '长度6-30字符，含数字、英文、短横线、下划线'
                  }
                ]
              })(
                <Input
                  type="password"
                  disabled={status}
                  placeholder="请输入密码"
                  autocomplete="new-password"
                />
              )}
            </Form.Item>
            {/* 确认密码 */}
            <Form.Item
              {...formItemLayout}
              label={<span>确认密码</span>}
              hasFeedback
            >
              {getFieldDecorator('confirm', {
                initialValue: status ? '123456' : '',
                rules: [
                  {
                    required: true,
                    message: '请再次输入密码!',
                    whitespace: true
                  },
                  { validator: this.checkConfirm },
                  {
                    pattern: /^[A-Za-z0-9_-]{6,30}$/,
                    message: '长度6-30字符，含数字、英文、短横线、下划线'
                  }
                ]
              })(
                <Input
                  type="password"
                  disabled={status}
                  placeholder="请再次输入密码"
                />
              )}
            </Form.Item>
            {/* 手机号 */}
            <Form.Item
              {...formItemLayout}
              label={<span>手机号</span>}
              hasFeedback
            >
              {getFieldDecorator('uPhone', {
                initialValue: initlist.uPhone,
                rules: [
                  {
                    required: true,
                    message: '请输入手机号!',
                    whitespace: true
                  },
                  {
                    pattern: /^1(3|4|5|6|7|8|9)\d{9}$/,
                    message: '手机号格式错误！'
                  }
                ]
              })(<Input placeholder="请输入手机号" />)}
            </Form.Item>
            {/* 电子邮箱 */}
            <Form.Item {...formItemLayout} label={<span>电子邮箱</span>}>
              {getFieldDecorator('uMail', {
                initialValue: initlist.uMail ? initlist.uMail : '',
                rules: [
                  {
                    pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[a-zA-Z]$/,
                    message: '电子邮箱格式不正确！'
                  }
                ]
              })(<Input placeholder="请输入电子邮箱" maxLength="50"/>)}
            </Form.Item>
            {/* 企业名称 */}
            <Form.Item {...formItemLayout} hasFeedback label={<span>企业名称</span>}>
              {getFieldDecorator('uCompany', {
                initialValue: initlist.uCompany?initlist.uCompany.props.title:'',
                rules: [
                  {
                    required: true,
                    message: '请输入企业名称!',
                    whitespace: true
                  },
                  {
                    pattern: /^.{0,100}$/,
                    message: '长度不能超过100'
                  }
                ]
              })(<Input placeholder="请输入企业名称" maxLength="100"/>)}
            </Form.Item>
            {/* 是否停用 */}
            <Form.Item {...formItemLayout} label={<span>是否停用</span>}>
              {getFieldDecorator('isEnabled', {
                initialValue: initlist.isEnabled ? (initlist.isEnabled.props.checked?'1':'2') : '1',
                rules: [{ required: true }]
              })(
                <Select>
                  <Option value="1">不停用</Option>
                  <Option value="2">停用</Option>
                </Select>
              )}
            </Form.Item>
            {/* 界面数量 */}
            <Form.Item {...formItemLayout} hasFeedback label={<span>界面数量</span>}>
              {getFieldDecorator('uPageNum', {
                initialValue: initlist.uPageNum ? initlist.uPageNum.props.title : 10,
                rules: [
                  { required: true, message: '请输入界面数量!' },
                  {
                    pattern: /^[1-9]\d{0,5}$/,
                    message: '请输入1-999999的正整数！'
                  }
                ]
              })(<Input placeholder="请输入界面数量" />)}
            </Form.Item>
            {/* 用户识别码 */}
            <Form.Item {...formItemLayout} label={<span>用户识别码</span>}>
              {getFieldDecorator('uCode', {
                initialValue: initlist.uCode ? initlist.uCode.props.title : this.state.code
              })(<Input disabled />)}
            </Form.Item>
            {/* 备注 */}
            <Form.Item
              {...formItemLayout}
              label={<span>备注</span>}
              className="textarea"
            >
              {getFieldDecorator('uMark', {
                initialValue: initlist.uMark ? initlist.uMark : ''
                // rules: [
                //   {
                //     pattern: /^.{0,200}$/,
                //     message: '请输入200字以内！'
                //   }
                // ]
              })(
                <TextArea
                  disabled={status}
                  placeholder="请输入备注"
                  maxLength="200"
                />
              )}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" className="sure">
                确定
              </Button>
              <Link to="/user">
                <Button type="primary" className="cancel">
                  返回
                </Button>
              </Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

const AdduserList = Form.create()(Create);
export default AdduserList;
