import React from 'react';
import {getConfigById, createConfig, updateConfig} from '../../services/api';
import {Link} from 'react-router-dom';

class Editor extends React.Component {
    // static propTypes = {};
    static defaultProps = {};

    constructor(props) {
        console.log(props);
        super(props);
        this.state = {
            displayId: props.match.params.id || -1,
            displayValue: '',
            imageData: '',
            displayData: '',
            displayName: ''
        };
    }

    componentDidMount() {
        // window.frames.onload=function(){
        //   window.frames['ht'].postMessage('从react发来的消息','*');
        // }
        window.addEventListener('message', this.receiveMessage, false);
        if (this.state.displayId !== -1) {
            getConfigById(this.state.displayId).then(data => {
                if (data.value && data.name) {
                    this.setState({
                        displayValue: JSON.parse(data.value),
                        displayName: data.name
                    });
                }
            });
        }
    }

    receiveMessage = event => {
        const data = event.data;
        // 初始给编辑器发送要编辑的数据
        if (data === 'success') {
            window.frames['htName'].postMessage(
                {
                    data: this.state.displayValue,
                    name: this.state.displayName,
                    url: this.props.location.search.replace(/\?tag=/g, '')
                },
                '*'
            );
        }
        const {content, path} = data;
        // path大概是display/xxx.json,其中xxx代表图纸的名字
        if (path) {
            this.setState({displayName: path.split('/')[1].split('.')[0]}, () => {
                this.saveDisplay();
            });
        }
        // 传过来的data如果是以data:image开头，说明是缩略图的数据，如果是json的形式，说明是图纸或组件的数据
        if (typeof content === 'string' && content.startsWith('data:image')) {
            this.setState({imageData: content}, () => {
                this.saveDisplay();
            });
        }
        if (typeof content === 'string' && !content.startsWith('data:image')) {
            this.setState({displayData: content}, () => {
                this.saveDisplay();
            });
        }
    };

    saveDisplay = () => {
        const {imageData, displayData, displayName, displayId} = this.state;
        if (imageData !== '' && displayData !== '' && displayName !== '') {
            const obj = {
                type: 'display',
                value: displayData,
                name: displayName,
                icon: imageData
            };
            if (displayId === -1) {
                createConfig(obj).then(data => {
                    const {id} = data;
                    this.setState({displayId: id});
                });
            } else {
                updateConfig(this.state.displayId, obj).then(data => {
                    console.log(data);
                });
            }
        }
    };


    render() {
        // displayId=-1 说明是新建 否则是打开或者编辑已经存在的
        const {search} = this.props.location;
        return (
            <div>
                {/*<button onClick={this.test}>测试</button>*/}
                <Link to="/">返回列表页</Link>
                <iframe
                    title="编辑"
                    ref={dom => {
                        this.iframe = dom;
                    }}
                    id="htId"
                    name={'htName'}
                    src={search ? `/template.html?hteditor=displays/${search.replace('?', '')}` : `/template.html`}
                    style={{width: '100vw', height: 'calc(100vh - 26px)'}}
                />
            </div>
        );
    }
}

export default Editor;
