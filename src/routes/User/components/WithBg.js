import React from 'react';
import { Carousel } from 'antd';
import banner01 from '@/image/banner01.jpg';
import banner02 from '@/image/banner02.jpg';
import banner03 from '@/image/banner03.jpg';
import banner04 from '@/image/banner04.jpg';
import Logo from '../../../image/logo.svg';

const imgs = [banner01, banner02, banner03, banner04];

const WithBg = Component => {
  return class extends React.Component {
    render() {
      return (
        <div className="page-login">
          <div className="header-wrapper">
            <Carousel autoplay>
              {imgs.map(pic => {
                return (
                  <div>
                    <img className="leftImg" src={pic} />
                  </div>
                );
              })}
            </Carousel>
            <div className="title">
              <img className="titleImg" src={Logo} />
              <span>极熵科技Web组态编辑管理系统</span>
            </div>
            <div className="intro">
              <div>提供工业物联网监控平台定制 </div>
              <div className="intro-sub"> 纯 Web 在线组态</div>
            </div>
          </div>
          <Component />
        </div>
      );
    }
  };
};

export default WithBg;
