import React from 'react';
import { Input, Form, Button, Checkbox, Icon, Row, Col } from 'antd';
import { connect } from 'react-redux';
import md5 from 'md5';
import { Link } from 'react-router-dom';
import { postLogin } from '@/services/login';
import history from '@/history';
import * as Cookies from 'js-cookie';
import { getCaptchaOnWeb } from '@/services/forget';
import { setUserInfo } from '@/actions/user';
import withBg from './components/WithBg';
import Logo from '../../image/logo.svg';
import './Login.less';

const Fragment = React.Fragment;
const FormItem = Form.Item;

class LoginForm extends React.Component {
  state = {
    loading: false,
    imgCode: '',
    blockingCount: 0,
    interval: null,
    disabled: false
  };

  componentDidMount() {
    this.onRefreshClick();
  }

  onRefreshClick = () => {
    this.props.form.setFields({ imageCode: '' });
    //图片验证码有效期五分钟

    const date = Date.parse(new Date());
    // getCaptchaOnWeb().then(
    //   (data) => {
    //   this.setState({
    //      //imageCodeUUid: data.uuid,
    //      //imgCode: `/api/v0.1/validateImages/code?t=${date}`,
    //
    //     });
    //   }
    // );

    //console.log(Cookies.get('imageCodeUUid'));
    this.setState({
      //imgCode: `http://101.132.64.184:9090/api/v0.1/validateImages/code?t=${date}}`,
      //imgCode: `http://127.0.0.1:9091/api/v0.1/validateImages/code?t=${date}}`,
      //imgCode: `http://10.9.108.189:9090/api/v0.1/validateImages/code?t=${date}}`,
      imgCode: `/api/v0.1/validateImages/code?t=${date}`,
      blockingCount: 300
    });

    const interval = setInterval(() => {
      const count = this.state.blockingCount;
      this.setState({
        blockingCount: count > 0 ? count - 1 : 0
      });
      if (count === 0) {
        //验证码失效
        clearInterval(interval);
        this.props.form.setFields({
          imageCode: {
            value: '',
            errors: [new Error('验证超时，请刷新验证码后重新输入')]
          }
        });
      }
    }, 1000);

    this.setState({
      interval
    });
  };

  remember = e => {
    this.setState({ isRemember: e.target.checked });
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.setState({ loading: true });
        try {
          const formData = new FormData();
          formData.append('username', values.userName.trim());
          formData.append('password', md5(values.password));
          formData.append('imageCode', values.imageCode);
          //formData.append('imageCodeUUid',Cookies.get('imageCodeUUid'));
          const params = {
            username: values.userName,
            password: md5(values.password)
          };
          const res = await postLogin(params);
          console.log(res);

          if (res.success) {
            localStorage.setItem('Authorization', res.token);
            this.setState({ loading: false });
            const userInfo = {
              ...res,
              remember: values.remember,
              rememberName: res.username
            };

            this.props.setUserInfo(userInfo);
            history.push(`/${res.id}/dashboard`);
          } else {
            this.setState({ loading: false });
            if (res.code === 101) {
              this.props.form.setFields({
                userName: { value: '', errors: [new Error('该账户已经停用')] }
              });
              this.onRefreshClick();
              this.props.form.setFieldsValue({ imageCode: '' });
            }
            if (res.code === 102) {
              this.props.form.setFields({
                imageCode: {
                  value: '',
                  errors: [new Error('您输入的验证码错误')]
                }
              });
            }
            if (res.code === 103) {
              this.props.form.setFields({
                userName: { value: '', errors: [new Error('您输入的账户错误')] }
              });
              this.onRefreshClick();
              this.props.form.setFieldsValue({ imageCode: '' });
            }

            if (res.code === 104) {
              this.props.form.setFields({
                password: { value: '', errors: [new Error('您输入的密码错误')] }
              });
              this.onRefreshClick();
              this.props.form.setFieldsValue({ imageCode: '' });
            }
          }
        } catch (error) {
          this.setState({ loading: false });
          console.log(error);
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { remember, rememberName } = this.props;

    return (
      <Fragment>
        <div className="form-wrapper">
          <div className="welcometitle">
            <img src={Logo} className="welcome-img" />
            <div className="hr1"> </div>
            <span>欢迎登录</span>
          </div>
          <div className="form-container">
            <Form onSubmit={this.handleSubmit}>
              <FormItem hasFeedback>
                {getFieldDecorator('userName', {
                  initialValue: remember ? rememberName || '' : '',
                  rules: [{ required: true, message: '请输入您的账户' }]
                })(
                  <Input
                    prefix={<Icon type="user" />}
                    placeholder="请输入用户名"
                    onPressEnter={this.handleSubmit}
                  />
                )}
              </FormItem>
              <FormItem className="password" hasFeedback>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: '请输入您的密码!' }]
                })(
                  <Input
                    prefix={<Icon type="unlock" />}
                    type="password"
                    placeholder="请输入密码"
                    onPressEnter={this.handleSubmit}
                  />
                )}
              </FormItem>
              <FormItem label="" hasFeedback className="captcha">
                <Row gutter={48}>
                  <Col span={16}>
                    {getFieldDecorator('imageCode', {
                      rules: [
                        { required: true, message: '请输入四位图片验证码!' },
                        { min: 4, max: 4, message: '请输入四位图片验证码!' }
                      ]
                    })(
                      <Input
                        prefix={<Icon type="edit" />}
                        placeholder="请输入四位验证码"
                      />
                    )}
                  </Col>
                  <Col span={8}>
                    <div className="captcha-layout">
                      <img src={this.state.imgCode} alt="图片验证码" />
                      <a className="reload-link" onClick={this.onRefreshClick}>
                        刷新
                      </a>
                    </div>
                  </Col>
                </Row>
              </FormItem>
              {/*<FormItem className="captcha">*/}
              {/*  <div className="captcha-layout">*/}
              {/*    <img src={this.state.imgCode} alt="图片验证码" />*/}
              {/*    <a className="reload-link" onClick={this.onRefreshClick}>*/}
              {/*      刷新*/}
              {/*    </a>*/}
              {/*  </div>*/}
              {/*</FormItem>*/}
              <FormItem>
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: remember
                })(
                  <Checkbox className="checkbox" onChange={this.remember}>
                    {' '}
                    记住用户名{' '}
                  </Checkbox>
                )}
                <Link to="/forget" className="forget-password-link">
                  忘记密码?
                </Link>
              </FormItem>
              <Button
                type="primary"
                htmlType="submit"
                className="submit-btn"
                onClick={this.handleSubmit}
                disabled={this.state.disabled}
                loading={this.state.loading}
              >
                登录
              </Button>
            </Form>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  remember: user.remember,
  rememberName: user.rememberName
});

const mapDispatchToProps = dispatch => {
  return {
    setUserInfo: data => dispatch(setUserInfo(data))
  };
};

export default withBg(
  Form.create()(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(LoginForm)
  )
);
