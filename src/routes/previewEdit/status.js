import React from 'react';
import { Icon } from 'antd';
import classNames from 'classnames';
import TableTooltip from './TableTooltip';

export function StatusFactory(enums = []) {
  return ({ value, hasColorBg, className }) => {
    const [matched] = enums.filter(e => e.value === value);
    if (!matched) {
      return null;
    }
    let style = {};
    if (hasColorBg) {
      style = { backgroundColor: matched.color };
    } else {
      style = { color: matched.color };
    }
    return (
      <span
        // className={classNames('statusSpan', {status: matched.icon}, className)}
        style={style}
      >
        {matched.icon ? <Icon type={matched.icon} /> : null}
        <TableTooltip text={matched.label} />
      </span>
    );
  };
}

export const MachineStatus = StatusFactory(
  [
    {
      value: 0,
      label: '运行中',
      color: '#00a651',
    },
    {
      value: 1,
      label: '预警',
      color: '#f96b4a',
    },
    {
      value: 2,
      label: '未连接',
      color: '#b7b7b7',
    },
    {
      value: 3,
      label: '未连接',
      color: '#b7b7b7',
    }
  ]
);