import React from 'react';
import { Tooltip } from 'antd';
import classNames from 'classnames';
import { isArray, isNil } from 'lodash';
import Highlighter from './Highlighter';

export default function ({ text, keyword, lines = 1, ...rest }) {
  const content = keyword
    ? (<Highlighter
      className="highlight"
      searchWords={isArray(keyword) ? keyword : [keyword]}
      textToHighlight={text}
      {...rest}
    />)
    : (isNil(text) || (typeof text === 'string' && text.trim() === '') ? '--' : text);
  return (
    <Tooltip overlay={content}>
      <span className={classNames({ ellipsis: lines === 1, lineClamp: lines > 1 })}>
        {content}
      </span>
    </Tooltip>
  );
}

