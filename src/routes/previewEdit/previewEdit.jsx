import React, {Component} from 'react';
import {getConfigById,getSimpleMachine,getMachineDetail,getMachineStatus} from '../../services/api';
import TableTooltip from './TableTooltip';
import { MachineStatus } from './status.js';
import { Icon } from 'antd';

const mockMacthineStatus = [{
  "id": 15,
  "status": 0,
},{
  "id": 153,
  "status": 0,
},{
  "id": 154,
  "status": 0,
},{
  "id": 155,
  "status": 0,
},{
  "id": 156,
  "status": 0,
},{
  "id": 29,
  "status": 0,
},{
  "id": 28,
  "status": 0,
},{
  "id": 27,
  "status": 0,
},{
  "id": 26,
  "status": 0,
},{
  "id": 14,
  "status": 0,
},{
  "id": 13,
  "status": 0,
},{
  "id": 12,
  "status": 0,
},{
  "id": 11,
  "status": 0,
},{
  "id": 33,
  "status": 0,
},{
  "id": 32,
  "status": 0,
},{
  "id": 31,
  "status": 0,
},{
  "id": 30,
  "status": 0,
},{
  "id": 19,
  "status": 0,
},]

const Grid = ({items = []}) => {
  const columns = items.map((item) => {
    return (
      <dl
        key={item.id}
      >
        <dt><TableTooltip text={item.key} /></dt>
        <dd><TableTooltip text={item.value} /></dd>
      </dl>
    );
  });
  if (items.length === 0) {
    return (
      <div><Icon type="frown-o" /> 暂无数据</div>
    );
  }
  return (
    <table>
      <tbody>
        <tr>
          <td>
            {columns}
          </td>
        </tr>
      </tbody>
    </table>
  );
};


class PreviewEdit extends Component {
  static propTypes = {};
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      displayId: props.match.params.id,
      displayValue: '',
      machineDetail:'',
      
      machineStatus:''
    }
  }
  
  componentDidMount() {
    // getSimpleMachine().then((machineResp) => {
    //   this.setState({
    //     machineTypeData: machineResp.data,
    //   });
    //   console.log(machineResp)
    // });

    getConfigById(this.state.displayId).then((data) => {
      console.log(data.value);
      this.setState({displayValue: JSON.parse(data.value)})
      this.init()
    })
  };


  init() {
    const dataModel = new window.ht.DataModel();
    const graphView = new window.ht.graph.GraphView(dataModel);
    graphView.addToDOM();
    const json = window.ht.Default.parse(this.state.displayValue);
    if(json.title) document.title = json.title;
    dataModel.deserialize(json);
    graphView.fitContent(true);
    graphView.setHeight(800)
    graphView.setWidth(1000)

    graphView.getView().addEventListener('mousemove',function (e) {
      const node = graphView.getDataAt(e);
      if(node){
        window.ht.Default.showToolTip(e,node._displayName)
      }else{
        window.ht.Default.hideToolTip()
      }
    })
    graphView.each(function (htData) {
      // console.log(data.getStyleMap())
      
      getMachineStatus(htData._tag).then((data) => {
       console.log(data)
       if(data.status===0){
        htData.setStyle('shape.background','red')
       }
      })
    })
    
    
   

    graphView.onDataClicked=(data,e) => {
      // console.log(data)
      // graphView.setRectSelectBackground(this.state.value)
      // data._styleMap['shape.background']=this.state.value

      getMachineDetail(data._tag).then((data) => {
        this.setState({machineDetail:data})
      })

      getMachineStatus(data._tag).then((data) => {
        // console.log(data)
        this.setState({machineStatus:data.status})
      })
    }
  }


  render() {

    const realtimeData = this.state.machineDetail || []

      return (
      <div style={{marginLeft:'1000px'}}>
        <h4>设备详情</h4>
        <div>
          {/* <MachineImage
            machine={machine}
            secret={this.props.secret}
          /> */}
        </div>
        <div>
          <p>
            <span>设备名称：</span>
            <strong style={{color: '#afdbfb'}} >
            </strong>
          </p>
          <p>
            <span>设备状态：</span>
            <strong ><MachineStatus value={this.state.machineStatus} />
            </strong>
          </p>
        </div>
        <div>
          <Grid
            items={realtimeData.map(p => ({
              id: p.key,
              key: p.name,
              value: p.value
            }))}
          />
        </div>
      </div>
    );
  }
}

export default PreviewEdit;

