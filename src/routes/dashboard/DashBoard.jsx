import React from 'react';
import { Skeleton, Pagination, message, List, Modal } from 'antd';
import { Link, BrowserRouter } from 'react-router-dom';
import { isObject } from 'lodash';
import history from '../../history';
import GlobalFooter from '../../components/GlobalFooter';
import GlobalHeader from '../../components/GlobalHeader';
import Filters from '../../components/Filters';
import {
  getScreenNumber,
  getScreen,
  postScreen,
  deleteScreen,
  copyScreen,
  transferScreen,
  getScreenLink,
} from '../../services/screen';
import Border from '../../components/ui/Border';
import CreateForm from './Create';
import TransferForm from './Transfer';
import PublishForm from './PublishForm';
import './style/DashBoard.less';
import './style/List.less';

class DashBoard extends React.Component {
  constructor(props) {
    super(props);

    const { match: { params: { userId } } = {} } = props;
    this.state = {
      modalVisible: false,
      transferModalVisible: false,
      publishModalVisible: false,
      data: [],
      used: null,
      unused: null,
      queryParams: {
        // page: 0,
        // perPage: 15,
        pageNum: 1,
        pageSize: 7,
        userId
      },
      disabled:true,
      pageUrl:''
    };
  }

  componentDidMount() {
    const {
      queryParams: { userId }
    } = this.state;
    getScreenNumber({ userId }).then(data => {
      if (data && data.data) {
        this.setState({
          used: data.data.usedPageNum,
          unused: data.data.unusedPageNum
        });
      }
    });
    this.loadData();
  }

  loadData(query) {
    getScreen(query || this.state.queryParams).then(data => {
      console.log(data);
      if (data && data.data) {
        this.setState({
          data: data.data.list,
          total: data.data.total,
          currentPage: data.data.pageNum,
          pageSize: data.data.pageSize
        });
      }
    });
  }

  onPaginationChange = page => {
    this.onFilter('pageNum', page);
  };

  onFilter = (key, value) => {
    const queryParams = isObject(key)
      ? {
          ...this.state.queryParams,
          ...key
        }
      : key === 'pageNum'
      ? { ...this.state.queryParams, [key]: value }
      : { ...this.state.queryParams, [key]: value, pageNum: 1 };
    this.setState({ queryParams });
    this.loadData(queryParams);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag
    });
  };

  /**
   * 拷屏Modal显示隐藏
   * @param flag
   */
  handleTransferModalVisible = flag => {
    this.setState({
      transferModalVisible: !!flag,
      publishModalVisible:false
    });
  };

  handlePublishModalVisible = flag => {
    this.setState({
      publishModalVisible: !!flag
    });
  };

  /**
   * 新建
   * @param fields
   */
  handleAdd = fields => {
    const {
      queryParams: { userId }
    } = this.state;
    const values = {
      ...fields,
      userId,
    }
    postScreen(values).then(data => {
      message.success('添加成功');
      this.setState({
        modalVisible: false
      });
      history.push({
        pathname: '/editor',
        search: decodeURI(`${item.pageTitle}.json`)
      });
    });
  };

  /**
   * 拷屏
   * @param fields
   */
  handleTransfer = fields => {
    const { selectScreenId } = this.state;
    if (selectScreenId) {
      const values = {
        ...fields,
        id: selectScreenId
      };
      transferScreen(values).then(data => {
        this.setState({
          handleModalVisible: false
        });
      });
    }
  };

  transfer = id => {
    this.handleTransferModalVisible(true);
    this.setState({
      selectScreenId: id
    });
  };

  publish = id => {
    this.handlePublishModalVisible(true);
    this.setState({
      selectScreenId: id
    });
  };

  copy = id => {
    copyScreen({ id }).then(() => {
      this.loadData();
    });
  };

  remove = id => {
    deleteScreen({ id }).then(() => {
      this.loadData();
    });
  };

  confirmRemove = id => {
    Modal.warning({
      title: 'This is a warning message',
      content: 'some messages...some messages...'
    });
  };

  handleChange = value => {
    console.log(value);
  };

  /**
   * 是否发布
   * @param checked
   */
  switchPublish = (checked) => {
    if (checked) {
      this.setState({
        disabled:false
      })
      const {
        queryParams: { userId }
      } = this.state;
      getScreenLink({ userId }).then(data => {
        if (data && data.data) {
          console.log(data)
          this.setState({
            pageUrl:data.data
          })
        }
      });
    }else{
      this.setState({
        disabled:true,
        pageUrl:''
      })
    }
  };


  render() {
    const {
      data,
      total,
      currentPage,
      pageSize,
      modalVisible,
      transferModalVisible,
      publishModalVisible,
      used,
      unused,
      queryParams
    } = this.state;

    const paginationProps = {
      showQuickJumper: true,
      showTotal: total => `总共 ${total} 条`,
      pageSize: pageSize,
      total,
      current: currentPage,
      onChange: this.onPaginationChange
    };
    const projectData = data.map((item, index) => {
      console.log(item.content);
      return (
        <Border key={item.id} className="my-screen">
          <div className="screen">
            <div className="screen-info">
              <div
                className="screen-img"
                style={{ backgroundImage: `url(${item.icon})` }}
              />
              <div className="screen-edit">
                <div className="screen-button">
                  <Link
                    className="edit-wrap"
                    to={{
                      pathname: '/editor',
                      search: decodeURI(`${item.pageTitle}.json`)
                    }}
                  >
                    <button className="edit-button">
                      <span className="v-md">编辑</span>
                    </button>
                  </Link>
                  <div className="main-button">
                    <span
                      className="button-span"
                      onClick={() => this.remove(item.id)}
                    >
                      <a>删除</a>
                    </span>
                    <span
                      className="button-span"
                      onClick={() => this.copy(item.id)}
                    >
                      <a>复制</a>
                    </span>
                    <span
                      className="button-span"
                      onClick={() => this.transfer(item.id)}
                    >
                      <a>拷屏</a>
                    </span>
                    <span
                      className="button-span"
                      onClick={() => this.publish(item.id)}
                    >
                      发布
                    </span>
                    <span className="button-span">
                      <a
                        className="datav-font"
                        href={`/display.html/test?tag=displays/${item.pageTitle}.json`}
                      >
                        预览
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="screen-main">
              <div className="main-name">
                <div className="screen-name-input">
                  <input
                    className="input"
                    value={item.pageTitle}
                    onChange={this.handleChange}
                    // onKeyDown={this.handleKeyDown}
                  />
                </div>
              </div>
            </div>
          </div>
        </Border>
      );
    });

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible
    };

    const content = [
      {
        type: 'Search',
        key: 'pageTitle',
        placeholder:'界面名称'
      },
      {
        type: 'Select',
        key: 'isPublish',
        // label: '发布状态',
        placeholder:'发布状态',
        data: [
          { value: '0', name: '全部' },
          { value: '1', name: '未发布' },
          { value: '2', name: '发布' }
        ],
        style:{
          width:"240px",
        }
      }
    ];

    return (
      <div className="dashBoard">
        <GlobalHeader />
        <div className="my-project">
          <div className="project-header">
            <div className="project-title">
              <h2>可视化界面</h2>
              <span className="project-title-info">
                <em>{used}</em>
                个/还可创建
                <em>{unused}</em>个
              </span>
            </div>

            <Filters
              onFilter={this.onFilter}
              content={content}
              query={queryParams}
              className="dark-filter"
            />
          </div>
          <div className="main-screen">
            <Border className="my-screen">
              <div
                className="new-project"
                onClick={() => this.handleModalVisible(true)}
              >
                <div className="add-new-screen">
                  <i className="icon-add" />
                  <span className="span-spacing">新增组态图</span>
                </div>
              </div>
            </Border>
            {projectData}
          </div>
          <div className="pagination-bar">
            <Pagination {...paginationProps} />
          </div>
        </div>
        <GlobalFooter />
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <TransferForm
          handleTransfer={this.handleTransfer}
          handleModalVisible={this.handleTransferModalVisible}
          modalVisible={transferModalVisible}
        />
        <PublishForm
          handleTransfer={this.handleTransfer}
          handleModalVisible={this.handleTransferModalVisible}
          publishModalVisible={publishModalVisible}
          switchPublish={this.switchPublish}
          disabled={this.state.disabled}
          pageUrl={this.state.pageUrl}
        />
      </div>
    );
  }
}

export default DashBoard;
