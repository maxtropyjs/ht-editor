import React from 'react';
import { Form, Input, Modal } from 'antd';
import { nameRegex } from '@/utils/helper'

const FormItem = Form.Item;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      // form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      title="新增可视化界面"
      wrapClassName="ht-modal-wrap"
      visible={modalVisible}
      destroyOnClose
      onOk={okHandle}
      onCancel={() => handleModalVisible('addModalVisible', false)}
    >
      <FormItem
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 15 }}
        label="界面名称"
      >
        {form.getFieldDecorator('pageTitle', {
          rules: [
            { required: true, message: '请输入界面名称' },
            { pattern: nameRegex(), message: '支持中文、英文、数字和下划线'}
            ]
        })(<Input placeholder="请输入界面名称" />)}
      </FormItem>
    </Modal>
  );
});

export default CreateForm;
