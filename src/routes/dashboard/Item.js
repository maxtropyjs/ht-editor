import * as React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { Tooltip, Icon, Modal, Badge, message } from 'antd';
import { isEqual } from 'lodash';
import { stringify } from 'qs';
import { LocalCache } from '@/utils/cache';
import { nameRegex } from '@/utils/helper'
import defaultLogo from '@/assets/logo-gray.svg';
import Border from '../../components/ui/Border';
import {
  deleteScreen,
  copyScreen,
  transferScreen,
  renameScreen,
} from '../../services/screen';
import {connect} from "react-redux";

const colors = [
  'pink',
  '#a0a0a0',
  '#00abff',
];
const status = ['全部', '未发布', '已发布'];

export const Meta = (props) => {
  const {
    prefixCls = 'ant-list',
    className,
    avatar,
    title,
    description,
    ...others
  } = props;

  const classString = classNames(`${prefixCls}-item-meta`, className);

  const content = (
    <div className={`${prefixCls}-item-meta-content`}>
      {title && <h4 className={`${prefixCls}-item-meta-title`}>{title}</h4>}
      {description && <div className={`${prefixCls}-item-meta-description`}>{description}</div>}
    </div>
  );
  return (
    <div {...others} className={classString}>
      {avatar && <div className={`${prefixCls}-item-meta-avatar`}>{avatar}</div>}
      {(title || description) && content}
    </div>
  );
};

class Item extends React.Component {
  static Meta = Meta;

  constructor(props) {
    super(props);

    this.state = {
      screenTitle: props.item.pageTitle,
    };
  }

  componentWillReceiveProps(nextProps) {
    if ('item' in nextProps && !isEqual(nextProps.item, this.props.item)) {
      this.setState({
        screenTitle: nextProps.item.pageTitle,
      });
    }
  }

  transfer = item => {
    this.props.transfer(item);
  };

  publish = item => {
    this.props.publish(item);
  };

  copy = item => {
    const { userId, id } = item;
    copyScreen({ userId, id }).then((data) => {
      if (data.success) {
        this.reload();
      } else {
        Modal.error({
          title: data.message,
        });
      }
    });
  };

  remove = item => {
    const { userId, id } = item;
    Modal.confirm({
      title: '确认删除',
      content: '删除后无法恢复，确认删除？',
      okText: '确认',
      cancelText: '取消',
      onOk: () => deleteScreen({ userId, id }).then(() => {
        this.reload();
      })
    });
  };

  preview = item => {
    LocalCache.set('sid', item.userId);
    window.open(`/display.html?tag=displays/${item.pageTitle}.json`, '_blank')
  };

  reload = () => {
    this.props.reload();
  };

  /**
   * input内输入修改名字
   * @param e
   */
  handleChange = e => {
    this.setState({
      screenTitle: e.target.value,
    });
  };

  handleKeyDown = (e, item) => {
    if (e.keyCode === 13) {
      this.input.blur();
    }
  };

  /**
   * 修改名字提交
   * @param e
   */
  rename = (e, item) => {
    console.log('失去焦点')
    const { userId, id } = item;
    const pageTitle = e.target.value;
    if (nameRegex().test(pageTitle)) {
      const values = {
        id,
        userId,
        pageTitle,
      };
      renameScreen(values).then(data => {
        if (data.success) {
          message.success('修改成功');
          this.reload();
        } else {
          this.setState({
            screenTitle: item.pageTitle,
          });
        }
      });
    } else {
      this.setState({
        screenTitle: item.pageTitle,
      });
    }
  };

  render() {
    const {
      screenTitle,
    } = this.state;
    const {
      reload,
      className,
      item,
      username,
      location: { search },
      ...others
    } = this.props;
    const classString = classNames('screen__item', className);
    const isAccess = username === 'superadmin' && (item.isSuper !== 1)
    && search !== '?from=dashboard'; // admin账号下，进入企业账号。
    return (
      <Border
        {...others}
        className={classString}
      >
        <div className="screen">
          <div className="screen-info">
            <div
              className={classNames('screen-img', {
                'screen-img-no-icon': !item.icon,
              })}
              style={{ backgroundImage: item.icon ? `url(${item.icon})` : `url(${defaultLogo})` }}
            />
            {
              item.isSuper === 1 && (
                <div className="isAdmin">
                  <Icon type="user-add" />
                </div>
              )
            }
            <div className="screen-edit">
              <div className="screen-button">
                <Link
                  className="edit-wrap"
                  to={ isAccess ?
                    {
                      pathname: `/${item.userId}/dashboard`,
                      search: stringify({
                        from: 'dashboard',
                      }),
                    } :
                    {
                      pathname: '/editor',
                      search: decodeURI(`${item.pageTitle}.json`)
                    }
                  }
                >
                  <button className="edit-button">
                    <span className="v-md">{ isAccess ? '进入账号' : '编辑' }</span>
                  </button>
                </Link>
                <ul className="actions main-button">
                  <Tooltip placement="bottom" title="复制">
                    <li className="button-span" onClick={() => this.copy(item)}>
                      <Icon type="plus-square" />
                    </li>
                  </Tooltip>
                  <Tooltip placement="bottom" title="预览">
                    <li className="button-span" onClick={() => this.preview(item)}>
                      <Icon type="play-square" />
                    </li>
                  </Tooltip>
                  <Tooltip placement="bottom" title="发布">
                    <li className="button-span" onClick={() => this.publish(item)}>
                      <Icon type="cloud-upload" />
                    </li>
                  </Tooltip>
                  <Tooltip placement="bottom" title="拷屏">
                    <li className="button-span" onClick={() => this.transfer(item)}>
                      <Icon type="desktop" />
                    </li>
                  </Tooltip>
                  <Tooltip placement="bottom" title="删除">
                    <li className="button-span" onClick={() => this.remove(item)}>
                      <Icon type="delete" />
                    </li>
                  </Tooltip>
                </ul>
              </div>
            </div>
          </div>
          <div className="screen-main">
            <div className="main-name">
              <div className="screen-name-input">
                <Tooltip placement="topLeft" title={screenTitle}>
                  <Icon type="edit" />
                  <input
                    className="input"
                    value={screenTitle}
                    onChange={this.handleChange}
                    onBlur={(e) => this.rename(e, item)}
                    onKeyDown={(e) => this.handleKeyDown(e, item)}
                    ref={node => {
                      this.input = node;
                    }}
                  />
                </Tooltip>
              </div>
              <div className="publish-info">
                <Badge color={colors[item.isPublish]} text={status[item.isPublish]} />
              </div>
            </div>
          </div>
        </div>
      </Border>
    );
  }
}


const mapStateToProps = ({user}) => ({
  username: user.username,
});

export default connect(
  mapStateToProps,
)(Item);
