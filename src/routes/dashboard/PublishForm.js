import React from 'react';
import {
  Form,
  Input,
  Modal,
  Switch,
  Icon,
  Radio,
  DatePicker,
  Button, Menu, message
} from 'antd';
import moment from 'moment';
import {
  characterAndNumberRegex,
} from '../../utils/helper';
import './style/publishform.less';
import {CopyToClipboard} from "react-copy-to-clipboard";


const FormItem = Form.Item;


const PublishForm = Form.create()(props => {
  const { publishModalVisible, form, handleModalVisible, switchPublish,disabled,buttonDisable,handlePublish,defaultData,eyeType,typeInput,changeEye,onChange,inputVal} = props;
  const { getFieldDecorator } = form;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handlePublish(fieldsValue);

    });
  };

  const disabledDate = (current) => {
    return current && current < moment().endOf('day');
  };
  const onCopy = () => {
    message.info('复制成功！');
  };
  const buttonProps = {
    disabled:buttonDisable
  }



  return (
    <Modal
      title="发布可视化界面"
      wrapClassName="ht-modal-wrap"
      visible={publishModalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      className="public-form"
      okText="保存"
      destroyOnClose
      okButtonProps={buttonProps}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="发布分享" style={{marginBottom:0}}>
        <Switch onChange={switchPublish} defaultChecked={defaultData?defaultData.isClose:false}/>
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="发布链接" className="int">
        {getFieldDecorator('pageLink',{
          initialValue:defaultData?defaultData.pageLink:''
        })(
          <div>
            <Input placeholder="开启发布分享后可获取访问链接" autocomplete="off" disabled={disabled} readOnly value={defaultData.pageLink}/>
            <CopyToClipboard text={defaultData.pageLink} onCopy={onCopy}>
              <Button className="btn" disabled={disabled}>复制</Button>
            </CopyToClipboard>
          </div>
          )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="访问密码">
        {getFieldDecorator('pagePassCode', {
          initialValue:defaultData?defaultData.pagePassCode:'',
          rules: [
            { min: 6, message: '登录密码为6位数字' },
            { max: 6, message: '登录密码为6位数字' },
            { pattern: /^[0-9]{0,6}$/,
              message: '账号仅支持数字' },
          ]
        })(
          <div className="eye">
            <Input type={typeInput} placeholder="请输入访问密码" autocomplete="off" disabled={disabled} defaultValue={defaultData.pagePassCode} onChange={onChange} value={inputVal}/>
            <Icon type={eyeType} onClick={changeEye}/>
          </div>
          )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="发布有效期">
        {getFieldDecorator('pagePassCodePeriod', {
          initialValue: defaultData?defaultData.pagePassCodePeriod:moment().add(3, 'months'),
          rules: [{ required: true, message: '请选择发布有效期' }]
        })(
          <DatePicker
            format="YYYY-MM-DD"
            disabledDate={disabledDate}
            disabled={disabled}
          />)
        }
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="发布页面">
        {getFieldDecorator('isRealTime', {
          initialValue: defaultData?defaultData.isRealTime:'1'
        })(
          <Radio.Group>
            <Radio value='1' disabled={disabled}>实时页面</Radio>
            <Radio value='2' disabled={disabled}>历史快照</Radio>
          </Radio.Group>
        )}
      </FormItem>
    </Modal>
  );
});

export default PublishForm;
