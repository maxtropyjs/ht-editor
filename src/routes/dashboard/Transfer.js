import React from 'react';
import {
  Form,
  Input,
  Modal,
} from 'antd';

const FormItem = Form.Item;

// 拷屏
const TransferForm = Form.create()(props => {
  const { modalVisible, form, handleTransfer, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      handleTransfer(fieldsValue);
    });
  };
  return (
    <Modal
      title="拷屏界面给他人"
      wrapClassName="ht-modal-wrap"
      visible={modalVisible}
      destroyOnClose
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="用户识别码">
        {form.getFieldDecorator('uCode', {
          rules: [{ required: true, message: '请输入用户识别码' }],
        })(<Input placeholder="输入用户识别码，区别大小写" />)}
      </FormItem>
    </Modal>
  );
});

export default TransferForm;
