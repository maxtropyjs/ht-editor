import React from 'react';
import { Skeleton, Pagination, message, List, Modal } from 'antd';
import { Link, BrowserRouter } from 'react-router-dom';
import { isEqual, isObject } from 'lodash';
import { connect } from 'react-redux';
import { LocalCache } from '@/utils/cache';
import moment from 'moment';
import md5 from 'md5';
import { setAccessAccountInfo } from 'actions/user';
import history from '../../history';
import Filters from '../../components/Filters';

import {
  getScreenNumber,
  getScreen,
  postScreen,
  transferScreen,
  getScreenLink,
  postPublish,
  getPagePublishInfo,
  postClose
} from '../../services/screen';
import Border from '../../components/ui/Border';
import CreateForm from './Create';
import TransferForm from './Transfer';
import PublishForm from './PublishForm';
import Item from './Item';
import './style/DashBoard.less';
import './style/List.less';

class DashBoard extends React.Component {
  constructor(props) {
    super(props);

    const { match: { params: { userId } } = {} } = props;
    LocalCache.set('sid', userId);
    this.state = {
      addModalVisible: false,
      transferModalVisible: false,
      publishModalVisible: false,
      data: [],
      used: null,
      unused: null,
      currentPage: 1,
      total: 0,
      queryParams: {
        // page: 0,
        // perPage: 15,
        pageNum: 1,
        pageSize: 7,
        userId
      },
      disabled: true,
      pageUrl: '',
      id: '',
      defaultData: {
        pageLink: '',
        pagePassCode: '',
        pagePassCodePeriod: '',
        isRealTime: '1',
        isClose: false
      },
      buttonDisable: true,
      typeInput: 'password',
      eyeType: 'eye-invisible',
      inputVal: ''
    };
  }

  componentDidMount() {
    this.loadNumberData();
    this.loadData();
  }

  componentDidUpdate(preProps) {
    const { match: { params: { userId } } = {} } = this.props;
    if (userId !== preProps.match.params.userId) {
      LocalCache.set('sid', userId);
      window.scrollTo(0, 0);
      const queryParams = {
        pageNum: 1,
        pageSize: 7,
        userId
      };
      this.setState(
        {
          queryParams
        },
        () => {
          this.loadNumberData();
          this.loadData();
        }
      );
    }
  }

  loadNumberData() {
    const {
      queryParams: { userId }
    } = this.state;
    getScreenNumber({ userId }).then(data => {
      if (data && data.data) {
        this.props.setAccessAccountInfo(data.data.company);
        this.setState({
          used: data.data.usedPageNum,
          unused: data.data.unusedPageNum
        });
      }
    });
  }

  loadData(query) {
    getScreen(query || this.state.queryParams).then(data => {
      if (data && data.data) {
        this.setState({
          data: data.data.list,
          total: data.data.total,
          currentPage: data.data.pageNum,
          pageSize: data.data.pageSize
        });
      }
    });
  }

  onPaginationChange = page => {
    this.onFilter('pageNum', page);
  };

  onFilter = (key, value, isLoad = true) => {
    const queryParams = isObject(key)
      ? {
          ...this.state.queryParams,
          ...key
        }
      : key === 'pageNum'
      ? { ...this.state.queryParams, [key]: value }
      : { ...this.state.queryParams, [key]: value, pageNum: 1 };
    this.setState({ queryParams });
    if (isLoad) {
      this.loadData(queryParams);
    }
  };

  reload = () => {
    this.loadNumberData();
    this.onFilter();
  };

  handleModalVisible = (key, flag) => {
    this.setState({
      [key]: !!flag
    });
  };

  /**
   * 拷屏Modal显示隐藏
   * @param flag
   */
  handleTransferModalVisible = flag => {
    this.setState(
      {
        transferModalVisible: !!flag,
        publishModalVisible: false,
        defaultData: {
          pageLink: '',
          pagePassCode: '',
          pagePassCodePeriod: '',
          isRealTime: '1',
          isClose: false
        },
        disabled: true,
        buttonDisable: true,
        typeInput: 'password',
        eyeType: 'eye-invisible'
      },
      () => {
        console.log(this.state.defaultData);
      }
    );
    this.loadData();
  };

  handlePublishModalVisible = flag => {
    this.setState({
      publishModalVisible: !!flag
    });
  };

  add = () => {
    const { unused } = this.state;
    if (unused > 0) {
      this.handleModalVisible('addModalVisible', true);
    } else {
      Modal.error({
        title: '您的可视化界面数量已经达到上限，请扩容后再新建'
      });
    }
  };

  /**
   * 新建
   * @param fields
   */
  handleAdd = fields => {
    const {
      queryParams: { userId }
    } = this.state;
    const values = {
      ...fields,
      userId
    };
    postScreen(values).then(data => {
      if (data.success) {
        message.success('添加成功');
        this.setState({
          addModalVisible: false
        });
        history.push({
          pathname: '/editor',
          search: decodeURI(`${fields.pageTitle}.json`)
        });
      } else {
        Modal.error({
          title: data.message
        });
      }
    });
  };

  /**
   * 拷屏
   * @param fields
   */
  handleTransfer = fields => {
    const {
      selectScreen: { id, userId }
    } = this.state;
    if (id) {
      const values = {
        ...fields,
        id,
        userId
      };
      transferScreen(values).then(data => {
        if (data.success) {
          this.setState(
            {
              transferModalVisible: false
            },
            () => {
              this.reload();
            }
          );
        } else {
          Modal.error({
            title: data.message
          });
        }
      });
    }
  };

  //发布界面
  handlePublish = fieldsValue => {
    const { selectScreenId } = this.state;
    const isClose = this.state.disabled ? 1 : 2;
    fieldsValue = {
      ...fieldsValue,
      isRealTime: fieldsValue.isRealTime == '1' ? 1 : 2,
      pagePassCode: fieldsValue.pagePassCode ? fieldsValue.pagePassCode : '',
      pagePassCodePeriod: moment(fieldsValue.pagePassCodePeriod).format(
        'YYYY-MM-DD'
      )
    };
    const params = {
      ...fieldsValue,
      id: selectScreenId,
      isClose
    };

    postPublish(params).then(data => {
      if (data.success) {
        message.success('保存成功');
        this.loadData();
        this.setState({
          publishModalVisible: false,
          defaultData: {
            pageLink: '',
            pagePassCode: '',
            pagePassCodePeriod: moment().add(3, 'months'),
            isRealTime: '1',
            isClose: false
          },
          disabled: true,
          buttonDisable: true,
          typeInput: 'password',
          eyeType: 'eye-invisible'
        });
      } else {
        message.error(data.message);
      }
    });
  };

  transfer = item => {
    this.handleTransferModalVisible(true);
    this.setState({
      selectScreen: item
    });
  };

  publish = item => {
    if (item.isPublish == 2) {
      getPagePublishInfo(item.id).then(data => {
        if (data.data.isPublish == 1) {
          this.setState({
            buttonDisable: true
          });
        } else {
          this.setState({
            buttonDisable: false
          });
        }
        console.log(data.data.pagePassCode);
        this.setState(
          {
            defaultData: {
              isClose: data.data.isClose
                ? data.data.isClose == 1
                  ? false
                  : true
                : false,
              pageLink: data.data.pageLink ? data.data.pageLink : '',
              pagePassCode: data.data.pagePassCode
                ? data.data.pagePassCode
                : '',
              pagePassCodePeriod: data.data.pagePassCodePeriod
                ? moment(data.data.pagePassCodePeriod, 'YYYY-MM-DD')
                : moment().add(3, 'months'),
              isRealTime: data.data.isRealTime
                ? data.data.isRealTime.toString()
                : '1'
            },
            disabled: false,
            inputVal: data.data.pagePassCode ? data.data.pagePassCode : ''
            // buttonDisable:false
          },
          () => {
            this.handlePublishModalVisible(true);
            console.log(this.state.defaultData);
          }
        );
      });
    } else {
      console.log(this.state.defaultData);
      this.handlePublishModalVisible(true);
    }
    this.setState({
      selectScreenId: item.id
    });
  };

  /**
   * 是否发布
   * @param checked
   */
  switchPublish = checked => {
    if (checked) {
      const dataList = this.state.defaultData;
      this.setState({
        disabled: false,
        buttonDisable: false,
        defaultData: {
          ...dataList,
          pagePassCodePeriod: moment().add(3, 'months')
        }
      });
      const {
        queryParams: { userId }
      } = this.state;
      getScreenLink({ userId }).then(data => {
        if (data && data.data) {
          console.log(data);
          const dataList = this.state.defaultData;
          this.setState({
            defaultData: {
              ...dataList,
              pageLink: data.data
            }
          });
        }
      });
    } else {
      this.setState({
        buttonDisable: true
      });
      postClose({ id: this.state.selectScreenId }).then(data => {});
      const dataList = this.state.defaultData;
      this.setState({
        disabled: true,
        defaultData: {
          ...dataList,
          pageLink: '',
          pagePassCode: '',
          pagePassCodePeriod: ''
        },
        inputVal: ''
      });
    }
  };
  inputChange = e => {
    e.persist();
    this.setState({
      inputVal: e.target.value
    });
  };
  //密码可见改变
  // typeInput:'password',
  //eyeType:"eye-invisible"不可见
  changeEye = () => {
    if (this.state.eyeType == 'eye-invisible') {
      this.setState({
        typeInput: 'text',
        eyeType: 'eye'
      });
    } else {
      this.setState({
        typeInput: 'password',
        eyeType: 'eye-invisible'
      });
    }
  };

  render() {
    const {
      data,
      total,
      currentPage,
      pageSize,
      addModalVisible,
      transferModalVisible,
      publishModalVisible,
      used,
      unused,
      queryParams
    } = this.state;

    const paginationProps = {
      showQuickJumper: true,
      showTotal: total => `总共 ${total} 条`,
      pageSize: pageSize,
      total,
      current: currentPage,
      onChange: this.onPaginationChange
    };

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible
    };
    const Methods = {
      handlePublish: this.handlePublish
    };

    const content = [
      {
        type: 'Search',
        key: 'pageTitle',
        placeholder: '界面名称'
      },
      {
        type: 'Select',
        key: 'isPublish',
        // label: '发布状态',
        placeholder: '发布状态',
        data: [
          { value: '0', name: '全部' },
          { value: '1', name: '未发布' },
          { value: '2', name: '发布' }
        ],
        style: {
          width: '240px'
        }
      }
    ];

    return (
      <div className="dashBoard">
        <div className="my-project">
          <div className="project-header">
            <div className="project-title">
              <h2>可视化界面</h2>
              <span className="project-title-info">
                <em>{used}</em>
                个/还可创建
                <em>{unused}</em>个
              </span>
            </div>
            <Filters
              onFilter={this.onFilter}
              content={content}
              query={queryParams}
              className="dark-filter"
            />
          </div>
          <div className="main-screen">
            <Border className="my-screen">
              <div className="new-project" onClick={this.add}>
                <div className="add-new-screen">
                  <i className="icon-add" />
                  <span className="span-spacing">新增可视化界面</span>
                </div>
              </div>
            </Border>
            {data.map(item => (
              <Item
                {...this.props}
                key={item.id}
                className="my-screen"
                item={item}
                reload={this.reload}
                transfer={this.transfer}
                publish={this.publish}
              />
            ))}
          </div>
          {total > 0 && (
            <div className="pagination-bar">
              <Pagination {...paginationProps} />
            </div>
          )}
        </div>
        <CreateForm {...parentMethods} modalVisible={addModalVisible} />
        <TransferForm
          handleTransfer={this.handleTransfer}
          handleModalVisible={this.handleTransferModalVisible}
          modalVisible={transferModalVisible}
        />
        <PublishForm
          {...Methods}
          handleTransfer={this.handleTransfer}
          handleModalVisible={this.handleTransferModalVisible}
          publishModalVisible={publishModalVisible}
          switchPublish={this.switchPublish}
          disabled={this.state.disabled}
          pageUrl={this.state.pageUrl}
          defaultData={this.state.defaultData}
          buttonDisable={this.state.buttonDisable}
          eyeType={this.state.eyeType}
          typeInput={this.state.typeInput}
          changeEye={this.changeEye}
          onChange={this.inputChange}
          inputVal={this.state.inputVal}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setAccessAccountInfo: company => dispatch(setAccessAccountInfo(company))
  };
};

export default connect(
  () => ({}),
  mapDispatchToProps
)(DashBoard);
