import React from 'react';
import { browserHistory } from 'react-router';
import { Button, Form, Input, Modal } from 'antd';
import md5 from 'md5';
import { postResetPwd } from '../../services/forget';
import './forget.less';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 20 },
    sm: { span: 8 }
  }
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 8
    },
    sm: {
      span: 8,
      offset: 8
    }
  }
};

class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      username: '',
      submitDisabled: true,
      preventInputConfirm: true
    };
  }

  componentDidMount() {
    const initCallback = ({ username }) => this.setState({ username });
    this.props.onInit(initCallback);
  }

  //处理表单重置
  componentWillReceiveProps(nextProps) {
    const inputPassword = nextProps.form.getFieldValue('password');
    const confirmPassword = nextProps.form.getFieldValue('confirmPassword');
    if (inputPassword && inputPassword.length !== 0) {
      this.setState({ preventInputConfirm: false });
    } else {
      this.setState({ preventInputConfirm: true });
    }
  }

  resetPwd = data => {
    postResetPwd(data).then(res => {
      this.setState({ loading: false });
      this.props.onSubmit();
    });
  };

  checkPassword = (rule, value, callback) => {

    if (value !== '') {
          this.setState({preventInputConfirm: false});
          callback();
        }
      };

   checkConfirmPassword = (rule, value, callback) => {
    if (value !== '') {
      if (value && value !== this.props.form.getFieldValue('password') ) {
        callback('两次输入不一致，请重新输入');

        this.setState({ submitDisabled: true });

      } else {
        this.props.form.setFieldsValue({ password: value });
        this.setState({ submitDisabled: false });

        callback();
      }
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFieldsAndScroll(err => {
      if (!err) {
        this.setState({ loading: true });
        const params={
          username:this.state.username.trim(),
          password:  md5(this.props.form.getFieldValue('password'))
        };
        this.resetPwd(params);
      } else {
        Modal.error({ content: '输入的信息有误，请核对' });
      }
    });
  };
  handleEnterKey = e => {
    if (e.charCode === 13) {
      this.input.blur();
    }
  };
  render() {
    const {submitDisabled, preventInputConfirm } = this.state;
    const {
      getFieldDecorator,
    } = this.props.form;



    return (
      <div>
        <Form hideRequiredMark={true}>
          <FormItem {...formItemLayout} label="密码" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: '密码必填' },
                { max: 30, message: '不能大于30位' },
                {
                  pattern: /^[0-9A-Za-z\w-]{1,30}$/,
                  message:
                    '长度不能大于30字符，含数字、英文、短横线、下划线。'
                },{
                  validator: this.checkPassword,
                },
              ]
            })(<Input type="password"  />)}
          </FormItem>
          <FormItem {...formItemLayout} label="确认密码" hasFeedback>
            {getFieldDecorator('confirmPassword', {
              rules: [
                {
                  required: true,
                  message: '请输入密码'
                },
                {
                  validator: this.checkConfirmPassword
                },
                { max: 30, message: '不能大于30位' },
                {
                  pattern: /^[0-9A-Za-z\w-]{1,30}$/,
                  message: '长度不能大于30字符，含数字、英文、短横线、下划线。'
                }
              ],

            })(
              <Input
                type="password"
                disabled={ preventInputConfirm}
                ref={input => {
                  this.input = input;
                }}
                onKeyPress={this.handleEnterKey}
              />
            )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              size="large"
              onClick={this.handleSubmit}
              disabled={submitDisabled}
              className="Btn"
              loading={this.state.loading}
            >
              <span>确认</span>
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default Form.create()(Reset);
