import React from 'react';
import { browserHistory } from 'react-router';
import { Button, Form, Input } from 'antd';
import './forget.less';
import {
  postCaptchaOnMobile,
  getCaptchaOnMobile
} from '../../services/forget';
import md5 from "md5";

const FormItem = Form.Item;

const SMS_CODE_LENGTH = 6;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  }
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 8
    },
    sm: {
      span: 8,
      offset: 8
    }
  }
};

class PhoneForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      username:'',
      phone:'',
      disabled: true,
      blockingCount: 0,
      intervalSend: null,
      blocking:0,
      intervalValid: null,
    };
  }

  componentDidMount() {
    this.props.onInit(this.fetchPhone);
  }



  componentWillUnmount() {
    if (this.state.intervalSend) {
      clearInterval(this.state.intervalSend);
    }
    if (this.state.intervalValid) {
      clearInterval(this.state.intervalValid);
    }
  }

  //获取验证码,有效期60s
  onSendClick = () => {

    getCaptchaOnMobile({username:this.state.username}).then(data => {
      this.setState({
        blockingCount: 60,
        blocking:300
      });
      const intervalValid = setInterval(() => {
        const count = this.state.blocking;
        this.setState({
          blocking: count > 0 ? count - 1 : 0
        });
        if (count === 0) {
          clearInterval(intervalValid);
          this.props.form.setFields({
            phoneCode: { value:'', errors: [new Error('验证超时，请重新发送短信验证码')] }
          });
        }
      }, 1000);

      const intervalSend = setInterval(() => {
        const count = this.state.blockingCount;
        this.setState({
          blockingCount: count > 0 ? count - 1 : 0
        });
        if (count === 0) {
          clearInterval(intervalSend);
        }
      }, 1000);

      this.setState({
        intervalSend,
      });
    });
  };

  //传递属性得到姓名对象,电话类型为字符型,中间4位为*号


  fetchPhone = (params) =>{
    const phone = params.phone;
    //中间6位*代替
    const showPhone = phone.substr(0, 3) + '****' + phone.substr(7);
    this.setState({
      ...params,
      phone: showPhone
    });


  };



  validphoneCode = (rule , value, callback) => {

    const { getFieldValue, setFieldsValue } = this.props.form;
    let code = getFieldValue('phoneCode') ? getFieldValue('phoneCode').toString() : '';

    if (code.length > SMS_CODE_LENGTH) {
      code = code.slice(0, 6);
      setFieldsValue({ phoneCode: code });
    }
    if (code.length === SMS_CODE_LENGTH) {
      this.setState({ disabled: false });
    }
    callback();
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields( (err,values) => {
      if (!err) {
        this.setState({ loading: true });
        let code =values.phoneCode.toString();
        postCaptchaOnMobile({ username: this.state.username, code: code })
          .then(data => {
            this.setState({ loading: false });
            if(data.success){
              this.props.onSubmit();
            }else{
              this.props.form.setFields({
                phoneCode: { value: code, errors: [new Error('验证码错误')] }
              });
            }


          });
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} >
          <FormItem {...formItemLayout} label="手机号">
            <span className="phone"> {this.state.phone} </span>
            <a
              onClick={this.onSendClick}
              disabled={this.state.blockingCount !== 0}
              className="sendcode-link"
            >
              {this.state.blockingCount
                ? `${this.state.blockingCount} s`
                : '发送短信验证码'}
            </a>
          </FormItem>
          <FormItem {...formItemLayout} label="短信验证码" hasFeedback>
            {getFieldDecorator('phoneCode', {
              rules: [
                {
                  required: true,
                  message: '请输入短信验证码!'
                },
                {
                  validator: this.validphoneCode
                }
              ]
            })(<Input   />)}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              size="large"
              htmlType="submit"
              disabled={this.state.disabled}
              className="Btn"
              loading={this.state.loading}
            >
              <span>下一步</span>
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default Form.create()(PhoneForm);
