import React from 'react';
import { Button, Form, Input } from 'antd';

import {
  fetchUsername,
  postNameAndCaptchaOnWeb
} from '../../services/forget';
import './forget.less';

const FormItem = Form.Item;

const CAPTCHA_MAX_LEN = 4;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  }
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 8
    },
    sm: {
      span: 8,
      offset: 8
    }
  }
};

class ForgetForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      captchaStatus: null,
      captchaHelp: null,
      blockingCount: 0,
      interval: null,
      captchaBlob: null,
      disabled: true
    };
  }

  componentDidMount() {
    this.onRefreshClick();
    this.input.focus();
  }

  onRefreshClick = () => {
    //图片验证码有效期五分钟

      this.setState({
        //captchaBlob: `http://101.132.64.184:9090/api/v0.1/validateImages/code?t=${new Date().toString()}`,
        captchaBlob: `/api/v0.1/validateImages/code?t=${new Date().toString()}`,
        blockingCount: 300
      });

        const interval = setInterval(() => {
        const count = this.state.blockingCount;
        this.setState({
          blockingCount: count > 0 ? count - 1 : 0
        });
        if (count === 0) {
          //验证码失效
          const code = this.props.form.getFieldValue('code');
          clearInterval(interval);
          this.props.form.setFields({
            code: { value:code, errors: [new Error('验证超时，请刷新验证码后重新输入')] }

          });
        }
      }, 1000);

      this.setState({
        interval
      });

  };

  //有效账号则输入框失焦
  checkUsername = (rule, value, callback) => {
    const { getFieldValue } = this.props.form;
    let username = getFieldValue('username').trim();
    const data = { username: username };
    if( value !== '')
    {
       fetchUsername(data)
      .then((res) => {
        if( !res.success){
          this.props.form.setFields({
            username: { value:username, errors: [new Error('请输入有效账户')] }
          });
        }
      });

    }
    callback();
  };

  //按下enter键或者输入框失焦触发账号后端认证
  handleEnterKey = e => {
    if (e.charCode === 13) {
      this.input.blur();
    }
  };

  onCaptchaChange = (rule, value, callback) => {
    this.setState({ verified: false });
    const { getFieldValue, setFieldsValue } = this.props.form;
    let captcha = getFieldValue('code') ? getFieldValue('code').toString() : '';
    if (captcha.length > CAPTCHA_MAX_LEN) {
      captcha = captcha.slice(0, 4);
      setFieldsValue({ code:captcha });
    }
    if (captcha.length < CAPTCHA_MAX_LEN) {
      this.setState({ disabled: true });
    }
    if (captcha.length === CAPTCHA_MAX_LEN) {
      this.setState({ disabled: false });
      this.setState({ verified:  true });
    }
    callback();
  };

  onUsernameChange = () => {
    if (this.state.verified) {
      const code = this.props.form.getFieldValue('code');
      this.setState({ verified: false, disabled: true });
      this.props.form.setFields({
        code: { value:'', errors: [new Error('请刷新验证码后重新输入')] }
      });
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields( (err, values) => {
      if (!err) {
        this.setState({ loading: true });
        const params = {
          username: values.username.trim(),
          code: values.code
        };
        postNameAndCaptchaOnWeb(params)
          .then(data => {
            this.setState({ loading: false });
            if(data.success){
              this.setState({ verified: true });
              this.setState({ captchaStatus: null, captchaHelp: null });
              this.props.onSubmit({
                username: data.data.username,
                phone: data.data.phone
              });

            }else {
              this.props.form.setFields({
                code: { value:'', errors: [new Error(data.message)] }
              });
            }
          });

      }
    });
  };

  componentWillUnmount() {
    if (this.state.interval) {
      clearInterval(this.state.interval);
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
          <FormItem {...formItemLayout} label="用户名" hasFeedback>
            {getFieldDecorator('username', {
              initialValue: '',
              validateTrigger: 'onBlur',
              rules: [
                { required: true, message: '请输入用户名!' },
                { validator: this.checkUsername }
              ]
            })(
              <Input
                ref={input => {
                  this.input = input;
                }}
                placeholder="请输入账户名"
                onChange={this.onUsernameChange}
                onKeyPress={this.handleEnterKey}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="安全验证"
            hasFeedback
            validateStatus={this.captchaStatus}
          >
            {getFieldDecorator('code', {
              rules: [
                { required: true, message: '请输入四位图片验证码!' },
                { validator: this.onCaptchaChange }
              ]
            })(<Input  />)}
          </FormItem>
          <FormItem {...tailFormItemLayout} className="captcha">
            <img src = {this.state.captchaBlob} alt="图片验证码" />
            <a onClick={this.onRefreshClick} className="reload-link">
              刷新
            </a>
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              disabled={this.state.disabled}
              className="Btn"
              loading={this.state.loading}
            >
              <span>安全验证</span>
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default Form.create()(ForgetForm);
