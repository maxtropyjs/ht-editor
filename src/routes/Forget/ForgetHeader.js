import React from 'react';
import { Link } from 'react-router-dom';
import './forget.less';
class ForgetHeader extends React.Component {
  render() {
    return (
      <div className="forgetHeader">
        <span className="line">|</span>
           找回密码
      </div>
    );
  }
}

export default ForgetHeader;
