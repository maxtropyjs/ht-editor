import React from 'react';
import { Steps } from 'antd';
import ForgetForm from './ForgetForm';
import PhoneForm from './PhoneForm';
import ResetForm from './ResetForm';
import ForgetHeader from './ForgetHeader';
import Header from '../../components/loginHeader/Header';
import './forget.less';
const { Step } = Steps;

class Forget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0
    };
  }

  onPhoneInit = callback => {
    callback({ username: this.state.username, phone: this.state.phone });
  };

  onResetInit = callback => {
    callback({ username: this.state.username });
  };

  next = params => {
    const current = this.state.current + 1;
    if (current >= 3) {
      this.props.history.push('/login');
    }
    this.setState({ ...params, current });
  };

  render() {
    const steps = [
      {
        title: '图片验证码',
        content: <ForgetForm onSubmit={this.next} />
      },
      {
        title: '手机验证码',
        content: <PhoneForm onInit={this.onPhoneInit} onSubmit={this.next} />
      },
      {
        title: '重置密码',
        content: <ResetForm onInit={this.onResetInit} onSubmit={this.next} />
      }
    ];

    return (
      <div className="page-forget-all">
        <Header />
        <div className="main">
          <div className="main-header">
            <span className="line">|</span>
            首页/找回密码
          </div>
          <div className="content">
            <ForgetHeader />
            <Steps
              current={this.state.current}
              labelPlacement="vertical"
              className="Steps"
            >
              <Step
                title="验证身份"
                icon={<span class="ant-steps-icon"> 1</span>}
              />
              <Step
                title="短信验证"
                icon={<span class="ant-steps-icon"> 2</span>}
              />
              <Step
                title="确认修改"
                icon={<span class="ant-steps-icon"> 3</span>}
              />
            </Steps>
            <div className="forgetform">
              {steps[this.state.current].content}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Forget;
