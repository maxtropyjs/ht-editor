import { stringify } from 'qs';
import request from '@/utils/request';
import { select } from '@/utils/stateStore';

export async function getScreenNumber(params) {
  return request(`/api/v0.1/index/getUserPageNum?${stringify(params)}`);
}

export async function getScreen(params) {
  return request(`/api/v0.1/index/list?${stringify(params)}`);
}

export async function postScreen(params) {
  return request('/api/v0.1/pages/save', {
    method: 'POST',
    body: params
  });
}

export async function putUsers(params) {
  return request('/api/v0.1/htUsers/update', {
    method: 'PUT',
    body: params
  });
}

export async function deleteScreen(params) {
  return request('/api/v0.1/pages/delete', {
    method: 'DELETE',
    body: {
      ...params,
    },
  });
}

export async function copyScreen(params) {
  return request('/api/v0.1/pages/copy', {
    method: 'POST',
    body: {
      ...params
    }
  });
}

export async function transferScreen(params) {
  return request('/api/v0.1/pages/copyPageForOther', {
    method: 'POST',
    body: {
      ...params
    }
  });
}

export async function renameScreen(params) {
  return request(`/api/rename?${stringify(params)}`, {
    method: 'POST',
    body: {
    }
  });
}

export async function getScreenLink(params) {
  return request(`/api/v0.1/pages/getLink?${stringify(params)}`);
}

export async function postPublish(params) {
  return request('/api/v0.1/pages/publish', {
    method: 'POST',
    body: params
  });
}

export async function getPagePublishInfo(id) {
  return request(`/api/v0.1/pages/getPagePublishInfo?id=${id}`);
}

export async function postClose(params) {
  return request('/api/v0.1/pages/close', {
    method: 'POST',
    body: params
  });
}
