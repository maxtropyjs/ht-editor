import request from '@/utils/request';

export async function postLogin(params) {
  return request('/api/login', {
    method: 'POST',
    body: params
  },false);
}

export async function getCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}

export async function getCode() {
  return request('/api/v0.1/htUsers/getCode');
}

export async function getUsers(params) {
  return request('/api/v0.1/htUsers/list');
}

export async function postUsers(params) {
  return request('/api/v0.1/htUsers/save', {
    method: 'POST',
    body: params
  });
}

export async function putUsers(params) {
  return request('/api/v0.1/htUsers/update', {
    method: 'PUT',
    body: params
  });
}
