import request from '../utils/request';

export async function login(data) {
  return request('/api/login', {
    method: 'POST',
    body: data
  });
}
export async function fetchUsername(data) {
  return request('/api/v0.1/Users/validateName', {
    method: 'POST',
    body: data
  });
}
// export async function getCaptchaOnWeb(date) {
//   return request(`/api/v0.1/validateImages/code?t=${date}`);
// }
export async function getCaptchaOnWeb() {
  return request('/api/v0.1/validateImages/code');
}
export async function postNameAndCaptchaOnWeb(data) {
  return request('/api/v0.1/Users/validateNameAndCode', {
    method: 'POST',
    body: data
  });
}
export async function getCaptchaOnMobile(params) {
  return request('/api/v0.1/Users/sendSmsCode', {
    method: 'POST',
    body:params
  });
}

export async function postCaptchaOnMobile(data) {
  return request('/api/v0.1/Users/validateSmsCode', {
    method: 'POST',
    body: data
  });
}

export async function postResetPwd(data) {
  return request('/api/v0.1/Users/setNewPassword', {
    method: 'POST',
    body: data
  });
}
