import request from '@/utils/request';

//单独查询是否存在对应账号和密码

function getConfig(params) {
  return request('get', '/api/config', { ...params });
}

function createConfig(body) {
  return request('post', '/api/config', body);
}

function getConfigById(id) {
  return request('get', `/api/config/${id}`, {});
}

function updateConfig(id, body) {
  return request('put', `/api/config/${id}`, body);
}

function getSimpleMachine() {
  return request('get', `/dbpe/machine/simple`, { project_id: 3 }, false, true);
}

function getMachineDetail(id) {
  return request(
    'get',
    `/dbpe/point/operate/last/${id}`,
    { project_id: 3, type_id: 3 },
    false,
    true
  );
}

function getMachineStatus(id) {
  return request(
    'get',
    `/dbpe/machine/status/${id}`,
    { project_id: 3 },
    false,
    true
  );
}

function getConfigData(id = 21) {
  return request('get', `/htConfig/ht/gateway/${id}`, null, true, false);
}

export {
  getConfigData,
  getConfig,
  getConfigById,
  createConfig,
  updateConfig,
  getSimpleMachine,
  getMachineDetail,
  getMachineStatus
};
