import { stringify } from 'qs';
import request from '@/utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return request('/api/currentUser');
}

export async function login(params) {
  return request('/api/login', {
    method: 'POST',
    body: params
  });
}

export async function getCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}

export async function getCode() {
  return request('/api/v0.1/htUsers/getCode');
}

export async function getUsers(params) {
  return request(`/api/v0.1/htUsers/list?${stringify(params)}`);
}

export async function postUsers(params) {
  return request('/api/v0.1/htUsers/save', {
    method: 'POST',
    body: params
  });
}

export async function putUsers(params) {
  return request('/api/v0.1/htUsers/update', {
    method: 'POST',
    body: params
  });
}

export async function deleteUsers(params) {
  return request('/api/v0.1/htUsers/delete', {
    method: 'DELETE',
    body: params
  });
}
