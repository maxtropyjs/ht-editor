/* eslint-disable import/no-mutable-exports */
import {setAuthority} from "../../utils/authority";

let CURRENT = 'NULL';
/**
 * use  authority or getAuthority
 * @param {string|()=>String} currentAuthority
 */
const renderAuthorize = Authorized => {
  if (window.location.search) {
    const params = window.location.search.split('&');
    const token = params[0].replace('?token=', '');
    const username = params[1].replace('username=', '');
    const ucode = params[2].replace('ucode=', '');
    const userId = params[3].replace('id=', '');
    localStorage.setItem('Authorization', token);
    localStorage.setItem('userId', userId);
    localStorage.setItem('username', username);
    localStorage.setItem('ucode', ucode);
    localStorage.setItem('sid', userId);
    if(username==='superadmin'){
      setAuthority('admin');
    }else {
      setAuthority('user');
    }
  }
  return currentAuthority => {
    if (currentAuthority) {
      if (currentAuthority.constructor.name === 'Function') {
        CURRENT = currentAuthority();
      }
      if (
        currentAuthority.constructor.name === 'String' ||
        currentAuthority.constructor.name === 'Array'
      ) {
        CURRENT = currentAuthority;
      }
    } else {
      CURRENT = 'NULL';
    }
    return Authorized;
  };
};

export {CURRENT};
export default Authorized => renderAuthorize(Authorized);
