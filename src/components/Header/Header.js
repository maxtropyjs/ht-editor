import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, Dropdown, Icon, message } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import logo from '../../image/logo.svg';
import './header.less';

class Header extends React.Component {
  user = 'superadmin';
  state = {
    visible: false
  };
  onCopy = () => {
    message.info('复制成功！');
    this.setState({ visible: false });
  };
  handleVisibleChange = flag => {
    this.setState({ visible: flag });
  };
  render() {
    let code = '26545sfd';
    const menu = (
      <Menu className="aa">
        <Menu.Item key="1">用户识别码</Menu.Item>
        <Menu.Item key="2">{code}</Menu.Item>
        <CopyToClipboard text={code} onCopy={this.onCopy}>
          <Menu.Item key="3">复制</Menu.Item>
        </CopyToClipboard>
      </Menu>
    );
    return (
      <div className="c-header">
        <div>
          <img src={logo} className="logo" />
          <p className="title">极熵科技Web组态编辑系统</p>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div>
            <Dropdown
              overlay={menu}
              visible={this.state.visible}
              onVisibleChange={this.handleVisibleChange}
            >
              <a className="ant-dropdown-link">
                <Icon type="user" />
                你好，焦提兵
                <Icon type="down" />
              </a>
            </Dropdown>
          </div>
          <Menu mode="horizontal" className="menu">
            <Menu.Item>
              <Link to="/user" title="管理中心">
                <Icon type="appstore-o" />
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/" title="返回首页">
                <Icon type="home" />
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/login" title="退出">
                <Icon type="logout" />
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </div>
    );
  }
}

export default Header;
