import React from 'react';
import { Table } from 'antd';

const { Column } = Table;

class TableComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { titleData, data, action, noActionItem, classname } = this.props;
    const tableItem = titleData.map(item => {
      return <Column {...item} />;
    });
    return (
      <Table
        dataSource={data}
        rowKey={record => record.id}
        className={classname}
        pagination={false}
      >
        {tableItem}
        {noActionItem ? null : action ? (
          <Column
            title="操作"
            key="action"
            render={(text, record) => action(record)}
          />
        ) : null}
      </Table>
    );
  }
}

export default TableComponent;
