import React, {Component} from 'react';
import {Pagination, message} from 'antd';
import PropTypes from 'prop-types';
import classname from 'classnames';
import './style.less';

function noop() {

}

class CustomPagination extends Component {
    static propTypes = {
        style: PropTypes.object,
        className: PropTypes.string,
        pageSize: PropTypes.number,
        changeCallback: PropTypes.func.isRequired,
        fetchApi: PropTypes.func.isRequired
    };
    static defaultProps = {
        pageSize: 10,
        changeCallback: noop,
        fetchApi: noop
    };

    constructor(props) {
        super(props);
        this.state = {
            dataArray: [],
            total: -1
        }
    }

    componentDidMount() {
        this.handleChange(1);
    };

    handleChange = (page) => {
        const {fetchApi} = this.props;
        message.info('数据加载中');
        fetchApi(page - 1).then((data) => {
            console.log(data)
            message.destroy();
            if (this.state.total === -1) {
                this.setState({total: data.total})
            }
            if (this.props.changeCallback) {
                this.props.changeCallback(data)
            }
        }).catch(e => {
            if (e === 'noAuth') {
                window.location.href = '/login';
            }
        })
    }

    render() {
        const {style, className, pageSize} = this.props;
        const {total} = this.state;
        const customClass = classname('customPaginationWrapper', className);
        return (
            <div style={style} className={customClass}>
                {total === -1 ? null : (
                    <Pagination onChange={this.handleChange} total={total} pageSize={pageSize}/>
                )}
            </div>
        );
    }
}

export default CustomPagination;

