import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isEqual } from 'lodash';
import { DatePicker, Input, Select, Button, Form as AntForm } from 'antd';
import { formatValue } from '@/utils/utils';
import CreateButton from '../ui/CreateButton';

import './style/filters.less';

const Search = Input.Search;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const FormItem = AntForm.Item;

export default class Filters extends React.Component {
  constructor(props) {
    super(props);
    const { query } = props;
    this.state = {
      query,
    };
  }

  componentWillReceiveProps(nextProps) {
    if ('query' in nextProps && !isEqual(nextProps.query, this.props.query)) {
      const { query } = nextProps;
      this.setState({ query });
    }
  }

  render() {
    const { style, className, content, onFilter, createUrl, createLabel } = this.props;
    const { query } = this.state;
    const filter = content.map((item, index) => {
      const isShowTime = !!this.props.type;
      let component = null;
      switch (item.type) {
        case 'Select': {
          const optionItem = item.render
            ? item.render(item.data || [])
            : (item.data || []).map(opt => {
              return (
                <Option value={opt.value} key={opt.value}>
                  <span>{opt.name}</span>
                </Option>
              );
            });
          component = (
            <Select
              placeholder={item.placeholder}
              // defaultValue={item.defaultValue || '全部'}
              showSearch
              optionFilterProp="children"
              onChange={value => onFilter(item.key, formatValue(value, item.valueType))}
              dropdownMatchSelectWidth={false}
              style={item.style}
            >
              {optionItem}
            </Select>
          );
          break;
        }
        case 'Search':
          component = (
            <Search
              value={query[item.key]}
              // placeholder={`输入关键字搜索${this.props.label}`}
              placeholder={item.placeholder}
              onSearch={value => onFilter(item.key, formatValue(value, item.valueType))}
              onBlur={e => onFilter(item.key, e.target.value, false)}
              onChange={e => {
                if (e.target.value.length === 0) {
                  onFilter(item.key, null);
                }
                this.setState({ query: { ...query, [item.key]: e.target.value } });
              }}
            />
          );
          break;
        case 'DatePicker':
          component = (
            <DatePicker
              showTime
              format={item.format || 'YYYY-MM-DD HH:mm:ss'}
              defaultValue={query[item.key]}
              showToday={false}
              onChange={data => onFilter(item.key, formatValue(data, item.valueType))}
            />
          );
          break;
        case 'DateRange':
          component = (
            <RangePicker
              style={{ minWidth: '300px', width: '100%' }}
              showTime={!isShowTime}
              showToday
              format={item.format || isShowTime ? 'YYYY-MM-DD' : 'YYYY-MM-DD HH:mm:ss'}
              defaultValue={query.dateRange}
              data={item.data}
              onChange={([start, end]) => {
                onFilter({
                  [item.start]: formatValue(start, item.valueType),
                  [item.end]: formatValue(end, item.valueType),
                });
              }}
              key={item.key}
            />
          );
          break;
        case 'Button':
          component = (
            <Button size="large" onClick={() => onFilter(item.params)}>
              {item.key}
            </Button>
          );
          break;
        default:
          break;
      }
      return (
        <FormItem key={index} label={item.label}>
          {component}
        </FormItem>
      );
    });
    const clsString = classNames('filter', className);
    return (
      <div className={clsString} style={{ ...style }}>
        <AntForm layout="inline">
          {filter.filter(v => v)}
          {createUrl ?
            <CreateButton
              label={createLabel}
              to={createUrl}
            /> : null
          }
        </AntForm>
      </div>
    );
  }
}

Filters.proptypes = {
  onFilter: PropTypes.func.isRequired,
  content: PropTypes.array,
  label: PropTypes.string,
  query: PropTypes.object,
  createUrl: PropTypes.string,
  createLabel: PropTypes.string,
  style: PropTypes.object,
  clickEventQuery: PropTypes.object,
  customButton: PropTypes.node,
};

Filters.defaultProps = {
  content: [],
  label: '',
  query: {},
  createLabel: '新增',
  style: {},
  clickEventQuery: {},
};
