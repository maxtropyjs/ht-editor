import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../image/logo.svg';
import './Header.less';

class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="loginHeader">
        <img src={Logo} className="logo" />

        <div className="title">极熵科技Web组态编辑系统</div>
        <Link to="/login" className="login-link">
          <span className="atext">返回登录</span>
        </Link>
      </div>
    );
  }
}
export default Header;
