import React from 'react';
import { Button, Icon } from 'antd';

const CreateButton = ({ to, label = '新增' }) => {
  return (
    <Link to={to}>
      <Button type="primary">
        <Icon type="plus" />
        {label}
      </Button>
    </Link>
  );
};

export default CreateButton;
