import React from 'react';
import classNames from 'classnames';
import './style/Border.less';

const Border = ({ children, className, ...rest }) => {
    const clsString = classNames('border', className);
    return (
        <div className={clsString} {...rest}>
            <i className="dash dash-tl" />
            <i className="dash dash-tr" />
            <i className="dash dash-bl" />
            <i className="dash dash-br" />
            {children}
        </div>
    );
};

export default Border;
