import React from 'react';
import classNames from 'classnames';
import styles from './index.module.less';

const GlobalFooter = ({ className }) => {
  const clsString = classNames(styles.globalFooter, className);
  return <footer className={clsString}></footer>;
};

export default GlobalFooter;
