import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import {
  Spin, Tag, Menu, Icon, Dropdown, Tooltip, message,
} from 'antd';
import logo from '@/assets/logo.svg';
import {logOut} from 'actions/user';
import history from "../../history";
import styles from './index.module.less';
import {CopyToClipboard} from "react-copy-to-clipboard";

class GlobalHeader extends PureComponent {
  state = {
    visible: false,
    current: 'home',
  };
  onCopy = () => {
    message.info('复制成功！');
    this.setState({visible: false});
  };
  handleVisibleChange = flag => {
    this.setState({visible: flag});
  };
  logOut = async () => {
    history.push('/login');
    this.props.logOut();
  };

  render() {
    const {username, ucode, company} = this.props;
    const isAdmin = username === 'superadmin';
    console.log(history.location.pathname)
    if(history.location.pathname=='/user'){
      this.setState({
        current: 'center',
      })
    }else{
      this.setState({
        current: 'home',
      })
    }
    const menu = (
      <Menu className={styles.menu} selectedKeys={[]}>
        <Menu.Item key="1">用户识别码</Menu.Item>
        <Menu.Item key="2">{ucode}</Menu.Item>
        <CopyToClipboard text={ucode} onCopy={this.onCopy}>
          <Menu.Item key="3">复制</Menu.Item>
        </CopyToClipboard>
      </Menu>
    );
    return (
      <div className={styles.header}>
        <Link to="/" className={styles.logo}>
          <img src={logo} alt="logo" width="48"/>
          极熵科技Web组态编辑管理系统
        </Link>
        <div className={styles.right}>
          <Tooltip placement="bottom" title={company}>
            <div className={styles.company}>
              <Icon type="global" /> { company }
            </div>
          </Tooltip>
          {username ? (
            <Dropdown overlay={menu} overlayClassName={styles.headerDropdown}>
              <span className={`${styles.action} ${styles.account}`}>
                <Icon type="user"/>
                <span className={styles.name}>您好,{username}</span>
                <Icon type="down"/>
              </span>
            </Dropdown>
          ) : (
            <Spin size="small" style={{marginLeft: 8}}/>
          )}
          <Menu mode="horizontal" className={styles.menu} selectedKeys={[this.state.current]}>
            {
              isAdmin && (
                <Menu.Item key="center">
                  <Tooltip placement="bottom" title="管理中心">
                    <Link to="/user" className={styles.action}>
                      <Icon type="appstore-o"/>
                    </Link>
                  </Tooltip>
                </Menu.Item>
              )
            }
            {
              isAdmin && (
                <Menu.Item key="home">
                  <Tooltip placement="bottom" title="返回首页">
                    <Link to="/" id="home" className={styles.action}>
                      <Icon type="home"/>
                    </Link>
                  </Tooltip></Menu.Item>

              )
            }
            <Menu.Item key="logout">
              <Tooltip placement="bottom" title="退出登录">
              <span className={`${styles.action} ${styles.logout}`} onClick={this.logOut}>
              <Icon type="logout"/>
              </span>
              </Tooltip>
            </Menu.Item>
          </Menu>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({user}) => ({
  username: user.username,
  ucode: user.ucode,
  company: user.company,
});

const mapDispatchToProps = dispatch => {
  return {
    logOut: () => dispatch(logOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalHeader);
