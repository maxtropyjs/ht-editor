import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop';
import { getRouterData } from './common/router';
import Authorized from './utils/Authorized';
import Login from './routes/User/Login';
import Forget from './routes/Forget';
import Editor from './routes/editor/Editor';

const { AuthorizedRoute } = Authorized;

export default function RouterConfig({ history }) {
  const routerData = getRouterData();
  const BasicLayout = routerData['/'].component;
  return (
    <Router history={history}>
      <ScrollToTop>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/forget" component={Forget} />
          <Route exact path="/editor/:id?" component={Editor} />
          <AuthorizedRoute
            path="/"
            render={props => <BasicLayout {...props} />}
            authority={['admin', 'user']}
            redirectPath="/login"
          />
        </Switch>
      </ScrollToTop>
    </Router>
  );
}
