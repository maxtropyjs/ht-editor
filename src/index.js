import React from 'react';
import ReactDOM from 'react-dom';
import bowser from 'bowser';
import { LocaleProvider, message } from 'antd';
import { Provider } from 'react-redux';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';
import 'moment/locale/zh-cn';

import RouterConfig from './router';
import history from './history';
import store from './store';

import './style/index.less';

moment.locale('zh-cn');

const supportEngine = ['webkit', 'gecko', 'blink', 'msedge'];
const checkBrowser = () => {
  if (!supportEngine.some(p => bowser[p])) {
    let elm = document.createElement('div');
    elm.innerHTML = '浏览器升级';
    document.body.appendChild(elm);
  }
};

checkBrowser();
message.config({
  top: 64,
  maxCount: 1
});

ReactDOM.render(
  <Provider store={store}>
    <LocaleProvider locale={zh_CN}>
      <RouterConfig store={store} history={history} />
    </LocaleProvider>
  </Provider>,
  document.getElementById('root')
);
