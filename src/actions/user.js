import { LOGIN, LOGOUT, ACCESSACCOUNT } from './ActionTypes';

export const setUserInfo = (user) => {
  return {
    type: LOGIN,
    data: user
  };
};

export const setAccessAccountInfo = (company) => {
  console.log('sssssssssssss')
  console.log(company)
  return {
    type: ACCESSACCOUNT,
    company,
  };
};

export const logOut = () => dispatch => {
  dispatch({
    type: LOGOUT,
    data: null
  });
};

export const initApp = userInfo => dispatch => {
  dispatch({
    type: LOGIN,
    data: userInfo
  });
};
