// 储存与用户有关的信息,包括是否登录,权限
import {
  LOGIN,
  LOGOUT,
  ACCESSACCOUNT,
} from '@/actions/ActionTypes';
import { LocalCache } from '@/utils/cache';
import { setAuthority } from '@/utils/authority';
import { reloadAuthorized } from '@/utils/Authorized';

let isRemember = LocalCache.get('remember') === 'true';
const initialState = {
  isLogin: false,
  username: LocalCache.get('username'),
  userId: LocalCache.get('userId'),
  ucode: LocalCache.get('ucode'),
  remember: isRemember,
  rememberName: LocalCache.get('rememberName'),
  company: '',
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      setAuthority('admin');
      reloadAuthorized();
      LocalCache.set('remember', action.data.remember);
      LocalCache.set('rememberName', action.data.rememberName);
      LocalCache.set('ucode', action.data.ucode);
      LocalCache.set('userId', action.data.id);
      LocalCache.set('username', action.data.username);
      return {
        ...state,
        isLogin: true,
        rememberName: action.data.rememberName,
        remember: action.data.remember,
        username: action.data.username,
        ucode: action.data.ucode,
        userId: action.data.id,
      };
    case ACCESSACCOUNT:
      return {
        ...state,
        company: action.company,
      };
    case LOGOUT:
      reloadAuthorized();
      setAuthority('guest');
      isRemember = LocalCache.get('remember') === 'true';
      !isRemember && LocalCache.set('rememberName', '');
      return {
        ...state,
        isLogin: false,
        username: '',
        userId: null,
        ucode: null,
        remember: isRemember,
        rememberName: LocalCache.get('rememberName'),
        company: '',
      };
    default:
      return state;
  }
};
