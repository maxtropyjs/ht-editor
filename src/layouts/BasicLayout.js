import React, { Fragment } from 'react';

import { Route, Redirect, Switch, routerRedux } from 'react-router-dom';
import GlobalFooter from '@/components/GlobalFooter';
import GlobalHeader from '@/components/GlobalHeader';
import Exception403 from '@/routes/Exception/403';
import { getRoutes } from '../utils/utils';
import Authorized from '../utils/Authorized';
import { getMenuData } from '../common/menu';
import logo from '../assets/logo.svg';
import './BasicLayout.less';


const { AuthorizedRoute, check } = Authorized;


/**
 * 根据菜单取得重定向地址.
 */
const redirectData = [];
const getRedirect = item => {
  if (item && item.children) {
    if (item.children[0] && item.children[0].path) {
      redirectData.push({
        from: `${item.path}`,
        to: `${item.children[0].path}`,
      });
      item.children.forEach(children => {
        getRedirect(children);
      });
    }
  }
};
getMenuData().forEach(getRedirect);

class BasicLayout extends React.PureComponent {

  getBaseRedirect = () => {
    // According to the url parameter to redirect
    // 这里是重定向的,重定向到 url 的 redirect 参数所示地址
    const urlParams = new URL(window.location.href);

    const redirect = urlParams.searchParams.get('redirect');

    console.log(redirect)
    // Remove the parameters in the url
    if (redirect) {
      urlParams.searchParams.delete('redirect');
      window.history.replaceState(null, 'redirect', urlParams.href);
    } else {
      const userId = localStorage.getItem('userId');
      const authorizedPath = `/${userId}/dashboard`;
      return authorizedPath;
    }
    return redirect;
  };

  render() {
    const {
      routerData,
      match,
    } = this.props;

    const baseRedirect = this.getBaseRedirect();
    console.log(baseRedirect)

    return (
      <div className="p-container">
        <GlobalHeader
          logo={logo}
          {...this.props}
        />
        <Switch>
          {redirectData.map(item => (
            <Redirect key={item.from} exact from={item.from} to={item.to} />
          ))}
          {
            getRoutes(match.path, routerData).map(item =>
              (
                <AuthorizedRoute
                  key={item.key}
                  path={item.path}
                  component={item.component}
                  exact={item.exact}
                  authority={item.authority}
                  redirectPath="/exception/403"
                />
              )
            )
          }
          <Redirect exact from="/" to={baseRedirect}/>
        </Switch>
        <GlobalFooter />
      </div>
    );
  }
}

export default BasicLayout;
