const path = require('path');
const paths = require('./paths');

// noinspection WebpackConfigHighlighting
module.exports = {
  '@': path.resolve(paths.appSrc),
  components: path.resolve(paths.appSrc, "components"),
  lib: path.resolve(paths.appSrc, "lib"),
  constants: path.resolve(paths.appSrc, "constants"),
  media: path.resolve(paths.appSrc, "media"),
  rest: path.resolve(paths.appSrc, "rest"),
  actions: path.resolve(paths.appSrc, "actions"),
  store: path.resolve(paths.appSrc, "store"),
  MathLib$: path.resolve(paths.appSrc, "lib/MathLib/index"),
  CustomForm$: path.resolve(paths.appSrc, "components/CustomForm/index"),
  Chart$: path.resolve(paths.appSrc, "components/Chart/index"),
  moment: path.resolve(paths.appNodeModules, "moment")
};
