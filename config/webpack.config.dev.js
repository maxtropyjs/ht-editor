const autoprefixer = require('autoprefixer')
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin')
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin')
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin')
const getClientEnvironment = require('./env')
const paths = require('./paths')
const ManifestPlugin = require('webpack-manifest-plugin');
const webpackResolveAlias = require('./webpack-resolve-alias');

const publicPath = '/'
const publicUrl = ''
const env = getClientEnvironment(publicUrl)


// const backendTarget = "http://101.132.64.184:9090";
const backendTarget = "https://ht.dbpe-cps.com";
// const backendTarget = "http://192.168.3.14:9091";
//const backendTarget = "http://192.168.3.88:9090";
const backendTarget2 = "http://10.9.108.189:9090";


module.exports = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    disableHostCheck: true,
    historyApiFallback: true,
    hot: true,
    open: true,
    https: false,
    contentBase: path.join(__dirname, '../public'),
    proxy: {
      '/api': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false
      },
      '/displays': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false
      },
      '/symbols': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false
      },
      '/components': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false
      },
      '/oss': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false,
        pathRewrite: {'/oss': '/api'}
      },
      '/img/assets': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false,
        pathRewrite: {'/img/assets': '/api/download?fileName=assets'}
      },
      '/htConfig': {
        'target': backendTarget,
        'changeOrigin': true,
        'secure': false,
        pathRewrite: {'/htConfig': '/api'}
      },
    }
  },
  entry: [
    require.resolve('react-dev-utils/webpackHotDevClient'),
    paths.appIndexJs
  ],
  output: {
    path: paths.appBuild,
    pathinfo: true,
    filename: 'static/js/bundle.js',
    chunkFilename: 'static/js/[name].chunk.js',
    publicPath: publicPath,
    devtoolModuleFilenameTemplate: info =>
      path.resolve(info.absoluteResourcePath)
        .replace(/\\/g, '/')
  },
  resolve: {
    modules: ['node_modules', paths.appNodeModules].concat(
      process.env.NODE_PATH.split(path.delimiter).filter(Boolean)
    ),
    extensions: [".ts", ".tsx", ".js", ".json", ".jsx"],
    alias: webpackResolveAlias,
    plugins: [
      new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson])
    ]
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        oneOf: [
          {
            test: val => val.startsWith(path.resolve(paths.appSrc, "remote")),
            loader: 'file-loader',
            type: 'javascript/auto',
            options: {
              name: 'static/remote/[name].[hash:base26:8].[ext]',
            }
          },
          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
            loader: require.resolve('url-loader'),
            options: {
              limit: 2048,
              name: 'static/media/[name].[hash:8].[ext]'
            }
          },
          {
            test: /\.(js|jsx)$/,
            include: paths.appSrc,
            loader: require.resolve('babel-loader')
          },
          {
            test: /\.tsx?$/,
            include: paths.appSrc,
            use: [
              {
                loader: require.resolve('ts-loader'),
                options: {
                  // disable type checker - we will use it in fork plugin
                  transpileOnly: true,
                },
              },
            ],
          },
          {
            test: /\.module\.(less|css)$/,
            use: [
              require.resolve('style-loader'),
              {
                loader: require.resolve('css-loader'),
                options: {
                  modules: true,
                  localIdentName: '[name]__[local]--[hash:base64:5]',
                }},
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      flexbox: 'no-2009'
                    })
                  ]
                }
              },
              require.resolve('less-loader'),
            ]
          },
          {
            test: /\.css$/,
            use: [
              require.resolve('style-loader'),
              {
                loader: require.resolve('css-loader'),
                options: {
                  importLoaders: 1
                }
              },
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      flexbox: 'no-2009'
                    })
                  ]
                }
              }
            ]
          },
          {
            test: /\.less$/,
            use: [
              require.resolve('style-loader'),
              require.resolve('css-loader'),
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      flexbox: 'no-2009'
                    })
                  ]
                }
              },
              {
                loader: require.resolve('less-loader'),
                options: {
                  javascriptEnabled: true
                }
              }
            ]
          },
          {
            exclude: [/\.js$/, /\.html$/, /\.json$/],
            loader: require.resolve('file-loader'),
            options: {
              name: 'static/media/[name].[hash:8].[ext]'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
      favicon: paths.appFavIcon
    }),
    new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
    new webpack.HotModuleReplacementPlugin(),
    new CaseSensitivePathsPlugin(),
    new WatchMissingNodeModulesPlugin(paths.appNodeModules),
    new webpack.DefinePlugin(env.stringified),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new ManifestPlugin({
      fileName: 'asset-manifest.json',
      publicPath: publicPath,
    }),
  ],
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  performance: {
    hints: false
  }
}
